/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_IDS_H_INCLUDED
#define SPHIREWALL_IDS_H_INCLUDED

#include <map>
#include <set>

#include "Core/Logger.h"
#include "Core/DetachedProcess.h"
#include <boost/regex.hpp>
#include <fstream>
#include "SFwallCore/Packetfwd.h"
#include "Core/ConfigurationManager.h"

class Lock;
class EventDb;

class AddressPair {
	public:
		AddressPair();
		AddressPair(int ip, int mask);

		long ip;
		long mask;
};

class IDS : public Configurable {
	public:
		IDS(EventDb *eventDb);
		~IDS();

		void start();
		void stop();
		bool status(){
			return running && process->running();
		}
		void refresh(){
			stop();
			if(enabled){
				start();
			}
		}

		void listExceptions(vector<AddressPair> &e);
		void addException(AddressPair pair);
		void delException(AddressPair pair);
		bool checkExceptions(unsigned int address);

		EventDb *getEventDb() const;

		bool enabled;
		std::string interface;

		/* Configurable methods */
		const char* getConfigurationSystemName(){
			return "Ids";
		}

		bool load();
		void save();

	private:
		std::vector<AddressPair> exceptions;
		EventDb *eventDb;
		DetachedProcess* process;
		bool running;
		ifstream stream;
		void __poll_for_alerts();
};

#endif
