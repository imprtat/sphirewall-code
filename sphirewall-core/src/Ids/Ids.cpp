/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <map>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <syslog.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>

#define __FAVOR_BSD
#if (linux)
#define __BSD_SOURCE
#endif

#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

using namespace std;

#include "Core/System.h"
#include "Ids/Ids.h"
#include "Core/Event.h"
#include "Utils/IP4Addr.h"

AddressPair::AddressPair()
{}

AddressPair::AddressPair(int ip, int mask)
	: ip(ip), mask(mask)
{}


void IDS::start() {
	Logger::instance()->log("sphirewalld.ids", INFO, "Starting IDS");

	/*
	   /usr/sbin/snort -m 027 -d -l /var/log/snort -u snort -g snort -c /etc/snort/snort.conf -i eth0
	*/	
	process->reset_params();
	process->set_executable("/usr/sbin/snort");
	process->add_param("-A");
	process->add_param("fast");
	process->add_param("-d");
	process->add_param("-l");
	process->add_param("/var/log/snort");
	process->add_param("-c");
	process->add_param("/etc/snort/snort.conf");
	process->add_param("-i");
	process->add_param(interface);	
	process->start(true);
	running = true;

	boost::thread(boost::bind(&IDS::__poll_for_alerts, this));
}

void IDS::stop() {
	process->stop();
	running = false;
}


void IDS::__poll_for_alerts() {
	while(running) {
		if (!stream.is_open()) {
			stream.open("/var/log/snort/alert");
			stream.clear();
		}

		if (stream.is_open()) {
			while (!stream.eof()) {
				std::string line;
				getline(stream, line);

				if(line.size() > 0){
					EventParams params;
					params["alert"] = line;
					Event* event = new Event(IDS_ALERT, params);
					eventDb->add(event);
				}
			}

			stream.clear();
		}

		sleep(1);
	}
}

IDS::IDS(EventDb *eventDb) {
	this->running = false;
	this->enabled = false;
	this->process = new DetachedProcess();
	this->eventDb = eventDb;
}

IDS::~IDS() {
	delete process;
}

void IDS::listExceptions(vector<AddressPair> &e) {
	e = exceptions;
}

void IDS::addException(AddressPair pair) {
	exceptions.push_back(pair);
}

void IDS::delException(AddressPair pair) {
	for (vector<AddressPair>::iterator iter = exceptions.begin(); iter != exceptions.end(); iter++) {
		AddressPair target = *iter;

		if (target.ip == pair.ip && target.mask == pair.mask) {
			exceptions.erase(iter);
			return;
		}
	}
}

bool IDS::checkExceptions(unsigned int address) {
	for (AddressPair ap : exceptions) {
		if (IP4Addr::matchNetwork(address, ap.ip, ap.mask) == 0) {
			return true;
		}
	}

	return false;
}

EventDb *IDS::getEventDb() const {
	return eventDb;
}

bool IDS::load(){
	if (configurationManager->has("ids")) {
		ObjectContainer *root = configurationManager->getElement("ids");
		enabled = root->get("enabled")->boolean();
		interface = root->get("interface")->string();
	}	

	refresh();
	return true;
}

void IDS::save(){
	ObjectContainer* root = new ObjectContainer(CREL);
	root->put("enabled", new ObjectWrapper(enabled));	
	root->put("interface", new ObjectWrapper(interface));	

	configurationManager->setElement("ids", root);
	configurationManager->save();
}

