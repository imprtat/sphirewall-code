/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include "Event.h"
#include "System.h"
#include "Auth/UserDb.h"
#include "SFwallCore/Firewall.h"

class LogEventHandler : public EventHandler {
	public:
		LogEventHandler *Clone();
		LogEventHandler();
		LogEventHandler(const LogEventHandler &h); 
		void handle(EventPtr event);
		string name() {
			return "Log Event Handler";
		}

		int getId() {
			return 1;
		}

		std::string key() const {
			return "handler.log";
		}
};

class FirewallBlockSourceAddressHandler : public EventHandler {
	public:

		FirewallBlockSourceAddressHandler();
		FirewallBlockSourceAddressHandler(const FirewallBlockSourceAddressHandler &h);
		FirewallBlockSourceAddressHandler *Clone();
		void handle(EventPtr event);
		string name() {
			return "Block source address";
		}

		int getId() {
			return 3;
		}

		std::string key() const {
			return "handler.firewall.block";
		}

};

#endif
