/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOR_PROVIDER_H
#define TOR_PROVIDER_H

#include "Core/ConfigurationManager.h"
#include "Core/DetachedProcess.h"
#include "Utils/Interfaces.h"

class TorProvider : public Configurable {
	public:
		TorProvider(){
			interface = "";
			enabled = false;
			dns_enabled = true;
			transport_listener_port = 9040;
			dns_listener_port = 53;
			process = new DetachedProcess();
		}

		/* Configuration Manager "Configurable" methods */
		bool load();
		void save();	
		const char* getConfigurationSystemName(){
			return "Tor provider";
		}

		/* Service methods */
		void start();
		void stop();
		bool status();
		std::string logs();

		//Configuration options
		std::string interface;
		bool enabled;
		bool dns_enabled;
		int transport_listener_port;
		int dns_listener_port;
		std::string extra_options;		

		InterfacePtr interface_ptr;
		DetachedProcess* process;
};

#endif
