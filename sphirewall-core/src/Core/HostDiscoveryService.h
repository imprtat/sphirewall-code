/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALLD_ARP_H
#define SPHIREWALLD_ARP_H

#include <map>
#include <vector>
#include <list>
#include <netinet/in.h>
#include <string>
#include <boost/unordered_map.hpp>

#include "Core/Logger.h"
#include "Core/Cron.h"
#include "Core/Lockable.h"
#include "Core/Host.h"

class EventDb;
class Lock;
namespace SFwallCore {
	class Packet;
	class Alias;

	typedef std::shared_ptr<Alias> AliasPtr;
};

class HostDnsHostnameResolverWorker : public Lockable {
	public:
		HostDnsHostnameResolverWorker();
		void push(HostPtr);
	private:
		void process();
		std::queue<HostPtr> workQueue;
		bool running;
};

class PersistedSession {
        public:
                PersistedSession() {}
                PersistedSession(std::string mac, std::string username) : mac(mac), username(username) {}

                std::string username;
                std::string mac;
};

class TimeoutConfiguration {
        public:
                TimeoutConfiguration() : timeout(0){}
                std::string id;

                std::list<SFwallCore::AliasPtr> networks;
                std::list<GroupPtr> groups;

                int timeout;

                bool matches(unsigned int ip, UserPtr user);
        private:
                bool __matches_groups(UserPtr user);
                bool __matches_network(unsigned int ip);
};

typedef boost::shared_ptr<TimeoutConfiguration> TimeoutConfigurationPtr;
typedef boost::shared_ptr<PersistedSession> PersistedSessionPtr;

class PersistedSessionStore : protected Lockable{
	public:
		void add_persisted_authentication_session(std::string mac_address, std::string username);
		void remove_persisted_authentication_session(std::string mac_address, std::string username);
		std::list<PersistedSessionPtr> get_persisted_authentication_sessions();
		PersistedSessionPtr check_for_persisted_session(std::string mac_address);
		void clear();
	private:
		std::map<std::string, PersistedSessionPtr> persisted_sessions;
};

class TimeoutConfigurationStore : protected Lockable {
	public:
		std::list<TimeoutConfigurationPtr> get_authentication_timeout_configurations();
		void remove_authentication_timeout_configuration(TimeoutConfigurationPtr conf);
		void add_authentication_timeout_configuration(TimeoutConfigurationPtr conf);
		void clear();
	private:
		std::list<TimeoutConfigurationPtr> timeout_configurations;
};

class HostDiscoveryService : protected Lockable, public Configurable {
	public:

		class CleanUpCron : public CronJob {
			public:

				CleanUpCron(HostDiscoveryService *root) : root(root), CronJob(60 * 30, "HOST_DISCOVERY_CLEANUP_CRON", true) {}
				void run();

			private:
				HostDiscoveryService *root;
		};

		HostDiscoveryService(CronManager *cron, Config *config);
		HostDiscoveryService();

		HostPtr get(in_addr_t ip);
		HostPtr get(struct in6_addr *ip);

		HostPtr update(SFwallCore::Packet *packet);
		HostPtr update(unsigned int ip, std::string mac);
		HostPtr update(struct in6_addr *ip, std::string mac);

		std::list<HostPtr> get_all_entries(bool authenticated=false);

		int size();
		long updates();

		void setEventDb(EventDb *eventDb);

		void authenticate_user_to_host(HostPtr host, UserPtr user, int timeout, bool absolute_timeout);
		void deauthenticate_user_to_host(HostPtr host, UserPtr user);
		void persist_authentication_session(HostPtr host, UserPtr user);

		/* Timeout Configuration Methods */
		std::list<TimeoutConfigurationPtr> get_authentication_timeout_configurations();
		void remove_authentication_timeout_configuration(TimeoutConfigurationPtr conf);
		void add_authentication_timeout_configuration(TimeoutConfigurationPtr conf);

		/* Persisted session methods */
		void add_persisted_authentication_session(std::string mac_address, std::string username);
		void remove_persisted_authentication_session(std::string mac_address, std::string username);
		std::list<PersistedSessionPtr> get_persisted_authentication_sessions();

		bool load();
		void save();
		const char* getConfigurationSystemName(){
			return "Host Manager";
		}

	private:
		HostPtr getByMac(std::string mac, in_addr_t ip);
		HostPtr getByMac(std::string mac, struct in6_addr *ip);

		boost::unordered_map<std::string, std::list<HostPtr> > entryTable;

		EventDb *eventDb;

		HostPtr add(std::string mac, in_addr_t ip);
		HostPtr add(std::string mac, struct in6_addr *ip);

		static int HOST_DISCOVERY_SERVICE_TIMEOUT;
		int _size;
		long _updates;

		PersistedSessionStore* persisted_session_store;
		TimeoutConfigurationStore* timeout_configuration_store;
		HostDnsHostnameResolverWorker *dnsHostnameWorker;
};

#endif
