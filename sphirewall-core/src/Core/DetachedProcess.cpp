#include <iostream>
#include <unistd.h>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <string.h>
#include <boost/algorithm/string/join.hpp>

#include "Core/DetachedProcess.h"
#include "Core/Logger.h"

using namespace std;

void DetachedProcess::start(bool single_instance){
	if(single_instance){
		stringstream s1; s1 << "killall -e -9 " << process;
		system(s1.str().c_str());
	}

	Logger::instance()->log("sphirewalld.detachedprocess.start", INFO, 
			"Starting process '%s' with params '%s'", process.c_str(), boost::algorithm::join(params, " ").c_str());

	signal(SIGCHLD, SIG_IGN);
	int child_pid = fork();
	if(child_pid != 0){
		pid = child_pid;
		return;
	}

	Logger::instance()->log("sphirewalld.detachedprocess.start", INFO,
			"Process started with pid: %d", pid);

	int maxfd=sysconf(_SC_OPEN_MAX);
	for(int fd=3; fd<maxfd; fd++) {
		close(fd);
	}

	std::vector<char*> execv_params;
	execv_params.push_back(strdup(process.c_str()));
	for(std::string param : params){
		execv_params.push_back(strdup(param.c_str()));		
	}
	execv_params.push_back(0);
	if(execv (process.c_str(), &execv_params.front()) < 0){
		Logger::instance()->log("sphirewalld.detachedprocess.start", ERROR, "Could not start process, execv failed");
	}

	exit(-1);
}

bool DetachedProcess::running(){
	std::stringstream ss; ss << "/proc/" << pid;
	struct stat sts;
	if (stat(ss.str().c_str(), &sts) == -1 && errno == ENOENT) {
		return false;
	}
	return true;
}

void DetachedProcess::stop(){
	if(pid > 1){
		Logger::instance()->log("sphirewalld.detachedprocess.stop", INFO, "Sending SIGTERM to process on pid %d", pid);
		kill(pid, SIGTERM);
	}
}
