#ifndef DETACHED_PROCESS_H
#define DETACHED_PROCESS_H

#include <vector>

class DetachedProcess {
	public:
		void set_executable(const std::string process){
			this->process = process;
		}

		void add_param(const std::string param){
			this->params.push_back(param);
		}

		void reset_params(){
			params.clear();
		}

		void start(bool single_instance=false);
		bool running();			
		void stop();

		
	private:
		std::string process;
		int pid;
		std::vector<std::string> params;
};

#endif
