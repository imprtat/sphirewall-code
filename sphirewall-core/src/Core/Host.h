/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HOST_H
#define HOST_H

#include <string>
#include <netinet/in.h>
#include <boost/shared_ptr.hpp>

#include "Auth/User.h"

namespace SFwallCore {
	class Packet;
};

class Host {
	public:
		Host(){
			quarantined = false;
			lastSeen = 0;
			firstSeen = 0;

			total_packet_count = 0;
			total_transfer_count = 0;
			total_active_connections = 0;

			authenticated_user_timeout = 0;
			authenticated_user_timeout_absolute = false;
			authenticated_user_login_time=0;
		}		

		std::string getIp();
		std::string hostname();
		void fetchHostname();

		std::string mac;
		in_addr_t ip;
		struct in6_addr ipV6;

		int lastSeen;
		int firstSeen;
		int type;
		bool quarantined;

		double total_packet_count;
		double total_transfer_count;
		double total_active_connections;

		UserPtr authenticated_user;	
		double authenticated_user_timeout;
		bool authenticated_user_timeout_absolute;
		unsigned int authenticated_user_login_time;
		bool has_user_timed_out();

		void increment(SFwallCore::Packet* packet);
	private:
		std::string hostnameCache;
};

typedef boost::shared_ptr<Host> HostPtr;

#endif // HOST_H
