#ifndef L2TP_IPSEC_GATEWAY_H
#define L2TP_IPSEC_GATEWAY_H

#include "Core/DetachedProcess.h"
#include "Auth/Group.h"

#include "SFwallCore/Firewall.h"
#include "SFwallCore/Packet.h"

class IPSecL2TPGatewayInstanceClient {
        public:
                std::string local_ip;
                std::string remote_ip;
                std::string username;
                std::string ifname;
};

typedef boost::shared_ptr<IPSecL2TPGatewayInstanceClient> IPSecL2TPGatewayInstanceClientPtr;

class IPSecL2TPGateway : public SFwallCore::ExternalFirewallHandler {
        public:
                enum {
                        L2TP_AUTHMODE_NONE =0,
                        L2TP_AUTHMODE_SPHIREWALL =1,
                        L2TP_AUTHMODE_PAP=2,
                        L2TP_AUTHMODE_CHAP=3
                };

                IPSecL2TPGateway(){
			this->name = "l2tp";
                        this->user_authentication_mode  = L2TP_AUTHMODE_NONE;
                        this->process = new DetachedProcess();
			this->auto_start = false;
			this->auto_configure_firewall = false;
                }

		int efh__filter_handle(SFwallCore::Packet* packet);
		bool efh__enabled();
	
                void initiate();
                void terminate();
                bool status(){
                        return (process && process->running());
                }
		void refresh();		

                bool vpn_assign_ip;
                unsigned int vpn_remote_ip_range_start;
                unsigned int vpn_remote_ip_range_end;
                unsigned int vpn_local_ip;

		bool provide_dns;
		std::string dns_ns1;
		std::string dns_ns2;

		std::string local_device;
		InterfacePtr resolved_local_device;

                bool auto_start;
                int auth_mode;
                std::string secret_key;
		bool auto_configure_firewall;

                /* User authentication options */
                int user_authentication_mode;
                std::list<GroupPtr> allowed_users_groups;
                std::list<IPSecL2TPGatewayInstanceClientPtr> active_clients;

		void save(ConfigurationManager* c);
		bool load(ConfigurationManager* c);
		
		std::string name;
	private:
		void __add_connection();
		void __add_keys();
		void __configure_xl2tp();
		void __unconfigure_xl2tp();
		bool configured;

		void __write_pppd_opts();
		void __write_xl2tp_opts();

		std::string __get_pppd_opts_filename();
		std::string __get_xl2tp_opts_filename();

		DetachedProcess* process;
};

#endif
