/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <signal.h>

using namespace std;

#include "Core/SignalHandler.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include <execinfo.h>
#include <signal.h>
#include <exception>
#include <iostream>
#include <cxxabi.h>

void sighandler(int sig) {
	switch (sig) {
	case SIGINT:
		Logger::instance()->log("sphirewalld", EVENT, "SIGINT caught");
		System::getInstance()->shutdown();
		break;

	case SIGSEGV:

	case SIGABRT: {
		void *array[10];
		size_t size;
		char **strings;
		size_t i;

		size = backtrace(array, 10);
		strings = backtrace_symbols(array, size);

		Logger::instance()->log("sphirewalld", CRITICAL_ERROR, "SIGSEGV or SIGTERM caught");

		for (i = 0; i < size; i++) {
			Logger::instance()->log("sphirewalld", CRITICAL_ERROR, strings[i]);
		}

		free(strings);
		System::getInstance()->shutdown();
		break;
	}

	case SIGPIPE:
		Logger::instance()->log("sphirewalld", EVENT, "SIGPIPE caught");
		break;

	case SIGTERM:
		Logger::instance()->log("sphirewalld", EVENT, "SIGTERM caught");
		System::getInstance()->shutdown();
		break;

	case SIGHUP:
		Logger::instance()->log("sphirewalld", EVENT, "SIGHUP caught");
		System::getInstance()->shutdown();
		break;

	default:
		break;
	}
}
