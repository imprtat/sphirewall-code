/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Auth/User.h"
#include "Auth/Group.h"
#include "Auth/UserDb.h"
#include "Auth/GroupDb.h"
#include "Core/QuotaManager.h"
#include "Core/Event.h"
#include "Core/System.h"
#include "Utils/TimeWrapper.h"
#include "SFwallCore/Connection.h"

void QuotaManager::run() {
        //Check the user quota's first:
        bool globalExceeded = evaluateGlobalQuotas();
        alertOnQuota(globalExceeded);

        for (string username : userDb->list()) {
                UserPtr user = userDb->getUser(username);
                if (user) {
                        bool exceeded = evaluateQuotaForUser(user);
                        alertOnQuota(user, exceeded);
                }
        }
}

bool QuotaManager::evaluateQuotaForUser(UserPtr user) {
        return evaluateQuotaForUser(time(NULL), user);
}

bool QuotaManager::evaluateGlobalQuotas() {
        if (globalQuota->dailyQuota) {
                if (globalCounter->getDayTotalTransfer() > globalQuota->dailyQuotaLimit) {
                        return true;
                }
        }

        if (globalQuota->weeklyQuota) {
                if (globalCounter->getWeekTotalTransfer() > globalQuota->weeklyQuotaLimit) {
                        return true;
                }
        }

        if (globalQuota->monthQuota) {
                if (globalCounter->getMonthTotalTransfer() > globalQuota->monthQuotaLimit) {
                        return true;
                }
        }

        return false;
}

bool QuotaManager::evaluateQuotaForUser(int baseTime, UserPtr user) {
        //First go through the users groups:
        for (GroupPtr group : user->getGroups()) {
                QuotaInfo &quota = group->getQuota();

                if (quota.totalQuota) {
                        if (user->getQuotaCounter()->getTotalTransfer() > group->getQuota().totalQuotaLimit) {
                                return true;
                        }
                }

                if (quota.dailyQuota) {
                        if (user->getQuotaCounter()->getDayTotalTransfer() > group->getQuota().dailyQuotaLimit) {
                                return true;
                        }
                }

                if (quota.weeklyQuota) {
                        if (user->getQuotaCounter()->getWeekTotalTransfer() > group->getQuota().weeklyQuotaLimit) {
                                return true;
                        }
                }

                if (quota.monthQuota) {
                        if (user->getQuotaCounter()->getMonthTotalTransfer() > quota.monthQuotaLimit) {
                                return true;
                        }
                }

		if (quota.timeQuota) {
			if ((time(NULL) - user->getCreationTime()) > quota.timeQuotaLimit) {
				return true;
			}
		}
	}

	if (user->getQuota()->totalQuota) {
		if (user->getQuotaCounter()->getTotalTransfer()  > user->getQuota()->totalQuotaLimit) {
			return true;
		}
	}

	if (user->getQuota()->dailyQuota) {
		if (user->getQuotaCounter()->getDayTotalTransfer() > user->getQuota()->dailyQuotaLimit) {
			return true;
		}
	}

	if (user->getQuota()->weeklyQuota) {
		if (user->getQuotaCounter()->getWeekTotalTransfer() > user->getQuota()->weeklyQuotaLimit) {
			return true;
		}
	}

	if (user->getQuota()->monthQuota) {
		if (user->getQuotaCounter()->getMonthTotalTransfer() > user->getQuota()->monthQuotaLimit) {
			return true;
		}
	}

	if (user->getQuota()->timeQuota) {
		if ((time(NULL) - user->getCreationTime()) > user->getQuota()->monthQuotaLimit) {
			return true;
		}
	}

	return false;
}

void QuotaManager::alertOnQuota(bool exceeded) {
	if (exceeded) {
		if (!getQuota()->quotaExceeded) {
			getQuota()->quotaExceeded = true;

			EventParams params;
			eventDb->add(new Event(GLOBAL_QUOTA_EXCEEDED, params));
		}
	}
	else {
		if (getQuota()->quotaExceeded) {
			getQuota()->quotaExceeded = false;

			EventParams params;
			eventDb->add(new Event(GLOBAL_QUOTA_OK, params));
		}
	}
}

void QuotaManager::alertOnQuota(UserPtr user, bool exceeded) {
	if (exceeded) {
		if (!user->isQuotaExceeded()) {
			user->setQuotaExceeded(true);

			EventParams params;
			params["username"] = user->getUserName();
			eventDb->add(new Event(USERDB_QUOTA_EXCEEDED, params));
		}
	}
	else {
		if (user->isQuotaExceeded()) {
			user->setQuotaExceeded(false);

			EventParams params;
			params["username"] = user->getUserName();
			eventDb->add(new Event(USERDB_QUOTA_OK, params));

		}
	}
}

bool QuotaManager::load() {
	globalQuota->dailyQuota = false;
	globalQuota->dailyQuotaLimit = -1;

	globalQuota->weeklyQuota = false;
	globalQuota->weeklyQuotaLimit = -1;

	globalQuota->monthQuotaLimit = -1;

	if (configurationManager->has("quotas")) {
		ObjectContainer *root = configurationManager->getElement("quotas");
		globalQuota->dailyQuota = root->get("dailyQuota")->boolean();
		globalQuota->dailyQuotaLimit = root->get("dailyQuotaLimit")->number();

		globalQuota->weeklyQuota = root->get("weeklyQuota")->boolean();
		globalQuota->weeklyQuotaLimit = root->get("weeklyQuotaLimit")->number();

		globalQuota->monthQuota = root->get("monthQuota")->boolean();
		globalQuota->monthQuotaLimit = root->get("monthQuotaLimit")->number();
	}
	return true;
}

void QuotaManager::save() {
	ObjectContainer *root = new ObjectContainer(CREL);
	root->put("dailyQuota", new ObjectWrapper((bool) globalQuota->dailyQuota));
	root->put("dailyQuotaLimit", new ObjectWrapper((double) globalQuota->dailyQuotaLimit));

	root->put("weeklyQuota", new ObjectWrapper((bool) globalQuota->weeklyQuota));
	root->put("weeklyQuotaLimit", new ObjectWrapper((double) globalQuota->weeklyQuotaLimit));

	root->put("monthQuota", new ObjectWrapper((bool) globalQuota->monthQuota));
	root->put("monthQuotaLimit", new ObjectWrapper((double) globalQuota->monthQuotaLimit));

	configurationManager->holdLock();
	configurationManager->setElement("quotas", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

QuotaManager::QuotaManager(UserDb *userDb, GroupDb *groupDb, EventDb* eventDb) : CronJob(60 * 5, "QUOTA_MANAGER_ROLL", true){
	this->userDb = userDb;
	this->groupDb = groupDb;
	this->eventDb= eventDb;
	this->globalQuota= new QuotaInfo();
	this->globalCounter = new QuotaCounter();
}

void QuotaCounter::increment(double size){
	cumulativeTotal += size;

	Time currentTime = System::getInstance()->getTimeManager().AsTime();
	//Update day stats:
	int currentDayOfYear = currentTime.dayOfYear();
	if(currentDayOfYear != lastUpdateDayOfYear){
		lastUpdateDayOfYear = currentDayOfYear;
		dayTotal = size;
	}else{
		dayTotal += size;
	}

	//Update week stats
	if((currentTime.timestamp()) > (lastUpdateWeekStart + 60 * 60 * 24 * 7)){
		Time* startOfWeek = System::getInstance()->getTimeManager().AsTime().startOfWeek();
		lastUpdateWeekStart = startOfWeek->timestamp(); 
		delete startOfWeek;
		weekTotal = size;
	}else{
		weekTotal += size;
	}

	//Update month stats
	int currentMonth = currentTime.month();
	if(currentMonth!= lastUpdateMonth){
		lastUpdateMonth = currentMonth;
		monthTotal = size;
	}else{
		monthTotal += size;
	}
}

double QuotaCounter::getTotalTransfer(){
	return cumulativeTotal;
}

double QuotaCounter::getDayTotalTransfer(){
	return dayTotal;
}

double QuotaCounter::getWeekTotalTransfer(){
	return weekTotal;
}

double QuotaCounter::getMonthTotalTransfer(){
	return monthTotal;
}

void QuotaManager::hook(SFwallCore::Connection *conn){
	if(conn->get_authenticated_user()){
		conn->get_authenticated_user()->getQuotaCounter()->increment(conn->getDownload() + conn->getUpload());
	}

	globalCounter->increment(conn->getDownload() + conn->getUpload());
}
