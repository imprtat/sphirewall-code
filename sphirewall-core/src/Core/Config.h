/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_CONFIG_H_INCLUDED
#define SPHIREWALL_CONFIG_H_INCLUDED

#include <list>
#include <string>
#include <map>
#include <vector>
#include "Core/Logger.h"
#include "Core/ConfigurationManager.h"

using namespace std;

class LoggingConfiguration : public Configurable{
	public:
		LoggingConfiguration(Logger *rootLogger);

		std::list<std::string> getContexts();
		int getLevel(std::string context);
		int setLevel(std::string context, int priority);
		int rmLevel(std::string context);

		void save();
		bool load();
                const char* getConfigurationSystemName(){
                        return "Logging configuration";
                }

	private:
		Logger *rootLogger;
};

class RuntimeConfiguration : public Configurable {
	public:
		RuntimeConfiguration();
		int get(std::string key);
		int set(std::string key, int newvalue);
		void loadOrPut(std::string key, int *value);
		void listAll(std::list<string> &l);
		bool contains(std::string key);

		bool load();
                const char* getConfigurationSystemName(){
                        return "Runtime optimized configuration";
                }

	private:
		void put(std::string key, int *ptr);
		map<string, int *> configMap;
};

class Config {
	public:
		Config();
		RuntimeConfiguration *getRuntime();
		void setConfigurationManager(ConfigurationManager *configurationManager);
		ConfigurationManager *getConfigurationManager();
	private:
		ConfigurationManager *configurationManager;
		RuntimeConfiguration *runtime;
};


#endif
