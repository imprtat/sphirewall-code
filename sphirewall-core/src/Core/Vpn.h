/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VPN_H
#define VPN_H

#include <boost/shared_ptr.hpp>
#include <list>

#define VPN_STATE_ESTABLISHING 1
#define VPN_STATE_ESTABLISHED 0
#define VPN_STATE_DEAD -1

#include "Core/ConfigurationManager.h"
#include "Core/IPSec.h"
#include "Core/L2tpIpsecGateway.h"

class SiteToSiteVpnManager;

class VpnManager : public Configurable{
	public:
		VpnManager();

		void save();
		bool load();
	        const char* getConfigurationSystemName(){
			return "Vpn Manager";
		}

		SiteToSiteVpnManager* get_sitetosite() {
			return site_to_site;
		}

		IPSecL2TPGateway* get_l2tp() {
			return l2tp_server;
		}
	private:
		SiteToSiteVpnManager* site_to_site;
		IPSecL2TPGateway* l2tp_server;
};

#endif
