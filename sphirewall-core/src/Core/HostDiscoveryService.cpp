/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Core/HostDiscoveryService.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "Utils/Utils.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Utils/Lock.h"
#include "Core/Lockable.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Alias.h"
#include "Core/Host.h"

int HostDiscoveryService::HOST_DISCOVERY_SERVICE_TIMEOUT = 60 * 10;

long HostDiscoveryService::updates(){
	return _updates;
}

HostPtr HostDiscoveryService::update(unsigned int ip, std::string mac) {
	if (tryLock()) {
		HostPtr arpEntry = getByMac(mac, ip);
		_updates++;

		if (arpEntry) {
			arpEntry->lastSeen = time(NULL);
			if(!arpEntry->authenticated_user){
				PersistedSessionPtr persisted_session = persisted_session_store->check_for_persisted_session(mac);
				if(persisted_session){
					authenticate_user_to_host(arpEntry, System::getInstance()->getUserDb()->getUser(persisted_session->username), -1, false); 
				}
			}
		}
		else {
			arpEntry = add(mac, ip);
			if (eventDb && arpEntry) {
				EventParams params;
				params["ip"] = IP4Addr::ip4AddrToString(ip);
				params["hw"] = mac;

				if (eventDb) {
					eventDb->add(new Event(NETWORK_ARP_DISCOVERED, params));
				}
			}

			PersistedSessionPtr persisted_session = persisted_session_store->check_for_persisted_session(mac);
			if(persisted_session){
				authenticate_user_to_host(arpEntry, System::getInstance()->getUserDb()->getUser(persisted_session->username), -1, false);
			}
		}

		releaseLock();
		return arpEntry;
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::update(struct in6_addr *ip, std::string mac) {
	if (tryLock()) {
		HostPtr arpEntry = getByMac(mac, ip);
		_updates++;
		if (arpEntry) {
			arpEntry->lastSeen = time(NULL);
			if(!arpEntry->authenticated_user){
				PersistedSessionPtr persisted_session = persisted_session_store->check_for_persisted_session(mac);
				if(persisted_session){
					authenticate_user_to_host(arpEntry, System::getInstance()->getUserDb()->getUser(persisted_session->username), -1, false); 
				}
			}
		}
		else {
			arpEntry = add(mac, ip);
			if (eventDb && arpEntry) {
				EventParams params;
				params["ip"] = IP6Addr::toString(ip);
				params["hw"] = mac;

				if (eventDb) {
					eventDb->add(new Event(NETWORK_ARP_DISCOVERED, params));
				}
			}

                        PersistedSessionPtr persisted_session = persisted_session_store->check_for_persisted_session(mac);
                        if(persisted_session){
                                authenticate_user_to_host(arpEntry, System::getInstance()->getUserDb()->getUser(persisted_session->username), -1, false); 
                        }
		}

		releaseLock();
		return arpEntry;
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::update(SFwallCore::Packet *pk) {
	if (pk->type() == IPV4) {
		SFwallCore::PacketV4 *v4 = (SFwallCore::PacketV4 *) pk;

		if (IP4Addr::isLocal(v4->getSrcIp()) && v4->getHw().size() != 0) {
			return update(v4->getSrcIp(), pk->getHw());
		}
	}
	else if (pk->type() == IPV6) {
		SFwallCore::PacketV6 *v4 = (SFwallCore::PacketV6 *) pk;

		if (v4->getSrcIp() && v4->getHw().size() != 0) {
			return update(v4->getSrcIp(), pk->getHw());
		}
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::get(in_addr_t ip) {
	holdLock();
	for (std::pair<std::string, std::list<HostPtr> > entries : entryTable) {
		for (HostPtr entry : entries.second) {
			if (entry->type == 0 && entry->ip == ip) {
				releaseLock();
				return entry;
			}
		}
	}
	releaseLock();
	return HostPtr();
}

HostPtr HostDiscoveryService::getByMac(string mac, in_addr_t ip) {
	boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.find(mac);

	if (iter != entryTable.end()) {
		std::list<HostPtr> entries = iter->second;

		for (HostPtr entry : entries) {
			if (entry->type == 0 && entry->ip == ip) {
				return entry;
			}
		}
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::get(struct in6_addr *ip) {
	holdLock();
	for (std::pair<std::string, std::list<HostPtr> > entries : entryTable) {
		for (HostPtr entry : entries.second) {
			if (entry->type == 1) {
				if (memcmp(ip, &entry->ipV6, sizeof(struct in6_addr)) == 0) {
					releaseLock();
					return entry;
				}
			}
		}
	}
	releaseLock();
	return HostPtr();
}

HostPtr HostDiscoveryService::getByMac(string mac, struct in6_addr *ip) {
	boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.find(mac);

	if (iter != entryTable.end()) {
		std::list<HostPtr> entries = iter->second;

		for (HostPtr entry : entries) {
			if (entry->type == 1) {
				if (memcmp(ip, &entry->ipV6, sizeof(struct in6_addr)) == 0) {
					return entry;
				}
			}
		}
	}

	return HostPtr();
}

std::list<HostPtr> HostDiscoveryService::get_all_entries(bool authenticated){
	holdLock();
	std::list<HostPtr> ret;
	for (boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.begin(); iter != entryTable.end(); iter++) {
		for (HostPtr entry : iter->second) {
			if(authenticated){
				if(entry->authenticated_user){
					ret.push_back(entry);
				}
			}else{
				ret.push_back(entry);
			}
		}
	}
	releaseLock();
	return ret;
}

bool Host::has_user_timed_out(){
	if(authenticated_user_timeout_absolute){
		if(time(NULL) > authenticated_user_timeout){
			return true;
		}
	}else{
		int elapsed_time = time(NULL) - lastSeen;
		if(elapsed_time > authenticated_user_timeout){
			return true;
		}
	}

	return false;
}

void HostDiscoveryService::CleanUpCron::run() {
	int t = time(NULL) - HOST_DISCOVERY_SERVICE_TIMEOUT;
	root->holdLock();
	vector<string> rowsToDelete;

	for (boost::unordered_map<string, std::list<HostPtr> >::iterator iter = root->entryTable.begin(); iter != root->entryTable.end(); iter++) {
		for (std::list<HostPtr>::iterator eiter = iter->second.begin(); eiter != iter->second.end(); eiter++) {

			HostPtr target = (*eiter);
			/* Do we have a user Object? If so then we must evaluate the user based timeouts */
			if(target->authenticated_user){
				if(target->has_user_timed_out()){
					if(System::getInstance()->getEventDb()){
						EventParams params;
						params["user"] = target->authenticated_user->getUserName();
						params["mac"] = target->mac;
						params["ip"] = target->getIp();
						System::getInstance()->getEventDb()->add(new Event(USERDB_SESSION_TIMEOUT, params));
					}

					root->deauthenticate_user_to_host(target, target->authenticated_user);
				}else{
					continue;
				}
			}

			if (target->lastSeen < t) {
				EventParams params;
				params["ip"] = target->getIp();
				params["hw"] = target->mac;

				if (root->eventDb) {
					root->eventDb->add(new Event(NETWORK_ARP_TIMEDOUT, params));
				}

				eiter = iter->second.erase(eiter);
				root->_size -= 1;
			}
		}

		if (iter->second.size() == 0) {
			rowsToDelete.push_back(iter->first);
		}
	}

	for (unsigned x = 0; x < rowsToDelete.size(); x++) {
		root->entryTable.erase(rowsToDelete[x]);
	}

	root->releaseLock();
}

HostPtr HostDiscoveryService::add(string mac, in_addr_t ip) {
	if (mac.size() == 17) {
		HostPtr entry(new Host());
		entry->mac = mac;
		entry->ip = ip;
		entry->firstSeen = time(NULL);
		entry->lastSeen = time(NULL);
		entry->type = 0;

		dnsHostnameWorker->push(entry);
		entryTable[mac].push_back(entry);
		_size += 1;

		return entry;
	}

	return HostPtr();
}


HostPtr HostDiscoveryService::add(string mac, struct in6_addr *ip) {
	if (mac.size() == 17) {
		HostPtr entry(new Host());
		entry->mac = mac;
		memcpy(&entry->ipV6, ip, sizeof(struct in6_addr));
		entry->firstSeen = time(NULL);
		entry->lastSeen = time(NULL);
		entry->type = 1;
		dnsHostnameWorker->push(entry);
		entryTable[mac].push_back(entry);
		_size += 1;

		return entry;
	}

	return HostPtr();
}


int HostDiscoveryService::size() {
	return _size;
}

HostDiscoveryService::HostDiscoveryService(CronManager *cron, Config *config) :  _size(0), _updates(0) {
	cron->registerJob(new CleanUpCron(this));
	config->getRuntime()->loadOrPut("HOST_DISCOVERY_SERVICE_TIMEOUT", &HOST_DISCOVERY_SERVICE_TIMEOUT);
	eventDb = NULL;

	persisted_session_store = new PersistedSessionStore();
	timeout_configuration_store = new TimeoutConfigurationStore();
	dnsHostnameWorker = new HostDnsHostnameResolverWorker();
}


HostDiscoveryService::HostDiscoveryService(): _size(0), _updates(0) {
	eventDb = NULL;
	dnsHostnameWorker = new HostDnsHostnameResolverWorker();
	persisted_session_store = new PersistedSessionStore();
	timeout_configuration_store = new TimeoutConfigurationStore();
}

void HostDiscoveryService::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

HostDnsHostnameResolverWorker::HostDnsHostnameResolverWorker()
	: running(false) {}

	void HostDnsHostnameResolverWorker::push(HostPtr hostPtr) {
		holdLock();
		workQueue.push(hostPtr);

		if (!running) {
			running = true;
			releaseLock();
			boost::thread(boost::bind(&HostDnsHostnameResolverWorker::process, this));
		}
		else {
			releaseLock();
		}
	}

void HostDnsHostnameResolverWorker::process() {
	while (true) {
		holdLock();

		if (workQueue.empty()) {
			releaseLock();
			break;
		}

		HostPtr host = workQueue.front();
		workQueue.pop();
		releaseLock();

		if (host) {
			host->fetchHostname();
		}
	}

	running = false;
}

void HostDiscoveryService::authenticate_user_to_host(HostPtr host, UserPtr user, int timeout=-1, bool absolute_timeout=false){
	//Determine Timeouts
	if(timeout == -1){
		int maxTimeout = 0;
		for(TimeoutConfigurationPtr nbt: get_authentication_timeout_configurations()){
			if(nbt->matches(host->ip, user)){
				if(nbt->timeout != 0 && maxTimeout < nbt->timeout){
					maxTimeout = nbt->timeout;
				}
			}
		}

		if(maxTimeout == 0){
			maxTimeout = configurationManager->hasByPath("authentication:session_timeout") ? configurationManager->get("authentication:session_timeout")->number() : -1;
		}

		host->authenticated_user_timeout = maxTimeout;
		host->authenticated_user = user;
		host->authenticated_user_login_time = time(NULL);
	}else{
		host->authenticated_user_timeout = timeout;
		host->authenticated_user_timeout_absolute = absolute_timeout;
		host->authenticated_user = user;
		host->authenticated_user_login_time = time(NULL);
	}

	if(System::getInstance()->getEventDb()){
		EventParams params;
		params["user"] = user->getUserName();
		params["ip"] = IP4Addr::ip4AddrToString(host->ip);
		params["timeout"] = host->authenticated_user_timeout;
		params["hw"] = host->mac;
		params["mac"] = host->mac;
		System::getInstance()->getEventDb()->add(new Event(USERDB_LOGIN_SUCCESS, params));
	}

}

void HostDiscoveryService::deauthenticate_user_to_host(HostPtr host, UserPtr user){
	host->authenticated_user = UserPtr();	
	host->authenticated_user_timeout = 0;
	host->authenticated_user_login_time = 0;
	host->authenticated_user_timeout_absolute = false;
}

void HostDiscoveryService::persist_authentication_session(HostPtr host, UserPtr user){
	add_persisted_authentication_session(host->mac, user->getUserName());
}

bool TimeoutConfiguration::__matches_network(unsigned int ip){
	if(networks.size() == 0){
		return true;
	}

	for(SFwallCore::AliasPtr a : networks){
		if(a->searchForNetworkMatch(ip)){
			return true;
		}
	}

	return false;
}

bool TimeoutConfiguration::__matches_groups(UserPtr user){
	if(groups.size() == 0){
		return true;
	}

	for(GroupPtr group : groups){
		if(user->checkForGroup(group)){
			return true;
		}
	}

	return false;
}


bool TimeoutConfiguration::matches(unsigned int ip, UserPtr user){
	return __matches_network(ip) && __matches_groups(user);
}

std::list<TimeoutConfigurationPtr> TimeoutConfigurationStore::get_authentication_timeout_configurations(){
	std::list<TimeoutConfigurationPtr> ret;
	holdLock();
	ret = timeout_configurations;
	releaseLock();
	return timeout_configurations;
}

void TimeoutConfigurationStore::remove_authentication_timeout_configuration(TimeoutConfigurationPtr timeout){
	holdLock();
	timeout_configurations.remove(timeout);
	releaseLock();
}

void TimeoutConfigurationStore::add_authentication_timeout_configuration(TimeoutConfigurationPtr timeout){
	holdLock();
	timeout_configurations.push_back(timeout);
	releaseLock();
}

void TimeoutConfigurationStore::clear(){
	holdLock();
	timeout_configurations.clear();
	releaseLock();
}

void PersistedSessionStore::add_persisted_authentication_session(std::string mac_address, std::string username){
	holdLock();
	persisted_sessions[mac_address] = PersistedSessionPtr(new PersistedSession(mac_address, username));
	releaseLock();
}

void PersistedSessionStore::remove_persisted_authentication_session(std::string mac_address, std::string username){
	holdLock();
	persisted_sessions.erase(mac_address);
	releaseLock();
}

PersistedSessionPtr PersistedSessionStore::check_for_persisted_session(std::string mac_address){
	if(persisted_sessions.find(mac_address) != persisted_sessions.end()){
		return persisted_sessions[mac_address];
	}

	return PersistedSessionPtr();
}

std::list<PersistedSessionPtr> PersistedSessionStore::get_persisted_authentication_sessions(){
	std::list<PersistedSessionPtr> ret;

	holdLock();
	for (pair<std::string, PersistedSessionPtr> target : persisted_sessions) {
		ret.push_back(target.second);
	}
	releaseLock();
	return ret;
}

void PersistedSessionStore::clear(){
	holdLock();
	persisted_sessions.clear();
	releaseLock();
}

void HostDiscoveryService::save(){
	this->holdLock();

	Logger::instance()->log("sphirewalld.sessiondb", EVENT, "Saving Sessions");
	if (configurationManager) {
		ObjectContainer *root = new ObjectContainer(CREL);
		ObjectContainer *array = new ObjectContainer(CARRAY);

		for (PersistedSessionPtr target : get_persisted_authentication_sessions()) {
			ObjectContainer *session = new ObjectContainer(CREL);

			session->put("username", new ObjectWrapper(target->username));
			session->put("macAddress", new ObjectWrapper(target->mac));

			array->put(new ObjectWrapper(session));
		}

		root->put("sessions", new ObjectWrapper(array));

		ObjectContainer *timeoutArray = new ObjectContainer(CARRAY);
		for (TimeoutConfigurationPtr timeout : get_authentication_timeout_configurations()) {
			ObjectContainer *to= new ObjectContainer(CREL);

			to->put("id", new ObjectWrapper((string) timeout->id));
			to->put("timeout", new ObjectWrapper((double) timeout->timeout));
			ObjectContainer *networksArray= new ObjectContainer(CARRAY);
			for(SFwallCore::AliasPtr network : timeout->networks){
				networksArray->put(new ObjectWrapper(network->id));
			}

			to->put("networks", new ObjectWrapper(networksArray));

			ObjectContainer *groupArray = new ObjectContainer(CARRAY);
			for(GroupPtr group: timeout->groups){
				groupArray->put(new ObjectWrapper((double) group->getId()));
			}

			to->put("groups", new ObjectWrapper(groupArray));

			timeoutArray->put(new ObjectWrapper(to));
		}

		root->put("network_based_timeouts", new ObjectWrapper(timeoutArray));

		configurationManager->holdLock();
		configurationManager->setElement("sessionDb", root);
		configurationManager->save();
		configurationManager->releaseLock();
	}

	this->releaseLock();
}

bool HostDiscoveryService::load(){
	this->holdLock();
	persisted_session_store->clear();
	timeout_configuration_store->clear();

	Logger::instance()->log("sphirewalld.sessiondb", EVENT, "Loading Sessions");

	if (!configurationManager->has("sessionDb")) {
		Logger::instance()->log("sphirewalld.sessiondb", ERROR, "Loading session database failed, no sessions found. Ignoring");
	}
	else {
		ObjectContainer* root = configurationManager->getElement("sessionDb");
		ObjectContainer* sessions = root->get("sessions")->container();

		for (int x = 0; x < sessions->size(); x++) {
			ObjectContainer* o = sessions->get(x)->container();
			std::string mac = o->get("macAddress")->string();
			std::string username = o->get("username")->string();
			add_persisted_authentication_session(mac, username);
		}

		if(root->has("network_based_timeouts")){
			ObjectContainer* nbto = root->get("network_based_timeouts")->container();

			for (int x = 0; x < nbto->size(); x++) {
				ObjectContainer* o = nbto->get(x)->container();

				TimeoutConfigurationPtr nbtp = TimeoutConfigurationPtr(new TimeoutConfiguration());
				nbtp->id = o->get("id")->string();
				nbtp->timeout = o->get("timeout")->number();

				SFwallCore::AliasDb* aliases = System::getInstance()->getFirewall()->aliases;
				ObjectContainer* networks = o->get("networks")->container();

				for(int y = 0; y < networks->size(); y++){
					SFwallCore::AliasPtr network = aliases->get(networks->get(y)->string());
					if(network){
						nbtp->networks.push_back(network);
					}
				}

				if(o->has("groups")){
					ObjectContainer* groups = o->get("groups")->container();
					for(int y = 0; y < groups->size(); y++){
						GroupPtr group = System::getInstance()->getGroupDb()->getGroup(groups->get(y)->number());
						if(group){
							nbtp->groups.push_back(group);
						}
					}
				}

				add_authentication_timeout_configuration(nbtp);
			}
		}
	}

	this->releaseLock();
	return true;
}

std::list<TimeoutConfigurationPtr> HostDiscoveryService::get_authentication_timeout_configurations(){
	return timeout_configuration_store->get_authentication_timeout_configurations();
}

void HostDiscoveryService::remove_authentication_timeout_configuration(TimeoutConfigurationPtr conf){
	timeout_configuration_store->remove_authentication_timeout_configuration(conf);
}

void HostDiscoveryService::add_authentication_timeout_configuration(TimeoutConfigurationPtr conf){
	timeout_configuration_store->add_authentication_timeout_configuration(conf);
}

/* Persisted session methods */
void HostDiscoveryService::add_persisted_authentication_session(std::string mac_address, std::string username){
	persisted_session_store->add_persisted_authentication_session(mac_address, username);
}

void HostDiscoveryService::remove_persisted_authentication_session(std::string mac_address, std::string username){
	persisted_session_store->remove_persisted_authentication_session(mac_address, username);
}

std::list<PersistedSessionPtr> HostDiscoveryService::get_persisted_authentication_sessions(){
	return persisted_session_store->get_persisted_authentication_sessions();
}

