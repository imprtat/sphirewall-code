#ifndef VERSION_NOTIFIER_H
#define VERSION_NOTIFIER_H

#include "Core/Cron.h"

class VersionNotifier : public CronJob {
	public:
		VersionNotifier();
		~VersionNotifier(){}
		void run();
	private:
		std::string endpoint;
		std::string uuid;
};

#endif
