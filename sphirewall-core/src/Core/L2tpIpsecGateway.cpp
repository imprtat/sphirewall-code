#include <iostream>
#include <sstream>

using namespace std;

#include "Core/L2tpIpsecGateway.h"
#include "Utils/FileUtils.h"
#include "Core/vici.h"
#include "Core/System.h"
#include "Auth/Group.h"
#include "Auth/GroupDb.h"
#include "Core/Event.h"
#include "Utils/LinuxUtils.h"

/* For the firewall exceptions */
#include "SFwallCore/Packet.h"
#include "SFwallCore/Firewall.h"

bool IPSecL2TPGateway::efh__enabled(){
	return auto_start && auto_configure_firewall && resolved_local_device;
}

int IPSecL2TPGateway::efh__filter_handle(SFwallCore::Packet* packet){
	if(packet->type() != IPV4){
		return SFwallCore::EXTERNAL_HANDLER__IGNORE;
	}

	SFwallCore::PacketV4* ipv4 = (SFwallCore::PacketV4*) packet;
	if(packet->getProtocol() == SFwallCore::UDP){
		int dst_port = packet->getDstPort();
		int src_port = packet->getSrcPort();

		//If the port matches IKE or L2TP
		if(dst_port == 500 || dst_port == 4500 || src_port == 500 || src_port == 4500 || src_port == 1701 || dst_port == 1701){
			if(ipv4->getSrcIp() == resolved_local_device->get_primary_ipv4_address() || ipv4->getDstIp() == resolved_local_device->get_primary_ipv4_address()){
				return SFwallCore::EXTERNAL_HANDLER__ALLOW;
			}
		}
	}

	/* If NAT-T is not in action, then we need to check for ESP packets */
	if(auto_start && packet->getProtocol() == SFwallCore::ESP && (ipv4->getSrcIp() == resolved_local_device->get_primary_ipv4_address()|| ipv4->getDstIp() == resolved_local_device->get_primary_ipv4_address())){
		return SFwallCore::EXTERNAL_HANDLER__ALLOW;
	}

	return SFwallCore::EXTERNAL_HANDLER__IGNORE;	
}

void IPSecL2TPGateway::refresh(){
	terminate();
	if(auto_start){
		initiate();
	}
}

void IPSecL2TPGateway::initiate(){
        if(!FileUtils::checkExists("/var/run/charon.pid")){
                Logger::instance()->log("sphirewalld.vpn.ipsec.init_service", INFO, "Starting ipsec services");

                string error_string;
                LinuxUtils::exec("ipsec start", error_string);
                Logger::instance()->log("sphirewalld.vpn.ipsec.init_service", INFO, error_string.c_str());
		sleep(2);
        }

	system("killall -9 -e xl2tpd"); /* Because debian is gay and init.d scripts might have started xl2tpd, we must kill it*/
	Logger::instance()->log("sphirewalld.vpn.ipsec.initiate", INFO, "initiating l2tp ipsec gateway");

	/* Load the interface entity */
	resolved_local_device = System::getInstance()->get_interface_manager()->get_interface(local_device);
        if(!resolved_local_device){
                Logger::instance()->log("sphirewalld.vpn.l2tp", ERROR,
                        "could start client vpn, missing binding device '%s'", local_device.c_str());
                return;
        }

	__add_keys();
	__add_connection();
	__configure_xl2tp();
	__write_pppd_opts();

	sleep(2);
	process->reset_params();
	process->set_executable("/usr/sbin/xl2tpd");
	process->add_param("-D");
	process->add_param("-c");
	process->add_param(__get_xl2tp_opts_filename());
	process->start(true);

	System::getInstance()->getEventDb()->add(new Event(VPN_L2TP_IPSEC_START , EventParams()));
}

void IPSecL2TPGateway::terminate(){
	Logger::instance()->log("sphirewalld.vpn.ipsec.initiate", INFO, "destroying l2tp ipsec gateway");
	System::getInstance()->getEventDb()->add(new Event(VPN_L2TP_IPSEC_STOP , EventParams()));

	__unconfigure_xl2tp();
	process->stop();
}

std::string IPSecL2TPGateway::__get_pppd_opts_filename(){
	stringstream ss; ss << "/etc/ppp/" << name << ".sphirewall.conf";
	return ss.str();
}

std::string IPSecL2TPGateway::__get_xl2tp_opts_filename(){
	stringstream ss; ss << "/etc/xl2tpd/" << name << ".sphirewall.conf";
	return ss.str();
}


void IPSecL2TPGateway::__write_xl2tp_opts(){
	stringstream ss;
	ss << "[lns " << name << "]\n";
	if(vpn_assign_ip){
		ss << "ip range=" << IP4Addr::ip4AddrToString(vpn_remote_ip_range_start) << "-" << IP4Addr::ip4AddrToString(vpn_remote_ip_range_end) << endl;
		ss << "local ip=" << IP4Addr::ip4AddrToString(vpn_local_ip) << endl;
		ss << "assign ip=yes" << endl;
	}

	/* Deal with authentication modes */
	if(user_authentication_mode == L2TP_AUTHMODE_NONE){
		ss << "require authentication=no" << endl;

	}else if(user_authentication_mode == L2TP_AUTHMODE_SPHIREWALL){
		ss << "require authentication=yes" << endl;
		ss << "require pap=yes" << endl;

	}else if(user_authentication_mode == L2TP_AUTHMODE_CHAP){
		ss << "require authentication=yes" << endl;
		ss << "require chap=yes" << endl;
		ss << "refuse pap=yes" << endl;

	}else if(user_authentication_mode == L2TP_AUTHMODE_PAP){
		ss << "require authentication=yes" << endl;
		ss << "refuse chap=yes" << endl;
		ss << "require pap=yes" << endl;
	}

	ss << "name=" << name << endl;
	ss << "ppp debug=yes" << endl;
	ss << "pppoptfile=" << __get_pppd_opts_filename() << endl;
	ss << "length bit=yes" << endl;
	ss << "lac=0.0.0.0-255.255.255.255" << endl;

	FileUtils::write(__get_xl2tp_opts_filename().c_str(), ss.str(), true);
}

void IPSecL2TPGateway::__write_pppd_opts(){
	stringstream ss;
	if(user_authentication_mode == L2TP_AUTHMODE_NONE){
		ss << "noauth\n";
	}else{
		ss << "auth\n";
	}

	if(provide_dns && dns_ns1.size() > 0){
		ss << "ms-dns " << dns_ns1 << endl;
	}

	if(provide_dns && dns_ns2.size() > 0){
		ss << "ms-dns " << dns_ns2 << endl;
	}

	ss << "mtu 1200\n";
	ss << "mru 1000\n";
	ss << "crtscts\n";
	ss << "hide-password\n";
	ss << "modem\n";
	ss << "name " << name << "\n";
	ss << "proxyarp\n";
	ss << "lcp-echo-interval 30\n";
	ss << "lcp-echo-failure 4\n";

	if(user_authentication_mode == L2TP_AUTHMODE_SPHIREWALL){
		ss << "plugin sphirewall_ppp.so realm_key " << name << " authenticate 1\n";
	}else{
		ss << "plugin sphirewall_ppp.so realm_key " << name << " authenticate 0\n";
	}

	FileUtils::write(__get_pppd_opts_filename().c_str(), ss.str(), true);
}

void IPSecL2TPGateway::__configure_xl2tp(){
	__write_pppd_opts();
	__write_xl2tp_opts();
}


void IPSecL2TPGateway::__unconfigure_xl2tp(){
}

void IPSecL2TPGateway::__add_connection(){
	ViciClient* client = new ViciClient();
	client->init();

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "load-conn";
	request->message = new ViciMessage();
	request->message->set_section(name, new ViciMessage());

	list<std::string> local_addrs; local_addrs.push_back(IP4Addr::ip4AddrToString(resolved_local_device->get_primary_ipv4_address()));
	list<std::string> remote_addrs; remote_addrs.push_back("%any");

	request->message->get_section(name)->set_value("version", "0");
	request->message->get_section(name)->set_list("local_addrs", local_addrs);
	request->message->get_section(name)->set_list("remote_addrs", remote_addrs);

	request->message->get_section(name)->set_section("local-auth1", new ViciMessage());
	request->message->get_section(name + ".local-auth1")->set_value("auth", "psk");
	request->message->get_section(name + ".local-auth1")->set_value("id", "172.16.44.23");

	request->message->get_section(name)->set_section("remote-auth1", new ViciMessage());
	request->message->get_section(name + ".remote-auth1")->set_value("auth", "psk");

	list<std::string> local_ts; local_ts.push_back("dynamic[udp/l2f]");
	list<std::string> remote_ts; remote_ts.push_back("dynamic[udp]");

	request->message->get_section(name)->set_section("children", new ViciMessage());
	request->message->get_section(name)->get_section("children")->set_section(name, new ViciMessage());
	request->message->get_section(name)->get_section("children")->get_section(name)->set_list("local_ts", local_ts);
	request->message->get_section(name)->get_section("children")->get_section(name)->set_list("remote_ts", remote_ts);
	request->message->get_section(name)->get_section("children")->get_section(name)->set_value("mode", "transport");

	client->request(request);
	client->destroy();
	delete client;
	delete request;
}

void IPSecL2TPGateway::__add_keys(){
	ViciClient* client = new ViciClient();
	client->init();

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "load-shared";
	request->message = new ViciMessage();
	request->message->set_value("type", "ike");
	request->message->set_value("data", secret_key);
	list<string> owners;
	owners.push_back(IP4Addr::ip4AddrToString((resolved_local_device->get_primary_ipv4_address())));
	owners.push_back("%any");
	request->message->set_list("owners", owners);

	client->request(request);
	ViciResponse* response = client->response();

	client->destroy();
	delete client;
	delete request;
	delete response;
}

bool IPSecL2TPGateway::load(ConfigurationManager* configuration){
	if (configuration->has("vpn:l2tp")) {
		ObjectContainer *root = configuration->getElement("vpn:l2tp");
		IPSecL2TPGateway* instance = new IPSecL2TPGateway();

		auto_start= root->get("auto_start")->boolean();
		auth_mode= root->get("auth_mode")->number();
		secret_key= root->get("secret_key")->string();
		local_device = root->get("local_device")->string();

		vpn_assign_ip = root->get("vpn_assign_ip")->boolean();
		vpn_remote_ip_range_start = IP4Addr::stringToIP4Addr(root->get("vpn_remote_ip_range_start")->string());
		vpn_remote_ip_range_end = IP4Addr::stringToIP4Addr(root->get("vpn_remote_ip_range_end")->string());
		vpn_local_ip = IP4Addr::stringToIP4Addr(root->get("vpn_local_ip")->string());

		local_device = root->get("local_device")->string();
		user_authentication_mode = root->get("user_authentication_mode")->number();

		provide_dns = root->get("provide_dns")->boolean();
		dns_ns1 = root->get("dns_ns1")->string();
		dns_ns2 = root->get("dns_ns2")->string();

		ObjectContainer *allowed_users_groups_arr = root->get("allowed_users_groups")->container();
		for (int y = 0; y < allowed_users_groups_arr->size(); y++) {
			GroupPtr group = System::getInstance()->getGroupDb()->getGroup(allowed_users_groups_arr->get(y)->number());
			if(group){
				allowed_users_groups.push_back(group);
			}
		}

		if(auto_start){
			initiate();
		}
	}

	return true;
}

void IPSecL2TPGateway::save(ConfigurationManager* configuration){
	ObjectContainer *o = new ObjectContainer(CREL);

	o->put("auto_start", new ObjectWrapper((bool) auto_start));
	o->put("auth_mode", new ObjectWrapper((double) auth_mode));
	o->put("secret_key", new ObjectWrapper((string) secret_key));

	o->put("vpn_assign_ip", new ObjectWrapper((bool) vpn_assign_ip));
	o->put("vpn_remote_ip_range_start", new ObjectWrapper((string) IP4Addr::ip4AddrToString(vpn_remote_ip_range_start)));
	o->put("vpn_remote_ip_range_end", new ObjectWrapper((string) IP4Addr::ip4AddrToString(vpn_remote_ip_range_end)));
	o->put("vpn_local_ip", new ObjectWrapper((string) IP4Addr::ip4AddrToString(vpn_local_ip)));
	o->put("user_authentication_mode", new ObjectWrapper((double) user_authentication_mode));
	o->put("local_device", new ObjectWrapper((string) local_device));

	o->put("provide_dns", new ObjectWrapper((bool) provide_dns));
	o->put("dns_ns1", new ObjectWrapper((string) dns_ns1));
	o->put("dns_ns2", new ObjectWrapper((string) dns_ns2));

	ObjectContainer *allowed_users_groups_arr = new ObjectContainer(CARRAY);
	for(GroupPtr group : allowed_users_groups){
		allowed_users_groups_arr->put(new ObjectWrapper((double) group->getId()));
	}

	o->put("allowed_users_groups", new ObjectWrapper(allowed_users_groups_arr));

	configuration->holdLock();
	configuration->setElement("vpn:l2tp", o);
	configuration->save();
	configuration->releaseLock();

}
