#include <iostream>
#include <gtest/gtest.h>

using namespace std;

#include "Core/HostDiscoveryService.h"
#include "Core/Config.h"
#include "Core/Cron.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/TestFactory.h"
#include "test.h"

TEST(HostDiscoveryService, ipv4_update_get) {
	MockFactory *tester = new MockFactory();
	HostDiscoveryService *arp = tester->givenArp();
	SFwallCore::Packet *packet = SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 80, 12333);
	packet->setHw("aa:bb:cc:dd:ee:ff");
	arp->update(packet);

	EXPECT_TRUE(arp->get("192.168.1.1").compare("aa:bb:cc:dd:ee:ff") == 0);

	list<HostPtr> entries;
	arp->list(entries);
	EXPECT_TRUE(entries.size() == 1);
	EXPECT_TRUE(arp->size() == 1);
}

TEST(HostDiscoveryService, ipv4_multiple) {
	MockFactory *tester = new MockFactory();
	HostDiscoveryService *arp = tester->givenArp();

	SFwallCore::Packet *packet = SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 80, 12333);
	SFwallCore::Packet *packet2 = SFwallCore::PacketBuilder::createTcp("192.168.1.12", "10.1.1.1", 80, 12333);
	packet->setHw("aa:bb:cc:dd:ee:ff");
	packet2->setHw("aa:bb:cc:dd:ee:f1");
	arp->update(packet);
	arp->update(packet2);

	EXPECT_TRUE(arp->get("192.168.1.1").compare("aa:bb:cc:dd:ee:ff") == 0);
	EXPECT_TRUE(arp->get("192.168.1.12").compare("aa:bb:cc:dd:ee:f1") == 0);

	list<HostPtr> entries;
	arp->list(entries);
	EXPECT_TRUE(entries.size() == 2);
	EXPECT_TRUE(arp->size() == 2);
}

TEST(HostDiscoveryService, ipv6_update_get) {
	MockFactory *tester = new MockFactory();
	HostDiscoveryService *arp = tester->givenArp();
	SFwallCore::Packet *packet = SFwallCore::PacketBuilder::createTcpV6("2607:f0d0:2001:a::1", "2607:f0d0:2001:a::2", 80, 12333);
	packet->setHw("aa:bb:cc:dd:ee:ff");
	arp->update(packet);

	EXPECT_TRUE(arp->get("2607:f0d0:2001:a::1").compare("aa:bb:cc:dd:ee:ff") == 0);

	list<HostPtr> entries;
	arp->list(entries);
	EXPECT_TRUE(entries.size() == 1);
	EXPECT_TRUE(arp->size() == 1);
}


