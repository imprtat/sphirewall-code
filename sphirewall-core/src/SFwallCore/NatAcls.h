/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NAT_ACLS_H
#define NAT_ACLS_H

#define UNSET (unsigned int) -1

#include "SFwallCore/Criteria.h"
#include <boost/unordered_set.hpp>
namespace SFwallCore {
	class Packet;

	class PortForwardingRule {
		public:
			PortForwardingRule() :
				forwardingDestination(UNSET),
				forwardingDestinationPort(UNSET),
				enabled(false) {}

			std::string id;
			std::list<Criteria*> criteria;

			unsigned int forwardingDestination;
			unsigned int forwardingDestinationPort;
			bool enabled;

			bool match(SFwallCore::Packet *packet);
	};

	class MasqueradeRule {
		public:
			MasqueradeRule() :
				enabled(false),
				natTargetIp(UNSET)
			{}

			in_addr_t getUsableNatTargetIp() {
				if (natTargetDevicePtr) {
					return natTargetDevicePtr->get_primary_ipv4_address();
				}
				else if (natTargetIp) {
					return natTargetIp;
				}

				return -1;
			}
	
			std::list<Criteria*> criteria;
	
			std::string natTargetDevice;
			InterfacePtr destinationDevicePtr;
			InterfacePtr natTargetDevicePtr;
			unsigned int natTargetIp;
			std::string id;
			bool enabled;

			bool match(SFwallCore::Packet *packet);
	};

	typedef boost::shared_ptr<PortForwardingRule> PortForwardingRulePtr;
	typedef boost::shared_ptr<MasqueradeRule> MasqueradeRulePtr;

	enum {
		DISABLED = -1, SINGLE=0, AUTOWAN_MODE_LOADBALANCING = 1, FAILOVER = 2, TOR_GATEWAY=3
	};

	class AutowanInterface {
		public:
			int failover_index;
			std::string interface;

			InterfacePtr resolved_interface;
			std::list<Criteria*> criteria;
	};	
	typedef boost::shared_ptr<AutowanInterface> AutowanInterfacePtr;	

	class NatAclStore : public Configurable, public Lockable {
		public:
			class InterfaceChangeListener : public virtual IntMgrChangeListener {
				public:
					InterfaceChangeListener(NatAclStore *store) :
						store(store) {}

					void interface_change() {
						store->initRules();
					}
				private:
					NatAclStore *store;
			};


			NatAclStore(IntMgr *interfaceManager):
				interfaceManager(interfaceManager) {

				interfaceManager->register_change_listener(new InterfaceChangeListener(this));
			}
			NatAclStore(){}

			void addMasqueradeRule(MasqueradeRulePtr rule);
			void delMasqueradeRule(MasqueradeRulePtr rule);
			std::list<MasqueradeRulePtr> listMasqueradeRules();
			MasqueradeRulePtr getMasqueradeRule(std::string id);

			void addForwardingRule(PortForwardingRulePtr rule);
			void delPortForwardingRule(PortForwardingRulePtr rule);
			std::list<PortForwardingRulePtr> listPortForwardingRules();
			PortForwardingRulePtr getPortForwardingRule(std::string id);

			bool load();
			void save();

			MasqueradeRulePtr matchMasqueradeRule(SFwallCore::Packet *packet);
			PortForwardingRulePtr matchForwardingRule(SFwallCore::Packet *packet);

			void initRules();

			//Autowan code
			int get_autowan_mode();
			InterfacePtr get_autowan_single_interface(){
				return autowan_single_interface;
			}

			void set_autowan_single_interface(std::string interface){
				this->autowan_single_interface_str = interface;
			}

			void set_autowan_single_interface(InterfacePtr interface){
				this->autowan_single_interface = interface;
			}

			void set_autowan_mode(int mode);
			int determine_routing_fwmark(SFwallCore::Packet* incomming_packet);
			bool determine_autowan_matches(SFwallCore::Packet* incomming_packet);
			bool determine_automatic_tor_gateway(SFwallCore::Packet* incomming_packet);
			std::vector<AutowanInterfacePtr>& get_autowan_rules();
			AutowanInterfacePtr get_autowan_rule_interface(std::string interface){
				for(AutowanInterfacePtr i : get_autowan_rules()){
					if(i->interface.compare(interface) == 0){
						return i;
					}
				}
				return AutowanInterfacePtr();
			}

                        AutowanInterfacePtr get_autowan_rule_interface(InterfacePtr target){
                                for(AutowanInterfacePtr i : get_autowan_rules()){
                                        if(i->resolved_interface == target){
                                                return i;
                                        }
                                }
                                return AutowanInterfacePtr();
                        }

			void del_autowan_rule_interface(std::string interface){
				std::vector<AutowanInterfacePtr>::iterator iter;
				for(iter = autowan_rules.begin(); iter != autowan_rules.end(); iter++){
					AutowanInterfacePtr i = (*iter);
					if(i->interface.compare(interface) == 0){
						autowan_rules.erase(iter);
						return;
					}
				}
			}

			const char* getConfigurationSystemName(){
				return "Forwarding and Filtering system";
			}

		private:
			std::list<MasqueradeRulePtr> masqueradeRules;
			std::list<PortForwardingRulePtr> portFowardingRules;

			vector<AutowanInterfacePtr> autowan_rules;
			std::set<InterfacePtr> autowan_active_interfaces;

			InterfacePtr autowan_single_interface;
			std::string autowan_single_interface_str;
			int autowan_load_balancing_device_pool_last;
			int autowan_mode;

			IntMgr* interfaceManager;
	};
};

#endif
