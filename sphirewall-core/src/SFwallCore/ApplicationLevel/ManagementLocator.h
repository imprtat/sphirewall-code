#ifndef MANAGEMENT_LOCATOR_H 
#define MANAGEMENT_LOCATOR_H 

#include "SFwallCore/ApplicationLevel/FilterHandler.h"

namespace SFwallCore {
        class Packet;
        class Connection;
        class ConnectionIp;

        class ManagementLocator : public ApplicationLayerFilterHandler, public Configurable {
                public:
                        ManagementLocator();

                        virtual void process(Connection *conn, Packet *packet);
                        bool matchesIpCriteria(Connection *conn, Packet *packet);
                        bool enabled() {
				return MANAGEMENT_LOCATOR_ENABLED == 1;
			}

                        static int MANAGEMENT_LOCATOR_ENABLED;

                        void save();
                        bool load();
                        const char* getConfigurationSystemName() {
                                return "Management Locator";
                        }
        };
};

#endif
