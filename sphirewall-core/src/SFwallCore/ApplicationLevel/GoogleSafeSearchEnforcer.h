/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GOOGLE_SAFE_SEARCH_ENFORCER_H 
#define GOOGLE_SAFE_SEARCH_ENFORCER_H 

#include "SFwallCore/ApplicationLevel/FilterHandler.h"

namespace SFwallCore {
	class Packet;
	class Connection;
	class GoogleSafeSearchEngine : public ApplicationLayerFilterHandler {
		public:
			GoogleSafeSearchEngine(ConfigurationManager *config,std::string from = "www.google.co", std::string to = "nosslsearch.google.com");
			virtual void process(Connection *conn, Packet *packet);
			bool matchesIpCriteria(Connection *conn, Packet *packet);
			bool enabled();

			static bool requestNeedsRewrite(unsigned char *request);

		private:
			ConfigurationManager *config;
			std::string from;
			std::string to;
	};
};
#endif
