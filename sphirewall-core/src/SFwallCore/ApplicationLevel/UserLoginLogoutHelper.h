#ifndef USER_LOGIN_LOGOUT_HELPER_H 
#define USER_LOGIN_LOGOUT_HELPER_H 

#include "SFwallCore/ApplicationLevel/FilterHandler.h"

namespace SFwallCore {
        class Packet;
        class Connection;
        class ConnectionIp;

        class UserLoginLogoutHelper : public ApplicationLayerFilterHandler, public Configurable {
                public:
                        UserLoginLogoutHelper();

                        virtual void process(Connection *conn, Packet *packet);
                        bool matchesIpCriteria(Connection *conn, Packet *packet);
                        bool enabled() {
				return LOGIN_LOGOUT_HELPER_ENABLED == 1;
			}

                        static int LOGIN_LOGOUT_HELPER_ENABLED;

                        void save();
                        bool load();
                        const char* getConfigurationSystemName() {
                                return "UserLoginLogoutHelper";
                        }
        };
};

#endif
