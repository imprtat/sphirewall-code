#ifndef SIGNATURE_FILTER_H
#define SIGNATURE_FILTER_H

#include <boost/shared_ptr.hpp>
#include <boost/regex.hpp>
#include <list>
#include "Core/ConfigurationManager.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Alias.h"

namespace SFwallCore {
	class Packet;
	class Criteria;
	class Signature;
        typedef boost::shared_ptr<Signature> SignaturePtr;

	class Signature {
		public:
			Signature(){}
			virtual ~Signature(){}

			virtual std::string type() = 0;
			virtual std::string id(){
				return type();
			}
			virtual std::string name() = 0;
			virtual std::string description() = 0;
			virtual std::string category() = 0;
			
			virtual bool matches(Connection* connection, Packet* packet) = 0;
			virtual void refresh(){}
			virtual bool is_category() {
				return false;
			}

			virtual bool requires_http() const {
				return false;
			}

			virtual bool requires_data() const {
				return false;
			}

			SignaturePtr resolved_category;
	};	

	class BitTorrentSignature: public Signature {
		public:
			~BitTorrentSignature(){}
			std::string type(){
				return "signatures.bittorrent";
			};

			std::string name(){
				return "Bittorrent peer traffic";
			};

			std::string description(){
				return "Bit torrent";
			};

			bool matches(Connection* connection, Packet* packet);
			std::string category() {
				return "sphirewall.application.p2p";
			}

                        bool requires_data() const {
                                return true;
                        }
	};

	class CriteriaSet {
		public:
			~CriteriaSet();
			list<Criteria*> criteria;
	};

	typedef boost::shared_ptr<CriteriaSet> CriteriaSetPtr;

	class DynamicSignature : public Signature {
		public:
			DynamicSignature(){
				this->exclusive_set = false;
				this->is_category_flag = false;
			}
			~DynamicSignature(){}
			std::string type(){
				return "dynamic";
			};

			std::string id(){
				return uniqueId;
			}			

			std::string name(){
				return userDefinedName;
			}

			std::string description(){
				return userDefinedDescription;
			};

			std::string category(){
				return userDefinedCategory;
			};


			bool matches(Connection* connection, Packet* packet);

			std::string uniqueId;
			std::string userDefinedName;
			std::string userDefinedDescription;
			std::string userDefinedCategory;

			bool exclusive_set;
			bool exclusive() const {
				return exclusive_set;
			}

			std::list<CriteriaSetPtr> criteria;

			bool is_category_flag;
			bool is_category(){
				return is_category_flag;
			}

                        bool requires_http() const;
                        bool requires_data() const;
	};


	typedef boost::shared_ptr<Signature> SignaturePtr;
	class SignatureStoreChangeHandler {
		public:
			virtual void signature_store_changed() = 0;
	};

	class SignatureStore : public virtual Lockable {
		public:
			SignatureStore();
			SignaturePtr get(std::string key);
			std::list<SignaturePtr> available();

			void add(SignaturePtr signature);
			void remove(SignaturePtr signature);

			bool import_signatures();
			void load_categorys_and_metadata();
			void register_change_listener(SignatureStoreChangeHandler* handler){
				change_listeners.push_back(handler);
			}

			bool is_enabled();
			std::string get_database_url();

		private:
			std::list<SignaturePtr> availableSignatures;
			void __load_signatures(ObjectContainer* object);
			std::list<SignatureStoreChangeHandler*> change_listeners;
	};	

        class SignatureStoreUpdateCronjob : public CronJob {
                public:
                        SignatureStoreUpdateCronjob(SignatureStore* store) :
                                CronJob(60 * 60 * 24, "APPLICATION_SIGNATURE_UPDATE_CRON", true),
                                store(store) {
                                }

                        void run(){
                                store->import_signatures();
                        }

                private:
                        SignatureStore* store;
        };


};

#endif
