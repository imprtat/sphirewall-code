#include <iostream>

#include "Core/System.h"
#include "Core/ConfigurationManager.h"

#include "SFwallCore/State.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"
#include "SFwallCore/ApplicationLevel/UserLoginLogoutHelper.h"

int SFwallCore::UserLoginLogoutHelper::LOGIN_LOGOUT_HELPER_ENABLED= 1;

SFwallCore::UserLoginLogoutHelper::UserLoginLogoutHelper(){
	System::getInstance()->config.getRuntime()->loadOrPut("LOGIN_LOGOUT_HELPER_ENABLED", &LOGIN_LOGOUT_HELPER_ENABLED);
}

void SFwallCore::UserLoginLogoutHelper::process(Connection *conn, Packet *packet){
	if (conn->getApplicationInfo()->http_hostNameSet) {
		std::string host = conn->getApplicationInfo()->http_hostName;
		if (host.compare("autologin.linewize.net") == 0) {
			conn->getApplicationInfo()->afVerdictApplied = true;
			conn->setRewriteRule(new HttpRedirectRewrite("http://login.linewize.net/login"));
			return;
		}

                if (host.compare("autologout.linewize.net") == 0) {
                        conn->getApplicationInfo()->afVerdictApplied = true;
                        conn->setRewriteRule(new HttpRedirectRewrite("http://login.linewize.net/logout"));
                        return;
                }
	}
}

bool SFwallCore::UserLoginLogoutHelper::matchesIpCriteria(Connection *conn, Packet *packet){
	if (conn->getProtocol() != TCP) {
		return false;
	}

	if (((TcpConnection *) conn)->getState()->state != TCPUP) {
		return false;
	}

	if (conn->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
		return false;
	}

	return true;
}

void SFwallCore::UserLoginLogoutHelper::save(){
}

bool SFwallCore::UserLoginLogoutHelper::load(){
	return true;
}
