/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CAPTURE_PORTAL_H 
#define CAPTURE_PORTAL_H 

namespace SFwallCore {
        class TcpConnection;
        class Packet;
        class Connection;
        class ConnectionIp;
        class Alias;
        class AliasDb;

        class CapturePortalEngine: public ApplicationLayerFilterHandler, public Configurable {
                public:
                        CapturePortalEngine(AliasDb *aliases);

                        virtual void process(Connection *conn, Packet *packet);
                        bool matchesIpCriteria(Connection *conn, Packet *packet);
                        bool enabled();

                        static int CAPTURE_PORTAL_FORCE; /* DEPRECATED, mode replaces this runtime var: 0 = disabled, 1 = selected networks, 2 = all networks/devices */
                        static int REWRITE_PROXY_REQUESTS_ENABLED;
                        static int CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION;
                        static int CAPTURE_PORTAL_DMZ_MODE_ENABLED;

                        void save();
                        bool load();
                        const char* getConfigurationSystemName() {
                                return "Capture portal engine";
                        }

                        long queries;
                        long hits;

                        std::list<AliasPtr> provided_inclusions_aliases;
                        std::list<AliasPtr> provided_exclusions_aliases;
                        std::list<std::string> provided_inclusions;
                        std::list<std::string> provided_exclusions;
			int endpoint;
			int mode;
			std::string cp_url;

			void initopts();
                private:
                        AliasDb *aliases;

			/* Items in the above provided inclusions/exclusions will be mapped into objects here on load/reload */	
                        std::list<AliasPtr> optimised_inclusions;
                        std::list<AliasPtr> optimised_exclusions;

                        bool isWebsiteInAliases(const AliasPtr& alias, const std::string &website);
			bool isMacAddressInAlias(const std::string &mac);
			bool should_be_excluded(Connection *conn, Packet *packet);
			bool should_be_included(Connection *conn, Packet *packet);
			void send_connection_to_portal(Connection *conn, Packet *packet);
        };
};

#endif
