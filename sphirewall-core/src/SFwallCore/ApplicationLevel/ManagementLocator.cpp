#include <iostream>

#include "Core/System.h"
#include "Core/ConfigurationManager.h"

#include "SFwallCore/State.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"
#include "SFwallCore/ApplicationLevel/ManagementLocator.h"

int SFwallCore::ManagementLocator::MANAGEMENT_LOCATOR_ENABLED = 1;

SFwallCore::ManagementLocator::ManagementLocator(){
	System::getInstance()->config.getRuntime()->loadOrPut("MANAGEMENT_LOCATOR_ENABLED", &MANAGEMENT_LOCATOR_ENABLED);
}

void SFwallCore::ManagementLocator::process(Connection *conn, Packet *packet){
	if (conn->getApplicationInfo()->http_hostNameSet) {
		std::string host = conn->getApplicationInfo()->http_hostName;
		if (host.compare("mydevice.sphirewall.net") == 0 || host.compare("mydevice.linewize.net") == 0) {
			conn->getApplicationInfo()->afVerdictApplied = true;

			//Determine IP address: If this devie has multiple addresses, then we have an issue
			for (InterfacePtr device : System::getInstance()->get_interface_manager()->get_all_interfaces()){
				if(device->get_primary_ipv4_address() != 0){
					std::string potentialIp = IP4Addr::ip4AddrToString(device->get_primary_ipv4_address());
					if(potentialIp.compare("127.0.0.1") != 0 && potentialIp.size() > 0) {
						stringstream url; url << "http://" << potentialIp << ":5001";
						conn->setRewriteRule(new HttpRedirectRewrite(url.str()));
						return;
					}
				}
			}	

			return;
		}
	}
}

bool SFwallCore::ManagementLocator::matchesIpCriteria(Connection *conn, Packet *packet){
	if (conn->getProtocol() != TCP) {
		return false;
	}

	if (((TcpConnection *) conn)->getState()->state != TCPUP) {
		return false;
	}

	if (conn->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
		return false;
	}

	return true;
}

void SFwallCore::ManagementLocator::save(){
}

bool SFwallCore::ManagementLocator::load(){
	return true;
}
