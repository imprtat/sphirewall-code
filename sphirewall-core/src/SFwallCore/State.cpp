/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "SFwallCore/State.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Firewall.h"

SFwallCore::TcpTrackable::TcpTrackable(const SFwallCore::TcpPacket *tcpPacket) {
	if (tcpPacket->synSet()) {
		state.state = SYNSYNACK;
	}
	else if (tcpPacket->rstSet() || tcpPacket->finSet()) {
		state.state = TIME_WAIT;
	}
	else {
		state.state = TCPUP;
	}
}

bool SFwallCore::TcpTrackable::hasExpired(int idleTime) {
	if (state.state == TCPUP && idleTime >= Firewall::TCP_CONNECTION_TIMEOUT_THESHOLD)
		return true;

	if (state.state == SYNSYNACK && idleTime >= Firewall::SYN_SYNACK_WAIT_TIMEOUT)
		return true;

	if (state.state == SYNACKACK && idleTime >= Firewall::SYN_ACKACK_TIMEOUT)
		return true;

	if (state.state == FIN_WAIT && idleTime >= Firewall::FIN_WAIT_TIMEOUT)
		return true;

	if (state.state == TIME_WAIT && idleTime >= Firewall::TIME_WAIT_TIMEOUT)
		return true;

	if (state.state == CLOSE_WAIT && idleTime >= Firewall::CLOSE_WAIT_TIMEOUT)
		return true;

	if (state.state == LAST_ACK && idleTime >= Firewall::LAST_ACK_TIMEOUT)
		return true;

	if (state.state == RESET && idleTime >= Firewall::TIME_WAIT_TIMEOUT)
		return true;

	return false;
}

void SFwallCore::TcpTrackable::calcState(const SFwallCore::TcpPacket *tcpPacket) {
	/*Determine which side we are calculating ---->*/

	switch (state.state) {
	case SYNACKACK: {
		if (tcpPacket->ackSet())
			state.state = TCPUP;
	}
	break;

	case SYNSYNACK: {
		if (tcpPacket->synSet() && tcpPacket->ackSet()) {
			state.state = SYNACKACK; // SYN|ACK sent, awaiting ACK
		}
	}
	break;

	case TCPUP: {
		if (tcpPacket->finSet()) {
			state.state = FIN_WAIT;
			state.fin_wait_seq = tcpPacket->getSeq() + 1;
		}
	}
	break;

	case FIN_WAIT: {
		if (tcpPacket->ackSet() && tcpPacket->getAckSeq() == state.fin_wait_seq) {
			state.state = CLOSE_WAIT;
		}
	}

	case CLOSE_WAIT: {
		if (tcpPacket->finSet()) {
			state.state = LAST_ACK;
			state.fin2_wait_seq = tcpPacket->getSeq() + 1;
		}
	}
	break;

	case LAST_ACK: {
		if (tcpPacket->ackSet() && tcpPacket->getAckSeq() == state.fin2_wait_seq) {
			state.state = TIME_WAIT;
		}
	}
	break;

	case TIME_WAIT: {
		if (tcpPacket->rstSet()) {
			state.state = CLOSED;
		}
	}
	break;

	default:
		break;
	};

	if (tcpPacket->rstSet())
		state.state = RESET;

	//Gather some more information on this connection:
	if (tcpPacket->synSet())
		lastSyn = true;
	else
		lastSyn = false;

	if (tcpPacket->ackSet())
		lastAck = true;
	else
		lastAck = false;

	if (tcpPacket->rstSet())
		lastRst = true;
	else
		lastRst = false;

	if (tcpPacket->finSet())
		lastFin = true;
	else
		lastFin = false;

	//Get the seq numbers:
	lastAckSeq = tcpPacket->getAckSeq();
	lastSeq = tcpPacket->getSeq();
}

std::string SFwallCore::TcpState::echo() {

	switch (state) {
	case SYNSYNACK:
		return "NEW";
		break;

	case SYNACKACK:
		return "NEW";
		break;

	case TCPUP:
		return "ESTABLISHED";
		break;

	case FIN_WAIT:
		return "FIN_WAIT";
		break;

	case CLOSE_WAIT:
		return "CLOSE_WAIT";
		break;

	case LAST_ACK:
		return "LAST_ACK";
		break;

	case TIME_WAIT:
		return "TIME_WAIT";
		break;

	case CLOSED:
		return "CLOSED";
		break;

	case RESET:
		return "RESET";
		break;

	default:
		break;
	};

	return "";
}

