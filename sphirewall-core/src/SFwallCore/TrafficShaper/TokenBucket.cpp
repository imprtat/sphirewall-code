/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "SFwallCore/Firewall.h"
#include "SFwallCore/TrafficShaper/TokenBucket.h"
#include "Core/System.h"
#include "SFwallCore/Packet.h"

using namespace std;

TokenBucket::TokenBucket(long upload, long download, TokenFilterPtr f) : uploadRate(upload), downloadRate(download), filter(f) {
	gettimeofday(&t, NULL);
	lastPacketTime = time(NULL);
	downloadTokens = 0;
	uploadTokens = 0;
	invalidated = false;
	lock = new Lock();
}

void TokenBucket::rollBucket() {
	SqClient *sqClient = &System::getInstance()->getFirewall()->sqClient;

	lock->lock();
	bool uploadEmpty = uploadPacketQueue.empty();
	lock->unlock();

	if (!uploadEmpty) {
		lock->lock();
		TokenBucketPending* packet = uploadPacketQueue.front();
		lock->unlock();

		if (packet) {
			if (uploadTokens > packet->size) {
				if (sqClient->reinject(packet->id) != -1) {
					uploadTokens -= packet->size;

					lock->lock();
					uploadPacketQueue.pop();
					delete packet;
					lock->unlock();
				}
			}
			else {
				if (canFlip()) {
					flip();
				}
			}
		}
	}

	lock->lock();
	bool downloadEmpty = downloadPacketQueue.empty();
	lock->unlock();

	if (!downloadEmpty) {
		lock->lock();
		TokenBucketPending* packet = downloadPacketQueue.front();
		lock->unlock();

		if (packet) {
			if (downloadTokens > packet->size) {
				if (sqClient->reinject(packet->id) != -1) {
					downloadTokens -= packet->size;

					lock->lock();
					downloadPacketQueue.pop();
					delete packet;
					lock->unlock();
				}
			}
			else {
				if (canFlip()) {
					flip();
				}
			}
		}
	}
}

bool TokenBucket::canFlip() {
	timeval now;
	long elapsedMilli;

	gettimeofday(&now, NULL);

	elapsedMilli = (now.tv_sec - t.tv_sec) * 1000.0;
	elapsedMilli += (now.tv_usec - t.tv_usec) / 1000.0;

	if (elapsedMilli >= 1000) {
		return true;
	}

	return false;
}

bool TokenBucket::hasExpired() {
	if ((time(NULL) - lastPacketTime) > ((1000 * 60) * 5)) {
		return true;
	}

	return false;
}

void TokenBucket::flip() {
	gettimeofday(&t, NULL);

	uploadTokens = uploadRate;
	downloadTokens = downloadRate;
}

void TokenBucket::addPacketToQueue(TokenBucketPending* item) {
	lock->lock();

	if (item->download) {
		downloadPacketQueue.push(item);
	}
	else {
		uploadPacketQueue.push(item);
	}

	lastPacketTime = time(NULL);
	lock->unlock();
}

