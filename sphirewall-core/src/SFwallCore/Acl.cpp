/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <time.h>
#include <sys/types.h>
#include <sstream>
#include <map>
#include <fstream>
#include <string>

using namespace std;

#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Alias.h"
#include "Utils/Utils.h"
#include "Utils/StringUtils.h"
#include "Json//JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Utils/TimeWrapper.h"
#include "Core/System.h"
#include "SFwallCore/TimePeriods.h"
#include "Packet.h"
#include "Auth/User.h"
#include "SFwallCore/Criteria.h"

SFwallCore::Nat::Nat() : type(NO_NAT) {}

SFwallCore::ACLStore::ACLStore(UserDb *userdb, GroupDb *groupdb, SFwallCore::AliasDb *aliases, TimePeriodStore *tps)
	: userDb(userdb),
	  groupDb(groupdb),
	  aliases(aliases),
	  tps(tps) {
}

SFwallCore::PriorityAclStore::PriorityAclStore(UserDb *userdb, GroupDb *groupdb, SFwallCore::AliasDb *aliases, TimePeriodStore *tps)
	: userDb(userdb),
	  groupDb(groupdb),
	  aliases(aliases),
	  tps(tps) {
}

SFwallCore::PriorityAclStore::PriorityAclStore(){}

SFwallCore::ACLStore::ACLStore() {}

void SFwallCore::ACLStore::setInterfaceManager(IntMgr* interfaceManager) {
	this->interfaceManager = interfaceManager;
}

void SFwallCore::PriorityAclStore::setInterfaceManager(IntMgr* interfaceManager) {
        this->interfaceManager = interfaceManager;
}

std::vector<SFwallCore::FilterRulePtr> &SFwallCore::ACLStore::listFilterRules() {
	return entries;
}

std::vector<SFwallCore::PriorityRulePtr> &SFwallCore::PriorityAclStore::listPriorityRules() {
	return priorityQosRules;
}

SFwallCore::FilterRule::FilterRule()
{
	this->log = false;
	this->enabled = false;
	this->valid = false;
	this->count = 0;
	this->last_hit = 0;
	this->ignoreconntrack = false;
	this->ignore_application_layer_filters = false;
	this->action = 0;
	this->hits_per_minute = new FlexibleSecondIntervalSampler();
}

SFwallCore::PriorityRulePtr SFwallCore::PriorityAclStore::match_rule(Packet *packet) {
	if (packet == NULL) {
		return SFwallCore::PriorityRulePtr();
	}

	for (auto rule : priorityQosRules) {
		if (rule->supermatch(packet, groupDb)) {
			return rule;
		}
	}

	return SFwallCore::PriorityRulePtr();
}

SFwallCore::FilterRulePtr SFwallCore::ACLStore::match_rule(Packet *packet) {
	if (packet == NULL) {
		return SFwallCore::FilterRulePtr();
	}

	for (auto fr : entries) {
		if (fr->supermatch(packet, groupDb)) {
			fr->count++;
			fr->last_hit = time(NULL);
			fr->hits_per_minute->input(1);
			return fr;
		}
	}

	return SFwallCore::FilterRulePtr();
}


bool SFwallCore::FilterRule::isActive() {
	return enabled;
}

bool SFwallCore::FilterRule::valueInRange(int lower, int value, int higher) {
	return (value >= lower && value <= higher);
}

bool SFwallCore::FilterRule::supermatch(Packet *packet, GroupDb *groupDb) {
	if(!isActive()){
		return false;
	}

	for(auto c : criteria){
		if(!c->match(packet)){
			return false;
		}
	}

	return true;
}

bool SFwallCore::PriorityRule::supermatch(Packet *packet, GroupDb *groupDb) {
	if(!enabled){
		return false;
	}

	for(auto c : criteria){
		if(!c->match(packet)){
			return false;
		}
	}

	return true;
}


void SFwallCore::ACLStore::deserializeRule(ObjectContainer *source, SFwallCore::FilterRulePtr entry) {
	if (source->has("id")) {
		entry->id = source->get("id")->string();
	}

	if (source->has("enabled")) {
		entry->enabled = source->get("enabled")->boolean();
	}

	if(source->has("criteria")){
		ObjectContainer* criteria = source->get("criteria")->container();
		for(int y = 0; y < criteria->size(); y++){
			CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(criteria->get(y)->container()));
			if(c){
				entry->criteria.push_back(c);
			}
		}
	}
}

void SFwallCore::PriorityAclStore::deserializeRule(ObjectContainer *source, SFwallCore::PriorityRulePtr entry) {
	if (source->has("id")) {
		entry->id = source->get("id")->string();
	}

	if (source->has("enabled")) {
		entry->enabled = source->get("enabled")->boolean();
	}

	if(source->has("criteria")){
		ObjectContainer* criteria = source->get("criteria")->container();
		for(int y = 0; y < criteria->size(); y++){
			CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(criteria->get(y)->container()));
			if(c){
				entry->criteria.push_back(c);
			}
		}
	}
}

bool SFwallCore::PriorityAclStore::load() {
	priorityQosRules.clear();
	ObjectContainer *root;

	if (configurationManager->has("priorityAcls")) {
		root = configurationManager->getElement("priorityAcls");
		ObjectContainer *rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			PriorityRulePtr entry = PriorityRulePtr(new PriorityRule());

			ObjectContainer *source = rules->get(x)->container();
			deserializeRule(source, entry);

			entry->nice = source->get("nice")->number();
			loadAclEntry(entry, true);
		}
	}
	else {
		Logger::instance()->log("sphirewalld.firewall.acls", INFO, "Could not find any nat rules in configuration");
	}
	return true;
}

bool SFwallCore::ACLStore::load() {
	int loaded = 0;

	ObjectContainer *root;
	entries.clear();

	if (configurationManager->has("filterAcls")) {
		root = configurationManager->getElement("filterAcls");
		ObjectContainer *rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			FilterRulePtr entry = FilterRulePtr(new FilterRule());
			ObjectContainer *source = rules->get(x)->container();

			deserializeRule(source, entry);

			if (source->has("log")) {
				entry->log = source->get("log")->boolean();
			}

			if (source->has("action")) {
				entry->action = source->get("action")->number();
			}

			if (source->has("comment")) {
				entry->comment = source->get("comment")->string();
			}

			if (source->has("ignoreconntrack")) {
				entry->ignoreconntrack = source->get("ignoreconntrack")->boolean();
			}

			loadAclEntry(entry, true);
			loaded++;
		}
	}

	if (loaded == 0) {
		Logger::instance()->log("sphirewalld.firewall.acls", INFO, "No filter nat rules found, loading default accept all ruleset");
		FilterRulePtr def= FilterRulePtr(new FilterRule());
		def->action = SQ_ACCEPT;
		def->enabled = true;
		loadAclEntry(def);
	}

	return true;
}

void SFwallCore::ACLStore::serializeRule(ObjectContainer *rule, SFwallCore::FilterRulePtr target) {
	if (target->comment.size() > 0) {
		rule->put("comment", new ObjectWrapper((string) target->comment));
	}

	rule->put("id", new ObjectWrapper((string) target->id));
	ObjectContainer* criteria = new ObjectContainer(CARRAY);
	for(auto c: target->criteria){
		criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
	}
	rule->put("criteria", new ObjectWrapper(criteria));
}

void SFwallCore::PriorityAclStore::serializeRule(ObjectContainer *rule, SFwallCore::PriorityRulePtr target) {
        rule->put("id", new ObjectWrapper((string) target->id));
        ObjectContainer* criteria = new ObjectContainer(CARRAY);
        for(auto c: target->criteria){
                criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
        }
        rule->put("criteria", new ObjectWrapper(criteria));
}

void SFwallCore::PriorityAclStore::save() {
	int loaded = 0;

	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rules = new ObjectContainer(CARRAY);

	for (int x = 0; x < (int) priorityQosRules.size(); x++) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		serializeRule(rule, priorityQosRules[x]);
		rule->put("id", new ObjectWrapper((string) priorityQosRules[x]->id));
		rule->put("enabled", new ObjectWrapper((bool) priorityQosRules[x]->enabled));
		rule->put("nice", new ObjectWrapper((double) priorityQosRules[x]->nice));
		loaded++;

		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));

	configurationManager->holdLock();
	configurationManager->setElement("priorityAcls", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

void SFwallCore::ACLStore::save() {
	int loaded = 0;

	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rules = new ObjectContainer(CARRAY);

	for (int x = 0; x < (int) entries.size(); x++) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		serializeRule(rule, entries[x]);
		rule->put("id", new ObjectWrapper((string) entries[x]->id));
		rule->put("enabled", new ObjectWrapper((bool) entries[x]->enabled));
		rule->put("action", new ObjectWrapper((double) entries[x]->action));
		rule->put("ignoreconntrack", new ObjectWrapper((bool) entries[x]->ignoreconntrack));
		rule->put("log", new ObjectWrapper((bool) entries[x]->log));
		loaded++;

		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));

	configurationManager->holdLock();
	configurationManager->setElement("filterAcls", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

void SFwallCore::ACLStore::initLazyObjects(FilterRulePtr rule) {
	rule->valid = true;

	for(auto c : rule->criteria){
		c->refresh();
	}
}

void SFwallCore::PriorityAclStore::initLazyObjects(PriorityRulePtr rule) {
	rule->valid = true;

	for(auto c : rule->criteria){
		c->refresh();
	}
}

SFwallCore::PriorityRule::PriorityRule(): nice(1)
{}

void SFwallCore::ACLStore::movedown(FilterRulePtr rule) {
	int x = findPos(rule);

	if ((x+ 1) < (int) entries.size()) {
		FilterRulePtr a = entries[x];
		FilterRulePtr b = entries[x+ 1];

		entries[x+ 1] = a;
		entries[x]     = b;
	}
	save();
}

void SFwallCore::PriorityAclStore::movedown(PriorityRulePtr rule) {
	int x = findPos(rule);

	if ((x + 1) < (int) priorityQosRules.size()) {
		PriorityRulePtr a = priorityQosRules[x];
		PriorityRulePtr b = priorityQosRules[x + 1];

		priorityQosRules[x + 1] = a;
		priorityQosRules[x]     = b;
	}
	save();
}

void SFwallCore::ACLStore::moveup(FilterRulePtr rule) {
	int x = findPos(rule);

	if ((x - 1) >= 0) {
		FilterRulePtr a = entries[x];
		FilterRulePtr b = entries[x - 1];

		entries[x - 1] = a;
		entries[x]     = b;
	}
	save();
}

void SFwallCore::PriorityAclStore::moveup(PriorityRulePtr rule) {
	int x = findPos(rule);
	if ((x - 1) >= 0) {
		PriorityRulePtr a = priorityQosRules[x];
		PriorityRulePtr b = priorityQosRules[x - 1];

		priorityQosRules[x - 1] = a;
		priorityQosRules[x]     = b;
	}

	save();
}


int SFwallCore::ACLStore::findPos(FilterRulePtr rule) {
	holdLock();

	for (uint x = 0; x < entries.size(); x++) {
		FilterRulePtr target = entries[x];

		if (target == rule) {
			releaseLock();
			return x;
		}
	}

	releaseLock();
	return -1;
}

int SFwallCore::PriorityAclStore::findPos(PriorityRulePtr rule) {
	holdLock();

	for (uint x = 0; x < priorityQosRules.size(); x++) {
		PriorityRulePtr target = priorityQosRules[x];

		if (target == rule) {
			releaseLock();
			return x;
		}
	}

	releaseLock();
	return -1;
}


SFwallCore::FilterRulePtr SFwallCore::ACLStore::getRuleById(std::string id) {
	holdLock();

	for (auto rule : entries) {
		if (rule->id.compare(id) == 0) {
			releaseLock();
			return rule;
		}
	}

	releaseLock();
	return SFwallCore::FilterRulePtr();
}

SFwallCore::PriorityRulePtr SFwallCore::PriorityAclStore::getRuleById(std::string id) {
	holdLock();

	for (auto rule : priorityQosRules) {
		if (rule->id.compare(id) == 0) {
			releaseLock();
			return rule;
		}
	}

	releaseLock();
	return PriorityRulePtr();
}

void SFwallCore::ACLStore::InterfaceChangeListener::interface_change() {
	store->holdLock();

	for (uint x = 0; x < store->entries.size(); x++) {
		store->initLazyObjects(store->entries[x]);
	}

	store->releaseLock();
}

void SFwallCore::ACLStore::aliasRemoved(AliasPtr alias) {
}

void SFwallCore::PriorityAclStore::aliasRemoved(AliasPtr alias) {
}
	SFwallCore::ACLStore::InterfaceChangeListener::InterfaceChangeListener(SFwallCore::ACLStore *store)
: store(store)
{}

int SFwallCore::ACLStore::loadAclEntry(FilterRulePtr rule, bool add){
	initLazyObjects(rule);

	if(rule->id.size() == 0 || add){
		rule->id = StringUtils::genRandom();
		entries.push_back(rule);
	}
}

int SFwallCore::ACLStore::unloadAclEntry(FilterRulePtr rule){
	int pos = findPos(rule);
	if(pos != -1){
		entries.erase(entries.begin() + pos);
	}

	save();
}

int SFwallCore::PriorityAclStore::loadAclEntry(PriorityRulePtr rule, bool add){
	initLazyObjects(rule);
	if(rule->id.size() == 0 || add){
		rule->id = StringUtils::genRandom();
		priorityQosRules.push_back(rule);
	}
}

int SFwallCore::PriorityAclStore::unloadAclEntry(PriorityRulePtr rule){
	int pos = findPos(rule);
	if(pos != -1){
		priorityQosRules.erase(priorityQosRules.begin() + pos);
	}
	save();
}

void SFwallCore::ACLStore::sample(map<string, double> &input){
	for(auto rule : entries){
		stringstream ss;
		ss << "firewall.acl." << rule->id << ".hits_per_minute";
		input[ss.str()] = rule->hits_per_minute->value();
	}
}
