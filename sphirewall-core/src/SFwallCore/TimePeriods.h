/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIMEPERIODS_H
#define TIMEPERIODS_H

#include <map>
#include <list>
#include <boost/shared_ptr.hpp>
#include "Core/ConfigurationManager.h"

class TimePeriod {
	public:
		TimePeriod();
		TimePeriod(
			std::string name,
			float startTime = -1,
			float endTime = -1,
			float startDate = -1,
			float endDate = -1,
			bool any = false,
			bool mon = false,
			bool tue = false,
			bool wed = false,
			bool thu = false,
			bool fri = false,
			bool sat = false,
			bool sun = false
		);
		virtual ~TimePeriod();
		bool matches(int currentTimeStamp);

		std::string uuid;
		std::string name;
	
		int startTime; // Integer i (= [0, 2359)
		int endTime; // Integer i (= [0, 2359)
		int startDate; // Unix timestamp
		int endDate; // Unix timestamp

		//days
		bool any;
		bool mon;
		bool tue;
		bool wed;
		bool thu;
		bool fri;
		bool sat;
		bool sun;
};

typedef boost::shared_ptr<TimePeriod> TimePeriodPtr;

class TimePeriodDeleteListener {
	public:
		virtual void timePeriodDeleted(TimePeriodPtr removed) = 0;
};

class TimePeriodStore : public Configurable {
	public:
		TimePeriodStore();
		virtual ~TimePeriodStore();
		static bool checkMatch(const std::list<TimePeriodPtr> &periods, int t);

		bool load();
		void save();	
	
		void add(TimePeriodPtr period);
		void remove(TimePeriodPtr period);
		TimePeriodPtr getById(std::string id);
		TimePeriodPtr getByName(std::string name);
		std::list<TimePeriodPtr> list();
	
		void registerDeleteListener(TimePeriodDeleteListener *listener);
		const char* getConfigurationSystemName(){
			return "Time period store";
		}
	private:
		std::map<std::string, TimePeriodPtr> store;
		std::list<TimePeriodDeleteListener *> deleteListeners;
};

#endif
