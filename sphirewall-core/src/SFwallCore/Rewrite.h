/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REWRITE_RULE_H
#define REWRITE_RULE_H

#include <list>
#include <set>

#include "Core/Logger.h"
#include "SFwallCore/Packetfwd.h"
class ConfigurationManager;
namespace SFwallCore {
	class TcpConnection;
	class Packet;
	class Connection;
	class ConnectionIp;
	class Alias;
	class AliasDb;


	enum RewriteType {
		HttpRedirect = 0,
		DNSRecast = 1,
		GSSEnforce = 2
	};

	class RewriteRule {
		public:
			virtual void rewrite(Connection *conn, Packet *packet) = 0;
			virtual ~RewriteRule();
			virtual PacketDirection direction() = 0;
	};

	class HttpEnforceGoogleSafeSearch : public RewriteRule {
		public:
			HttpEnforceGoogleSafeSearch();
			virtual ~HttpEnforceGoogleSafeSearch();

			virtual void rewrite(Connection *conn, Packet *packet);
			PacketDirection direction();
	};

	class HttpRedirectRewrite : public RewriteRule {
		public:
			HttpRedirectRewrite(std::string url);
			HttpRedirectRewrite(std::string id, std::string url);
			~HttpRedirectRewrite();

			virtual void rewrite(Connection *conn, Packet *packet);
			PacketDirection direction();

		private:
			std::string id;
			std::string url;
	};


	class DNSRecaster : public RewriteRule {
		public:
			DNSRecaster();
			DNSRecaster(std::set<std::string> from, std::string to);
			virtual ~DNSRecaster();
			virtual void rewrite(Connection *conn, Packet *packet);

			PacketDirection direction();

			std::set<std::string> from;
			std::string to;
	};
};

#endif
