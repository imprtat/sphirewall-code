/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <linux/types.h>
#include <sys/types.h>
#include <sstream>
#include <vector>

#include "Kernel/sphirewall_queue.h"
#include "Kernel/client.h"

using namespace std;

#include "Utils/Checksum.h"
#include "Core/System.h"
#include "Utils/IP4Addr.h"
#include "SFwallCore/Capture.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/Connection.h"
#include "BandwidthDb/BandwidthDbPrimer.h"
#include "Core/HostDiscoveryService.h"
#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/NatAcls.h"
#include "Core/TorProvider.h"

struct sq_packet *SFwallCore::Packet::cloneSqp(struct sq_packet *sqp) {
	struct sq_packet *copy = (struct sq_packet *) malloc(sizeof(struct sq_packet));
	unsigned char *dataCopy = (unsigned char *) malloc(RAWSIZE);

	memcpy(copy, sqp, sizeof(struct sq_packet));
	memcpy(dataCopy, sqp->raw_packet, RAWSIZE);
	copy->raw_packet = dataCopy;
	return copy;
}

struct NatInstruction *SFwallCore::processPrerouting(struct sq_packet *sqp) {
	Firewall *sFirewall = System::getInstance()->getFirewall();

	if (sFirewall->PREROUTING_ENABLED == 1) {
		Packet *packet = Packet::parsePacket(sqp); //very cheap operation, just sets up pointers

		if (packet && (packet->getProtocol() == TCP || packet->getProtocol() == UDP || packet->getProtocol() == ICMP || packet->getProtocol() == GRE)) {
			sFirewall->natAcls->holdLock();
			
			if(sFirewall->ENABLE_FIREWALL_EXCEPTION_HANDLERS == 1){
				for(ExternalFirewallHandler* handler : sFirewall->external_firewall_handlers){
					if(handler->efh__enabled() && handler->efh__prerouting_handle(packet) == EXTERNAL_HANDLER__ALLOW){
						sFirewall->natAcls->releaseLock();
						delete packet;
						return NULL;
					}
				}
			}

			PortForwardingRulePtr natRule = sFirewall->natAcls->matchForwardingRule(packet);
			if (natRule) {
				struct NatInstruction *instruction = (struct NatInstruction *) malloc(sizeof(struct NatInstruction));

				if (packet->getProtocol() == TCP || packet->getProtocol() == UDP) {

					TcpUdpPacket *transport = (TcpUdpPacket *) packet->getTransport();
					instruction->type = NAT_DNAT;
					instruction->targetPort = natRule->forwardingDestinationPort != UNSET ? natRule->forwardingDestinationPort : transport->getDstPort();
					instruction->targetAddress = natRule->forwardingDestination;

					sFirewall->natAcls->releaseLock();
					delete packet;
					return instruction;
				}else{
					instruction->type = NAT_DNAT;
					instruction->targetAddress = natRule->forwardingDestination;	
					sFirewall->natAcls->releaseLock();
					delete packet;
					return instruction;
				}	
			}

			sFirewall->natAcls->releaseLock();
			int routing_fw_mark = sFirewall->natAcls->determine_routing_fwmark(packet);
			if(routing_fw_mark != -1){
				struct NatInstruction *instruction = (struct NatInstruction *) malloc(sizeof(struct NatInstruction));
				instruction->type = NAT_ROUTE;
				instruction->routemark = routing_fw_mark;
				delete packet;
				return instruction;
			}

			if(sFirewall->natAcls->determine_automatic_tor_gateway(packet)){
				TorProvider* tor_provider = System::getInstance()->get_tor_provider();
				struct NatInstruction *instruction = (struct NatInstruction *) malloc(sizeof(struct NatInstruction));

				instruction->type = NAT_DNAT;
				if(packet->getDstPort() == 53){
					instruction->targetPort = tor_provider->dns_listener_port;
				}else{
					instruction->targetPort = tor_provider->transport_listener_port;
				}

				instruction->targetAddress = tor_provider->interface_ptr->get_primary_ipv4_address();

				delete packet;
				return instruction;
			}

		}

		delete packet;
	}

	return NULL;
}

int SFwallCore::processFilterHook(struct sq_packet *sqp, bool &modified) {
	Firewall *sFirewall = System::getInstance()->getFirewall();
	//Check that this protocol is allowed:
	if (sqp->type == IPV4 && sFirewall->FIREWALL_IPV4_ENABLED == 0) {
		//Dont pass ipv4 packets
		return SQ_ACCEPT;
	}

	if (sqp->type == IPV6 && sFirewall->FIREWALL_IPV6_ENABLED == 0) {
		//Dont pass ipv6 packets
		return SQ_ACCEPT;
	}

	Packet *packet = NULL;
	Connection *conn = NULL;
	int verdict = SQ_DENY;
	int nice = 1;

	sFirewall->connectionTracker->holdLock();
	packet = Packet::parsePacket(sqp);

	if(sFirewall->ENABLE_FIREWALL_EXCEPTION_HANDLERS == 1){
		for(ExternalFirewallHandler* handler : sFirewall->external_firewall_handlers){
			if(handler->efh__enabled()){
				int response = handler->efh__filter_handle(packet);
				if(response == EXTERNAL_HANDLER__ALLOW){
					sFirewall->connectionTracker->releaseLock();

					delete packet;
					return SQ_ACCEPT;
				}else if(response == EXTERNAL_HANDLER__DENY){
					sFirewall->connectionTracker->releaseLock();

					delete packet;
					return SQ_DENY;
				}
			}
		}
	}

	if (packet && !packet->isBroadcast() && !packet->isMulticast() && packet->getTransport()) {
		if ((conn = sFirewall->connectionTracker->offer(packet))) {
			conn->verdict_filter_rule->hits_per_minute->input(1);
			if(conn->verdict_application_rule){
				conn->verdict_application_rule->hits_per_minute->input(1);
			}
	
			if(conn->getHostDiscoveryServiceEntry()){
				conn->getHostDiscoveryServiceEntry()->increment(packet);
			}

			nice = conn->nice;
			//Set the qos nice value:
			if (sFirewall->QOS_ENABLED == 1 && conn->getQosBucket()) {
				verdict = SQ_QUEUE;
			}
			else {
				verdict = SQ_ACCEPT;
			}

			if(sFirewall->APPLICATION_LAYER_FILTERS_ENABLED == 1 && !conn->getApplicationInfo()->ignore_all_filtering){
				if (!conn->getApplicationInfo()->afVerdictApplied) {
					if((conn->checkPacket(packet, DIR_SAME) && !conn->getApplicationInfo()->afProcessedRequest) 
							|| (conn->checkPacket(packet, DIR_OPPOSITE) && !conn->getApplicationInfo()->afProcessedResponse)){

						for (ApplicationLayerFilterHandler * handler : sFirewall->applicationLayerFilteringHandlers) {
							if (handler->enabled() && handler->matchesIpCriteria(conn, packet)) {
								handler->process(conn, packet);
							}
						}
					}
				}
			}

			if (sFirewall->RESIZE_TCP_ADJUST_SEQ_ENABLED == 1 && conn->getProtocol() == TCP) {
				TcpConnection *tcp = (TcpConnection *) conn;

				if (tcp->adjustSeqAckWindow(packet)) {
					modified = true;
					sFirewall->totalTcpWindowResized++;
				}
			}

			//Rewrites:
			if (sFirewall->REWRITE_ENABLED == 1 && conn->getRewriteRule()) {
				if (conn->checkPacket(packet, conn->getRewriteRule()->direction()) && packet->getTransport()->getApplication()->getSize() > 0) {
					conn->getRewriteRule()->rewrite(conn, packet);
					sFirewall->totalRewrites++;
					modified = true;
				}
			}

			//Terminate if required
			if (conn->isTerminating() && conn->checkPacket(packet, DIR_OPPOSITE)) {
				sFirewall->totalReset++;
				verdict = SQ_RESET;
			}

			if (conn->mustLog()) {
				Logger::instance()->log("sphirewalld.firewall.capture", EVENT, packet->toString());
			}

		}
		else {
			//Check the rules
			HostPtr host = System::getInstance()->getArp()->update(packet);
			if(host){
				host->total_packet_count++;
				host->total_transfer_count += packet->getLen();
				packet->setHostDiscoveryServiceEntry(host);
			}

			sFirewall->acls->holdLock();
			FilterRulePtr matchingRulePtr = sFirewall->acls->match_rule(packet);

			if (matchingRulePtr) {
				if ((verdict = matchingRulePtr->action) == SQ_ACCEPT) {
					if (!matchingRulePtr->ignoreconntrack) {
						conn = sFirewall->connectionTracker->create(packet);
					}

					if (conn) {
						if(host){
							host->total_active_connections++;
							conn->setHostDiscoveryServiceEntry(host);
						}

						conn->verdict_filter_rule = matchingRulePtr;
						conn->getApplicationInfo()->ignore_all_filtering = matchingRulePtr->ignore_application_layer_filters;
						packet->setConnection(conn);

						if (sFirewall->QOS_ENABLED == 1) {
							TokenBucketPtr qosBucket = sFirewall->trafficShaper->match(packet);
							if(qosBucket && qosBucket->isValid()){
								conn->setQosBucket(qosBucket);
								verdict = SQ_QUEUE;
							}

							PriorityRulePtr pr = sFirewall->priority_acls->match_rule(packet);
							if (pr != NULL) {
								nice = conn->nice = pr->nice;
							}
							else {
								nice = conn->nice = 1;
							}
						}

						if(sFirewall->APPLICATION_LAYER_FILTERS_ENABLED == 1 && !matchingRulePtr->ignore_application_layer_filters){
							for (ApplicationLayerFilterHandler * handler : sFirewall->applicationLayerFilteringHandlers) {
								if (handler->enabled() && handler->matchesIpCriteria(conn, packet)) {
									handler->process(conn, packet);
								}
							}
						}
						conn->setMustLog(matchingRulePtr->log);
					}
				}

				if (matchingRulePtr->log) {
					Logger::instance()->log("sphirewalld.firewall.capture", EVENT, packet->toString());
				}
			}

			sFirewall->acls->releaseLock();
		}

	}
	else if (packet) {
		sFirewall->acls->holdLock();
		FilterRulePtr matchingRulePtr = sFirewall->acls->match_rule(packet);

		if (matchingRulePtr) {
			verdict = matchingRulePtr->action;

			if (matchingRulePtr->log) {
				Logger::instance()->log("sphirewalld.firewall.capture", EVENT, packet->toString());
			}
		}

		sFirewall->acls->releaseLock();
	}

	if (sFirewall->QOS_ENABLED == 1 && (verdict == SQ_QUEUE && conn)) {
		if (conn->getQosBucket() && conn->getQosBucket()->isValid()) {
			if (conn->checkPacket(packet, SFwallCore::DIR_SAME)) {
				TokenBucketPending* pending = new TokenBucketPending();
				pending->id = sqp->id;
				pending->size = packet->getLen();
				pending->download = false;
				conn->getQosBucket()->addPacketToQueue(pending);
			}
			else {
				TokenBucketPending* pending = new TokenBucketPending();
				pending->id = sqp->id;
				pending->size = packet->getLen();
				pending->download = true;
				conn->getQosBucket()->addPacketToQueue(pending);
			}
		}else{
			verdict = SQ_ACCEPT;
		}	
	}

	sFirewall->connectionTracker->releaseLock();

	if (packet) {
		switch (packet->getProtocol()) {
			case TCP:
				sFirewall->totalTcpPackets++;
				break;
			case UDP:
				sFirewall->totalUdpPackets++;
				break;

			case ICMP:
				sFirewall->totalIcmpPackets++;
				break;
			default:
				break;
		}

		if (verdict == SQ_DENY) {
			Logger::instance()->log("sphirewalld.firewall.capture", INFO, "Dropping packet '%s'", packet->toString().c_str());
			sFirewall->totalDefaultAction++;
			sFirewall->totalDenied++;
		}else{
			sFirewall->totalAllowed++;
		}

		sFirewall->transferSampler->input(packet->getLen());
	}

	delete packet;
	if (sFirewall->QOS_ENABLED == 1 && sFirewall->priority_acls->listPriorityRules().size() > 0) {
		return (nice + SQ_PRIORITY_QUEUE);
	}else{
		return verdict;
	}

}

struct NatInstruction *SFwallCore::processPostrouting(struct sq_packet *sqp) {
	Firewall *sFirewall = System::getInstance()->getFirewall();

	if (sFirewall->POSTROUTING_ENABLED == 1) {
		Packet *packet = Packet::parsePacket(sqp);

		if (packet && (packet->getProtocol() == TCP || packet->getProtocol() == UDP || packet->getProtocol() == ICMP || packet->getProtocol() == GRE) && packet->type() == IPV4) {
			sFirewall->natAcls->holdLock();

			if(sFirewall->ENABLE_FIREWALL_EXCEPTION_HANDLERS == 1){
				for(ExternalFirewallHandler* handler : sFirewall->external_firewall_handlers){
					if(handler->efh__enabled()){
						int response = handler->efh__postrouting_handle(packet);
						if(response != EXTERNAL_HANDLER__IGNORE){
							sFirewall->natAcls->releaseLock();
							delete packet; 
							return NULL;
						}
					}
				}       
			}

			MasqueradeRulePtr natRule = sFirewall->natAcls->matchMasqueradeRule(packet);

			if (natRule) {
				struct NatInstruction *instruction = (struct NatInstruction *) malloc(sizeof(struct NatInstruction));
				instruction->type = NAT_SNAT;
				instruction->targetAddress = natRule->getUsableNatTargetIp();

				sFirewall->natAcls->releaseLock();
				delete packet;
				return instruction;
			}

			PacketV4* ipv4 = (PacketV4*) packet;
			if(sFirewall->natAcls->determine_autowan_matches(packet)){
				struct NatInstruction *instruction = (struct NatInstruction *) malloc(sizeof(struct NatInstruction));
				instruction->type = NAT_SNAT;
				instruction->targetAddress = packet->getDestNetDevice()->get_primary_ipv4_address();

				sFirewall->natAcls->releaseLock();
				delete packet;
				return instruction;
			}

			sFirewall->natAcls->releaseLock();
		}

		delete packet;
	}

	return NULL; // not modified
}

void SFwallCore::startCapture() {
	SqClient *client = &System::getInstance()->getFirewall()->sqClient;

	Logger::instance()->log("sphirewalld.firewall.capture", EVENT, "Attempting to establish a connction with the kernel module", true);
	client->registerFilterCallback(&processFilterHook);
	client->registerPreroutingCallback(&processPrerouting);
	client->registerPostroutingCallback(&processPostrouting);

	if (client->connect() < 0) {
		Logger::instance()->log("sphirewalld.firewall.capture", CONSOLE, "Could not connect to the sphirewall_queue kernel module", true);
		exit(-1);
		System::getInstance()->shutdown();
	}

	client->setYieldCountSetting(&SFwallCore::Firewall::SQUEUE_DEQUEUE_YIELD_TIMEOUT);
	client->setUsleepInterval(&SFwallCore::Firewall::SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL);

	Logger::instance()->log("sphirewalld.firewall.capture", EVENT, "Core packet capture started", true);
	client->run();
	Logger::instance()->log("sphirewalld.firewall.capture", EVENT, "Core packet capture stopped", true);
}

