/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <gtest/gtest.h>
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Api/Delegates/AuthDelegate.h"
#include "Auth/User.h"
#include "Auth/Group.h"
#include "Auth/GroupDb.h"
#include "Core/ConfigurationManager.h"
#include "Core/HostDiscoveryService.h"
#include "test.h"

using namespace std;

TEST(AuthDelegate, base) {
	EXPECT_TRUE(true);
}

TEST(AuthDelegate, auth_login) {
	MockFactory *tester = new MockFactory();

	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	sessionDb->setConfigurationManager(new ConfigurationManager());
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();
	arp->update(IP4Addr::stringToIP4Addr("10.1.1.1"), "aa:bb:cc:dd:ee:ff");

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp);

	UserPtr user = userDb->createUser("michael");
	user->setPassword("1234");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == 0);

	if (ret[L"response"]->AsNumber() != 0) cerr << "Reason: " << ret[L"message"]->String() << endl;

	EXPECT_TRUE(sessionDb->get("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1")) != NULL);
}

TEST(AuthDelegate, auth_login_withoutpassword) {
	MockFactory *tester = new MockFactory();

	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	sessionDb->setConfigurationManager(new ConfigurationManager());
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();
	arp->update(IP4Addr::stringToIP4Addr("10.1.1.1"), "mac_address");

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp);
	userDb->createUser("michael");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() < 0);
}

TEST(AuthDelegate, auth_login_invalid_user) {
	MockFactory *tester = new MockFactory();
	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();

	arp->update(IP4Addr::stringToIP4Addr("10.1.1.1"), "mac_address");

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp);

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
	EXPECT_TRUE(sessionDb->get("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1")) == NULL);
}

TEST(AuthDelegate, auth_login_invalid_password) {
	MockFactory *tester = new MockFactory();

	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();

	arp->update(IP4Addr::stringToIP4Addr("10.1.1.1"), "mac_address");

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp);

	UserPtr user = userDb->createUser("michael");
	user->setPassword("proper_password");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
	EXPECT_TRUE(sessionDb->get("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1")) == NULL);
}

TEST(AuthDelegate, auth_login_invalid_macaddress) {
	MockFactory *tester = new MockFactory();
	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();

	UserPtr user = userDb->createUser("michael");
	user->setPassword("1234");
        AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp);

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "1.1.1.2"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
	EXPECT_TRUE(sessionDb->list().size() == 0);
}


