/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NetworkSettingsDelegate_H
#define NetworkSettingsDelegate_H

#include "Api/Delegate.h"
#include "Utils/Interfaces.h"
#include "Core/IPSec.h"
#include "Core/Vpn.h"

class NetworkSettingsDelegate : public virtual Delegate {
	public:
		NetworkSettingsDelegate(IntMgr *interfaceManager, DNSConfig *dnsConfig, HostDiscoveryService *arpTable);
		JSONObject process(std::string uri, JSONObject object);
		std::string rexpression();
		void setConnectionManager(ConnectionManager *connectionManager) ;
		void setEventDb(EventDb *eventDb);
		void setVpnManager(VpnManager *o){
			this->vpnManager = o;
		}
	private:
		IntMgr* interfaceManager;
		DNSConfig *dnsConfig;
		HostDiscoveryService *arp;
		ConnectionManager *connectionManager;
		EventDb *eventDb;
		VpnManager *vpnManager;

		_BEGIN_SPHIREWALL_DELEGATES(NetworkSettingsDelegate)
		DelegateHandlerImpl devices_publish;
		DelegateHandlerImpl devices_list;
                DelegateHandlerImpl devices_up;
                DelegateHandlerImpl devices_save;
                DelegateHandlerImpl devices_down;

		DelegateHandlerImpl devices_set;
		DelegateHandlerImpl devices_dynamicdns_set;
		DelegateHandlerImpl devices_synced;
		DelegateHandlerImpl dns_get;
		DelegateHandlerImpl dns_set;
		DelegateHandlerImpl routes_list;
		DelegateHandlerImpl routes_add;
		DelegateHandlerImpl routes_del;
		DelegateHandlerImpl arp_list;
		DelegateHandlerImpl arp_size;
		DelegateHandlerImpl dnsmasq_publish;
		DelegateHandlerImpl dnsmasq_start;
		DelegateHandlerImpl dnsmasq_stop;
		DelegateHandlerImpl dnsmasq_running;
		DelegateHandlerImpl devices_delete;
		DelegateHandlerImpl devices_toggle;
		DelegateHandlerImpl devices_toggledhcp;
		DelegateHandlerImpl devices_alias_list;
		DelegateHandlerImpl devices_alias_delete;
		DelegateHandlerImpl devices_alias_add;
		DelegateHandlerImpl connections_list;
		DelegateHandlerImpl connectons_add;
		DelegateHandlerImpl connections_del;
		DelegateHandlerImpl connections_save;
		DelegateHandlerImpl connections_connect;
		DelegateHandlerImpl connections_disconnect;

		DelegateHandlerImpl vpn_start;
		DelegateHandlerImpl vpn_stop;
		DelegateHandlerImpl vpn_publish;
		DelegateHandlerImpl vpn_status;
		DelegateHandlerImpl vpn_options;
                DelegateHandlerImpl vpn_instances_list;
                DelegateHandlerImpl vpn_instance_create;
                DelegateHandlerImpl vpn_instance_remove;

                DelegateHandlerImpl vpn_clientvpn;
                DelegateHandlerImpl vpn_clientvpn_status;
                DelegateHandlerImpl vpn_clientvpn_options;

		DelegateHandlerImpl wireless_start;
		DelegateHandlerImpl wireless_stop;
		DelegateHandlerImpl wireless_online;
		DelegateHandlerImpl dyndns;
		DelegateHandlerImpl dyndns_set;

		DelegateHandlerImpl devices_leases_add;
		DelegateHandlerImpl devices_leases_del;
		DelegateHandlerImpl ping;
		DelegateHandlerImpl set_quarantined;

		DelegateHandlerImpl tor;
		DelegateHandlerImpl tor_set;
		DelegateHandlerImpl tor_status;
        	DelegateHandlerImpl tor_start;
		DelegateHandlerImpl tor_stop;

		DelegateHandlerImpl mdns;
		DelegateHandlerImpl mdns_set;
		DelegateHandlerImpl mdns_del;

		DelegateHandlerImpl dig;
		DelegateHandlerImpl traceroute;
		DelegateHandlerImpl capture;
		DelegateHandlerImpl capture_start;
		DelegateHandlerImpl capture_stop;
		DelegateHandlerImpl capture_raw;

		_END_SPHIREWALL_DELEGATES
};

#endif
