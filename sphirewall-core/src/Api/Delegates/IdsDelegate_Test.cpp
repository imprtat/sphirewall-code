/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>

using namespace std;

#include "Ids/Ids.h"
#include "Api/Delegates/IdsDelegate.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Utils/IP4Addr.h"

TEST(IdsDelegate, exceptionsList) {
	IDS *ids = new IDS(NULL);
	ids->addException(AddressPair(IP4Addr::stringToIP4Addr("10.1.1.1"), IP4Addr::stringToIP4Addr("255.255.255.0")));
	ids->addException(AddressPair(IP4Addr::stringToIP4Addr("10.1.1.2"), IP4Addr::stringToIP4Addr("255.255.255.0")));

	IdsDelegate *delegate = new IdsDelegate(ids);
	JSONObject ret = delegate->process("ids/exceptions/list", JSONObject());

	JSONArray exceptions = ret[L"exceptions"]->AsArray();
	EXPECT_TRUE(exceptions.size() == 2);

	JSONObject e1 = exceptions[0]->AsObject();
	JSONObject e2 = exceptions[1]->AsObject();

	EXPECT_TRUE(e1[L"ip"]->String().compare("10.1.1.1") == 0);
	EXPECT_TRUE(e1[L"mask"]->String().compare("255.255.255.0") == 0);

	EXPECT_TRUE(e2[L"ip"]->String().compare("10.1.1.2") == 0);
	EXPECT_TRUE(e2[L"mask"]->String().compare("255.255.255.0") == 0);
}

TEST(IdsDelegate, exceptionAdd) {
	IDS *ids = new IDS(NULL);
	IdsDelegate *delegate = new IdsDelegate(ids);

	JSONObject object;
	object.put(L"ip", new JSONValue((string) "10.1.1.1"));
	object.put(L"mask", new JSONValue((string) "255.255.0.0"));

	delegate->process("ids/exceptions/add", object);

	vector<AddressPair> l;
	ids->listExceptions(l);

	EXPECT_TRUE(l.size() == 1);
}

TEST(IdsDelegate, exceptionsDel) {
	IDS *ids = new IDS(NULL);
	ids->addException(AddressPair(IP4Addr::stringToIP4Addr("10.1.1.1"), IP4Addr::stringToIP4Addr("255.255.255.0")));
	ids->addException(AddressPair(IP4Addr::stringToIP4Addr("10.1.1.2"), IP4Addr::stringToIP4Addr("255.255.255.0")));

	JSONObject object;
	object.put(L"ip", new JSONValue((string) "10.1.1.1"));
	object.put(L"mask", new JSONValue((string) "255.255.255.0"));

	IdsDelegate *delegate = new IdsDelegate(ids);
	JSONObject ret = delegate->process("ids/exceptions/del", object);

	vector<AddressPair> l;
	ids->listExceptions(l);

	EXPECT_TRUE(l.size() == 2);
}

