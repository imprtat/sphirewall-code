/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ManagementSocketServer_h
#define ManagementSocketServer_h

#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/asio/ssl.hpp> // Added for SSL.

#include <Utils/StringUtils.h>
#include "Core/Logger.h"

using boost::asio::ip::tcp;

// Maximum socket data length is 10 MB.
#define MAX_SOCKET_DATA_LENGTH (1024 * 1000000)

typedef boost::shared_ptr<tcp::socket> socket_ptr;
typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> SSL_SOCKET;

class JsonManagementService;
class ConfigurationManager;

class ManagementSocketServer {
	public:
		ManagementSocketServer(int port,
							   JsonManagementService *jsonManagementService, bool ssl)
			: port(port), jsonManagementService(jsonManagementService),
			  ssl(ssl) {
		}

		void run();
		void runSSL();
		void handleConnection(socket_ptr sock);

		void setConfigurationManager(ConfigurationManager *config) {
			this->config = config;
		}

		// SSLSession nested class definition.
		class SSLSession {
			public:
				SSLSession(boost::asio::io_service &io_service,
						   boost::asio::ssl::context &context);
				SSL_SOCKET::lowest_layer_type &socket();
				void start();
				void handle_handshake(const boost::system::error_code &error);
				void handle_read(const boost::system::error_code &error,
								 size_t bytes_transferred);
				void handle_write(const boost::system::error_code &error);
				void setManagementSocketServer(ManagementSocketServer *mss) {
					this->mss = mss;
				};
				ManagementSocketServer *getManagementSocketServer() {
					return mss;
				};

			private:
				SSL_SOCKET sslSocket;
				char data_[MAX_SOCKET_DATA_LENGTH];
				ManagementSocketServer *mss;
		};

		// SSLServer nested class definition.
		class SSLServer {
			public:
				SSLServer(boost::asio::io_service &io_service, unsigned short port, ManagementSocketServer *mss);
				std::string getPassword();
				void handle_accept(SSLSession *new_session,
								   const boost::system::error_code &error);
				void setManagementSocketServer(ManagementSocketServer *mss) {
					this->mss = mss;
				};
				ManagementSocketServer *getManagementSocketServer() {
					return mss;
				};

			private:
				boost::asio::io_service       &io_service;
				boost::asio::ip::tcp::acceptor acceptor;
				boost::asio::ssl::context      context;
				ManagementSocketServer        *mss;
		};

	private:
		int                    port;
		JsonManagementService *jsonManagementService;
		ConfigurationManager  *config;
		SSLServer              *sslServer;
		bool                   ssl;
};

#endif
