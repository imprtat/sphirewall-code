/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WINWMIAUTH_H
#define WINWMIAUTH_H

#include <sstream>
#include <list>

#include "Core/Lockable.h"
#include "Core/ConfigurationManager.h"

using namespace std;

class WmiException: public exception {
	public:

		WmiException(std::string message) {
			w = message;
		}

		~WmiException() throw () {
		}

		virtual const char *what() const throw () {
			return "WMIC Exception";
		}

		const std::string &message() const {
			return w;
		}

	private:
		std::string w;
};

class WmiLoginEvent {
	public:
		std::string eventId;
		std::string user;
		std::string host;

		bool isValidUser();

		std::string toString() {
			std::stringstream ss;
			ss << "id:" << eventId << " user:" << getPlainUsername() << " host:" << getIpv4Address();
			return ss.str();
		}

		std::string getPlainUsername();
		std::string getIpv4Address();
};


class EventDb;
class AuthenticationManager;
class HostDiscoveryService;
class ConfigurationManager;

class WinDomainController {
	public:
		WinDomainController(std::string hostname){
			this->cursor = 0;
			this->hostname = hostname;
		}		

		int cursor;
		std::string hostname;
};

class WinWmiAuth : public Lockable {
	public:
		WinWmiAuth(EventDb *eventDb, AuthenticationManager *authManager, HostDiscoveryService *arp, ConfigurationManager *config):
			eventDb(eventDb), authManager(authManager), arp(arp), config(config) {
		}

		WinWmiAuth() {
		}

		bool isEnabled();
		std::string getUsername();
		std::string getDomain();
		std::string getPassword();
		std::string getHost();

		void poll();
		bool check();
		void run();

		bool loaded_cursors(){
			for(WinDomainController* dc : domain_controllers){
				if(dc->cursor == 0){
					return false; 
				}
			}
			return true;
		}

		void refresh();
	private:
		std::list<WinDomainController*> domain_controllers;

		WmiLoginEvent *getRawEvent(WinDomainController* controller, std::string id);
		void seedCursor(WinDomainController* controller);
		std::string attribute(std::string raw, std::string key);
		std::list<std::string> findEventsSince(WinDomainController* controller);
		std::string query(WinDomainController* controller, std::string input);
		void updateCursor(WinDomainController* controller, std::string input);

		//Event Mapping Properties:
		std::string getUsernameAttribute();
		std::string getHostAttribute();
		std::string getTargetEventId();

		EventDb *eventDb;
		AuthenticationManager *authManager;
		HostDiscoveryService *arp;
		ConfigurationManager *config;
};

class WinWmiAuthConfigurationCallback : public virtual ConfigurationCallback {
        public:
                WinWmiAuthConfigurationCallback(WinWmiAuth* wmi) {
                        this->wmi = wmi;
                }
                ~WinWmiAuthConfigurationCallback() {};

                void run(){
                        this->wmi->refresh();
                }

	private:
		WinWmiAuth* wmi;
};

#endif
