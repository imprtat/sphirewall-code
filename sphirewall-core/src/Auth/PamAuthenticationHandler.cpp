/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
using namespace std;

#include "Core/Event.h"
#include "Auth/UserDb.h"
#include "Auth/User.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include <iostream>
#include <fcntl.h>
#include <sys/resource.h>
#include <security/pam_appl.h>
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include "Utils/HttpRequestWrapper.h"
#include <boost/regex.hpp>
#include <set>
#include "Auth/PamAuthenticationHandler.h"

#define STATUS_OK 	  0
#define STATUS_UNKNOWN	  1
#define STATUS_INVALID	  2
#define STATUS_BLOCKED    3
#define STATUS_EXPIRED    4
#define STATUS_PW_EXPIRED 5
#define STATUS_NOLOGIN    6
#define STATUS_MANYFAILS  7

#define STATUS_INT_USER  50
#define STATUS_INT_ARGS  51
#define STATUS_INT_ERR   52

int hisuid;
int haveuid = 0;

#define BFSZ 1024
#define msgi(i) (*(msg[i]))

struct ad_user {
	char *login;
	char *passwd;
};

using namespace std;

bool PamAuthenticationMethod::enabled() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:pam:enabled")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:pam:enabled")->boolean();
	}

	return false;
}

int PAM_conv(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr) {
	struct ad_user *user = (struct ad_user *) appdata_ptr;
	struct pam_response *response;
	int i;

	if (msg == NULL || resp == NULL || user == NULL)
		return PAM_CONV_ERR;

	response = (struct pam_response *)
		malloc(num_msg * sizeof(struct pam_response));

	for (i = 0; i < num_msg; i++) {
		response[i].resp_retcode = 0;
		response[i].resp = NULL;

		switch (msgi(i).msg_style) {
			case PAM_PROMPT_ECHO_ON:
				response[i].resp = appdata_ptr ? (char *) strdup(user->login) : NULL;
				break;

			case PAM_PROMPT_ECHO_OFF:
				response[i].resp = appdata_ptr ? (char *) strdup(user->passwd) : NULL;
				break;

			case PAM_TEXT_INFO:
			case PAM_ERROR_MSG:
				break;

			default:
				if (response != NULL) free(response);
				return PAM_CONV_ERR;
		}
	}

	*resp = response;
	return PAM_SUCCESS;
}

bool PamAuthenticationMethod::authenticate(const std::string& username, const std::string& password) {
	char *usernameArray = (char *) malloc(username.size() * sizeof(char));
	char *passwordArray = (char *) malloc(password.size() * sizeof(char));
	memcpy((void *) usernameArray, (void *) username.c_str(), username.size());
	memcpy((void *) passwordArray, (void *) password.c_str(), password.size());

	struct ad_user user_info = {usernameArray, passwordArray};
	struct pam_conv conv = {PAM_conv, (void *) &user_info};
	pam_handle_t *pamh = NULL;
	int retval;

	user_info.login = usernameArray;
	user_info.passwd = passwordArray;

	string pam_module;

	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:pam:module")) {
		pam_module = System::getInstance()->getConfigurationManager()->get("authentication:pam:module")->string();
	}
	else {
		pam_module = "common-password";
	}

	retval = pam_start((char *) pam_module.c_str(), usernameArray, &conv, &pamh);

	if (retval == PAM_SUCCESS)
		retval = pam_authenticate(pamh, PAM_SILENT);

	if (retval == PAM_SUCCESS)
		retval = pam_acct_mgmt(pamh, 0);

	if (pam_end(pamh, retval) != PAM_SUCCESS) {
		pamh = NULL;
		free(usernameArray);
		free(passwordArray);
		return false;
	}

	if (retval == 0) {
		free(usernameArray);
		free(passwordArray);
		return true;
	}

	return false;
}

