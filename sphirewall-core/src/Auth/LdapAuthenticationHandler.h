/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUTH_LDAP 
#define AUTH_LDAP 

#define LDAP_DEPRECATED 1

#include <ldap.h>
#include <list>
#include "Core/Logger.h"
#include "Core/Cron.h"
#include "Auth/AuthenticationType.h"
#include "Auth/UserDb.h"

class ConfigurationManager;
class Lock;
typedef boost::shared_ptr<User> UserPtr;

class LdapBase {
	public:
		LDAP *connect(std::string hostname, int port, std::string username, std::string password);

		//Server properties:
		std::string getHostname() const;
		int getPort() const;
		std::string getLdapUsername() const;
		std::string getLdapPassword() const;
		std::string getBaseDn() const;
		std::string getUserNameAttribute();
		std::string getUserBase();
		std::string getUserGroupAttribute();
		std::string getGroupBase();
		std::string getGroupNameAttribute();
		std::string getGroupMemberNameAttribute();
                bool isEnabled();

		std::string getUserDnAttribute();
		bool authenticateUserUsingSuppliedDn();
		bool userOwnsGroupMembership();

		bool isConnected();
};

class LdapDirectoryProvider : public virtual DirectoryServiceProvider, public virtual LdapBase {
	public:
		int type(){
			return LDAP_DB;
		}

		bool enabled(){
			return isEnabled();
		}

		bool find(std::list<std::string>& groups, const std::string& username);
		bool find_all_groups(std::list<std::string>& groups);

		bool check(){
			return isConnected();
		}
};

class LdapAuthenticationProvider : public virtual AuthenticationProvider,  public virtual LdapBase{
	public:
		int type(){
			return LDAP_DB;
		}	

		bool enabled(){
			return isEnabled();
		}

		bool authenticate(const std::string& username, const std::string& password);

		bool check(){
			return isConnected();
		}
	private:
		std::string find_dn(const std::string& username);
};

#endif
