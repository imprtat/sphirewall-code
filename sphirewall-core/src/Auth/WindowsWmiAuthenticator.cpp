/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "Auth/WindowsWmiAuthenticator.h"
#include "Core/System.h"
#include <boost/regex.hpp>

#include "Core/Event.h"
#include "Core/ConfigurationManager.h"
#include "Auth/User.h"
#include "Auth/UserDb.h"
#include "Utils/IP4Addr.h"
#include "Auth/Session.h"
#include "Core/HostDiscoveryService.h"
#include "Utils/LinuxUtils.h"
#include "Utils/StringUtils.h"

using namespace std;

void WinWmiAuth::poll() {
	if (!isEnabled()) {
		return;
	}

	Logger::instance()->log("sphirewalld.wmic.poll", DEBUG, "Polling for new events");

	holdLock();
	for(WinDomainController* controller : domain_controllers){
		try {
			seedCursor(controller);
			list<std::string> ids = findEventsSince(controller);

			for (list<string>::iterator iter = ids.begin(); iter != ids.end(); iter++) {
				WmiLoginEvent *event = getRawEvent(controller, (*iter));

				if (event->isValidUser()) {
					Logger::instance()->log("sphirewalld.wmic.poll", INFO, "Found a kerberos user login event '%s' on dc '%s'", event->toString().c_str(), controller->hostname.c_str());

					HostPtr host_entry = arp->get(IP4Addr::stringToIP4Addr(event->getIpv4Address()));
					if (!host_entry) {
						EventParams event_params;
						event_params["dc"] = controller->hostname;
						event_params["user"] = event->getPlainUsername();
						event_params["ip"] = event->getIpv4Address();
						eventDb->add(new Event(USERDB_COULD_NOT_FIND_HOST, event_params));

						Logger::instance()->log("sphirewalld.wmic.poll", ERROR, 
							"Could not find mac address for new session '%s' on dc '%s'", event->toString().c_str(), controller->hostname.c_str());
						updateCursor(controller, (*iter));
						delete event;
						continue;
					}

					UserPtr user = System::getInstance()->getUserDb()->getUser(event->getPlainUsername(), true);
					if (!user) {
						EventParams event_params;
						event_params["dc"] = controller->hostname;
						event_params["user"] = event->getPlainUsername();
						event_params["ip"] = event->getIpv4Address();
						eventDb->add(new Event(USERDB_LOGIN_FAILED, event_params));

						Logger::instance()->log("sphirewalld.wmic.poll", ERROR, 
							"Could not find user account for new session '%s' on dc '%s'", event->toString().c_str(), controller->hostname.c_str());
						updateCursor(controller, (*iter));
						delete event;
						continue;
					}

					if(host_entry->authenticated_user){
						if (host_entry->authenticated_user == user) {
							updateCursor(controller, (*iter));
							delete event;
							continue;
						}
						else {
							arp->deauthenticate_user_to_host(host_entry, user);
						}
					}

					arp->authenticate_user_to_host(host_entry, user, -1, false);
				}

				updateCursor(controller, (*iter));
			}

		}catch (const WmiException &e) {
			Logger::instance()->log("sphirewalld.wmic.poll", ERROR, e.message());
		}
	}
	releaseLock();
}

void WinWmiAuth::run() {
	while (true) {
		poll();
		sleep(2);
	}
}

void WinWmiAuth::refresh(){
	holdLock();
	std::string all_hostnames = getHost();	
	vector<string> all_hostnames_list;
	split(all_hostnames, ',', all_hostnames_list);

	for(WinDomainController* c : domain_controllers){
		delete c;
	}
	domain_controllers.clear();

	for(string hostname : all_hostnames_list){
		Logger::instance()->log("sphirewalld.wmic.query", INFO, "adding domain controller for wmi '%s'", hostname.c_str());
		domain_controllers.push_back(new WinDomainController(hostname));
	}
	releaseLock();
}

std::string WinWmiAuth::query(WinDomainController* controller, std::string input) {
	Logger::instance()->log("sphirewalld.wmic.query", DEBUG, "executing wmic query '%s'", input.c_str());
	stringstream queryIds;
	queryIds << "wmic -U " << getDomain() << "/" << getUsername() << " --password \"" << getPassword() << "\" //" << controller->hostname << " \"" << input << "\"";

	string ret;
	if (LinuxUtils::exec(queryIds.str(), ret) != 0) {
		throw WmiException(ret);
	}

	return ret;
}

bool WinWmiAuth::check() {
	try {
		Logger::instance()->log("sphirewalld.wmic.check", INFO, "Polling wmic to check connection details");
		for(WinDomainController* controller : domain_controllers){
			query(controller, "Select RecordNumber from Win32_NTLogEvent where RecordNumber = 1");
		}
	}
	catch (const WmiException &e) {
		Logger::instance()->log("sphirewalld.wmic.check", ERROR, "Could not connect, reason: " + e.message());
		throw e;
	}

	return true;
}

list<string> WinWmiAuth::findEventsSince(WinDomainController* controller) {
	stringstream q;
	q << "Select RecordNumber from Win32_NTLogEvent Where Logfile = 'Security' And EventIdentifier = "<< getTargetEventId()<< " And RecordNumber > " << controller->cursor;
	string ret = query(controller, q.str());
	boost::regex reg("Security|([0-9]+)");
	boost::smatch what;

	std::string::const_iterator start, end;
	start = ret.begin();
	end = ret.end();

	list<std::string> items;

	while (boost::regex_search(start, end, what, reg)) {
		items.push_back(what[1]);
		start = what[0].second;
	}

	items.remove("");
	return items;
}

void WinWmiAuth::seedCursor(WinDomainController* controller) {
	if(controller->cursor != 0){
		return;
	}

	stringstream q;
	q << "Select RecordNumber from Win32_NTLogEvent Where Logfile = 'Security' And EventIdentifier = " << getTargetEventId() << " And RecordNumber > " << controller->cursor;
	string ret = query(controller, q.str());
	boost::regex reg("Security|([0-9]+)");
	boost::smatch what;

	std::string::const_iterator start, end;
	start = ret.begin();
	end = ret.end();

	while (boost::regex_search(start, end, what, reg)) {
		string is = what[1];
		int i = atoi(is.c_str());
		if(i > controller->cursor){
			controller->cursor = i;
		}
		start = what[0].second;
	}
}

void WinWmiAuth::updateCursor(WinDomainController* controller, std::string input) {
	int value = stoi(input);

	if (value > controller->cursor) {
		controller->cursor = value;
	}
}

WmiLoginEvent *WinWmiAuth::getRawEvent(WinDomainController* controller, std::string id) {
	stringstream ss;
	ss << "Select Message from Win32_NTLogEvent Where Logfile = 'Security' And EventIdentifier = "<< getTargetEventId() << " and RecordNumber = ";
	ss << id;
	string ret = query(controller, ss.str());

	WmiLoginEvent *event = new WmiLoginEvent();
	event->eventId = id;
	event->user = attribute(ret, getUsernameAttribute());
	event->host = attribute(ret, getHostAttribute());
	return event;
}

string WinWmiAuth::attribute(string event, string key) {
	stringstream exp;
	exp << key << ":" << "([\\s]+)([:,a-z,A-Z,0-9\\.,@,\\$,-]+)";
	boost::regex req(exp.str());
	boost::smatch what2;

	while (boost::regex_search(event, what2, req)) {
		return what2[2];
	}

	return "";
}

bool WmiLoginEvent::isValidUser() {
	return getPlainUsername().size() > 0;
}

std::string WmiLoginEvent::getPlainUsername() {
	boost::regex req("([a-z,A-Z,0-9,\\.,-]+)");
	boost::smatch what2;

	if (boost::regex_search(user, what2, req)) {
		return what2[1];
	}

	return "";
}

std::string WmiLoginEvent::getIpv4Address() {
	boost::regex req("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");
	boost::smatch what2;

	if (boost::regex_search(host, what2, req)) {
		stringstream ss;
		ss << what2[1] << "." << what2[2] << "." << what2[3] << "." << what2[4];
		return ss.str();
	}

	return "";
}

std::string WinWmiAuth::getUsernameAttribute(){
	if (config->hasByPath("authentication:wmic:usernameAttribute")) {
		string value = config->get("authentication:wmic:usernameAttribute")->string();
		if(value.size() > 0){
			return value;
		}
	}

	return "Account Name";
}

std::string WinWmiAuth::getHostAttribute(){
	if (config->hasByPath("authentication:wmic:hostAttribute")) {
		string value = config->get("authentication:wmic:hostAttribute")->string();
		if(value.size() > 0){
			return value;
		}
	}

	return "Client Address";
}

std::string WinWmiAuth::getTargetEventId(){
	if (config->hasByPath("authentication:wmic:targetEventId")) {
		string value = config->get("authentication:wmic:targetEventId")->string();
		if(value.size() > 0){
			return value;
		}
	}

	return "4768";
}


bool WinWmiAuth::isEnabled() {
	if (config->hasByPath("authentication:wmic:enabled")) {
		return config->get("authentication:wmic:enabled")->boolean();
	}

	return false;
}
std::string WinWmiAuth::getUsername() {
	if (config->hasByPath("authentication:wmic:username")) {
		return config->get("authentication:wmic:username")->string();
	}

	return "";
}

std::string WinWmiAuth::getDomain() {
	if (config->hasByPath("authentication:wmic:domain")) {
		return config->get("authentication:wmic:domain")->string();
	}

	return "";
}

std::string WinWmiAuth::getPassword() {
	if (config->hasByPath("authentication:wmic:password")) {
		return config->get("authentication:wmic:password")->string();
	}

	return "";
}

std::string WinWmiAuth::getHost() {
	if (config->hasByPath("authentication:wmic:hostname")) {
		return config->get("authentication:wmic:hostname")->string();
	}

	return "";
}


