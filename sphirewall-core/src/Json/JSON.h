/*
 * JSON.h
 * Copyright (C) 2010 Mike Anchor <mikea@mjpa.co.uk>
 *
 * Part of the MJPA JSON Library - http://mjpa.co.uk/blog/view/A-simple-C-JSON-library/
 *
 * License: http://mjpa.co.uk/licenses/GPLv2/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _JSON_H_
#define _JSON_H_

#include <vector>
#include <string>
#include <map>
#include <exception>

// Custom types
class JSONValue;
typedef std::vector<JSONValue*> JSONArray;

class JSONFieldNotFoundException: public std::exception
{
public:
        JSONFieldNotFoundException(std::string message){
                w = message;
        }

        ~JSONFieldNotFoundException() throw(){
        }

        virtual const char* what() const throw()
        {
                return w.c_str();
        }

        std::string message(){
                return w;
        }

private:
        std::string w;
};

class JSONObject {
	public:
		JSONValue* operator [] (std::wstring st);				
		void put(std::wstring st, JSONValue* value);		
		int size() {
			return values.size();
		}	

		bool has(std::wstring k){
			return values.find(k) != values.end();
		}

		bool hasString(std::wstring k);
	
		std::map<std::wstring, JSONValue*> values;
		~JSONObject();
};

#include "JSONValue.h"

class JSON
{
	friend class JSONValue;
	
	public:
		static JSONValue* Parse(const char *data);
		static JSONValue* Parse(const wchar_t *data);
		static std::wstring Stringify(JSONValue *value);
	protected:
		static bool SkipWhitespace(const wchar_t **data);
		static bool ExtractString(const wchar_t **data, std::wstring &str);
		static int ParseInt(const wchar_t **data);
	private:
		JSON();
};


std::wstring StringToWString(const std::string& s);
std::string WStringToString(const std::wstring& s);
int wcsncasecmp(const wchar_t *s1, const wchar_t *s2, size_t n);
#endif
