#include <iostream>
#include <fstream>

using namespace std;

#include "SFwallCore/Packet.h"
#include "BandwidthDb/AuditListener.h"
#include "Core/ConfigurationManager.h"
#include "BandwidthDb/ConnectionStatistics.h"
#include "Utils/TimeWrapper.h"
#include "Core/System.h"

int AuditListener::push(std::vector<ConnectionStatistics*> input){
	stringstream filename;
	filename << get_audit_directory() << "/";
	filename << "audit_" << Time(time(NULL)).format("%Y-%m-%d") << ".log";

	ofstream myfile;
	myfile.open (filename.str(), ios::app);
	for(ConnectionStatistics* item : input){
		ObjectContainer* item_container = item->serialize();
		ObjectWrapper* wrapper = new ObjectWrapper(item_container);
		myfile << JsonSerialiser::serializeToJsonString(wrapper) << endl;
		delete wrapper;
	}

	myfile.close();
	return -1;
}

bool AuditListener::enabled(){
        if (System::getInstance()->getConfigurationManager()->hasByPath("audit:enabled")) {
                return System::getInstance()->getConfigurationManager()->get("audit:enabled")->boolean();
        }

	return true;
}

std::string AuditListener::get_audit_directory(){
        if (System::getInstance()->getConfigurationManager()->hasByPath("audit:directory")) {
                return System::getInstance()->getConfigurationManager()->get("audit:directory")->string();
        }

	return "/var/log/sphirewalld_audit";
}
