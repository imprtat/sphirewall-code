/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "BandwidthException.h"

BandwithException::BandwithException()
	: exception_value("Unknown exception occurred")
{}

BandwithException::BandwithException(const BandwithException &other)
	: exception_value(other.exception_value)
{}

BandwithException::BandwithException(std::string what)
	: exception_value(what)
{}


BandwithException::~BandwithException() throw()
{}

BandwithException &BandwithException::operator=(const BandwithException &other) {
	this->exception_value = other.exception_value;
	return *this;
}

bool BandwithException::operator==(const BandwithException &other) {
	return this->exception_value == other.exception_value;
}

std::string BandwithException::what() {
	return "BandwithException: " + this->exception_value;
}
