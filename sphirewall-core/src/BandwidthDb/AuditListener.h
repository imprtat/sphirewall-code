#ifndef AUDIT_LISTENER_H
#define AUDIT_LISTENER_H

#include "BandwidthDb/BandwidthListener.h"

class AuditListener : public BandwidthListener {
	public:
		int push(std::vector<ConnectionStatistics *> input);
		bool enabled();
		std::string name(){
			return "AuditListener";
		}
	private:
		std::string get_audit_directory();
};

#endif
