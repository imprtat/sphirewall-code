/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>
#include "Api/Delegate.h"
#include "Api/Exceptions.h"
#include "test.h"

using namespace std;

class TeamCityPrinter : public ::testing::EmptyTestEventListener {
		// Called before a test starts.
		virtual void OnTestStart(const ::testing::TestInfo &test_info) {
			current_test = string(test_info.test_case_name()) + "." + string(test_info.name());
			cout << "##teamcity[testStarted name='" << current_test << "']\n";
		}

		// Called after a failed assertion or a SUCCEED() invocation.
		virtual void OnTestPartResult(const ::testing::TestPartResult &test_part_result) {
			string summ = "";

			if (test_part_result.failed()) {
				string summary = test_part_result.summary();

				for (unsigned i = 0; i < summary.length(); i++) {
					if (summary[i] == '\'') {
						summ += "|'";
					}
					else if (summary[i] == '\n') {
						summ += "|n";
					}
					else if (summary[i] == '\r') {
						summ += "|r";
					}
					else if (summary[i] == '|') {
						summ += "||";
					}
					else if (summary[i] == ']') {
						summ += "|]";
					}
					else {
						summ += summary[i];
					}
				}

				cout << "##teamcity[testFailed "
					 << "name='" << current_test << "' "
					 << "message='A Test Failed' "
					 << "details='" << summ << "|n"
					 << test_part_result.file_name() << ":" << test_part_result.line_number() << "' "
					 << "]\n";
			}
		}

		// Called after a test ends.
		virtual void OnTestEnd(const ::testing::TestInfo &test_info) {
			cout << "##teamcity[testFinished name='" << current_test << "']\n";
		}

	private:
		string current_test;
};

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);

	for (int i = 0; i < argc; i++) {
		if (!strncmp(argv[i], "--teamcity", strlen("--teamcity"))) {
			::testing::TestEventListeners &listeners = ::testing::UnitTest::GetInstance()->listeners();
			delete listeners.Release(listeners.default_result_printer());
			listeners.Append(new TeamCityPrinter);
			break;
		}
	}

	return RUN_ALL_TESTS();
}

void expectException(Delegate *delegate, std::string path, JSONObject args) {
	bool caught = false;

	try {
		delegate->process(path, args);
	}
	catch (const DelegateGeneralException &e) {
		caught = true;
	}

	EXPECT_TRUE(caught == true);
}

