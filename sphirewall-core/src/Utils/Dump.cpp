/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
#include "Utils/Dump.h"
#include <iomanip>

void hexdump(std::ostream &output, char *buffer, size_t len, size_t line_len) {
	std::string pchars;
	output << std::hex << std::setfill('0');
	unsigned i = 0;

	for (; i < len; i++) {
		unsigned c = (unsigned char) buffer[i];

		if (!(i % line_len)) {
			if (i) {
				output << " " << pchars << std::endl;
				pchars = "";
			}

			output << std::setw(4) << i << ": ";
		}

		output << std::setw(2) << c;

		if (c == 127 || c < 32) {
			pchars += ".";
		}
		else {
			pchars += c;
		}

		if (i & 1) {
			output << " ";
		}
	}

	for (i = i % line_len; i < line_len; i++) {
		output << ((i & 1) ? "   " : "  ");
	}

	output << " " << pchars << std::endl;
	output << std::dec << std::setfill(' ');
}
