/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <glob.h>
#include "DnsType.h"
#include <fstream>
#include "Utils/IP4Addr.h"
#include "Utils/Dump.h"

void hexdump(std::ostream &output, char *buffer, size_t len, size_t line_len);

const unsigned char dnsquery[] =    "\x59\xc4" "\x01\x00" "\x00\x01" "\x00\x00" "\x00\x00" "\x00\x00" "\x0c\x73" "\x6d\x61"
									"\x73\x68" "\x6d\x79" "\x73\x74" "\x61\x63" "\x6b\x03" "\x63\x6f" "\x6d\x00" "\x00\x01"
									"\x00\x01";

const unsigned char dnsresponse[] = "\xdb\x42" "\x81\x80" "\x00\x01" "\x00\x01" "\x00\x00" "\x00\x00" "\x03\x77" "\x77\x77"
									"\x0c\x6e" "\x6f\x72" "\x74\x68" "\x65\x61" "\x73\x74" "\x65\x72" "\x6e\x03" "\x65\x64"
									"\x75\x00" "\x00\x01" "\x00\x01" "\xc0\x0c" "\x00\x01" "\x00\x01" "\x00\x00" "\x02\x58"
									"\x00\x04" "\x9b\x21" "\x11\x44";

const unsigned char dnsAForFacebook[] =
	"\x8e\x53" "\x01\x00" "\x00\x01" "\x00\x00" "\x00\x00" "\x00\x00" "\x03\x77" "\x77\x77"
	"\x08\x66" "\x61\x63" "\x65\x62" "\x6f\x6f" "\x6b\x03" "\x63\x6f" "\x6d\x00" "\x00\x01"
	"\x00\x01";

const unsigned char dnsCNAMEForFacebookA[] =
	"\x8e\x53" "\x81\x80" "\x00\x01" "\x00\x02" "\x00\x00" "\x00\x00" "\x03\x77" "\x77\x77"
	"\x08\x66" "\x61\x63" "\x65\x62" "\x6f\x6f" "\x6b\x03" "\x63\x6f" "\x6d\x00" "\x00\x01"
	"\x00\x01" "\xc0\x0c" "\x00\x05" "\x00\x01" "\x00\x00" "\x06\x03" "\x00\x0c" "\x04\x73"
	"\x74\x61" "\x72\x04" "\x63\x31" "\x30\x72" "\xc0\x10" "\xc0\x2e" "\x00\x01" "\x00\x01"
	"\x00\x00" "\x00\x17" "\x00\x04" "\xad\xfc" "\x6e\x1b";

const unsigned char dnsResponseForGoogle_com[] =
	"\x94\xf5\x81\x80" "\x00\x01\x00\x10" "\x00\x04\x00\x05" "\x06\x67\x6f\x6f" "\x67\x6c\x65\x03" "\x63\x6f\x6d\x00" "\x00\x01\x00\x01"
	"\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x2a" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04"
	"\xca\x7c\x7f\x2e" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x30" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00"
	"\x00\xdd\x00\x04" "\xca\x7c\x7f\x31" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x35" "\xc0\x0c\x00\x01"
	"\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x39" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x3b"
	"\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x10" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04"
	"\xca\x7c\x7f\x14" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x18" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00"
	"\x00\xdd\x00\x04" "\xca\x7c\x7f\x1a" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x1b" "\xc0\x0c\x00\x01"
	"\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x1f" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x23"
	"\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04" "\xca\x7c\x7f\x25" "\xc0\x0c\x00\x01" "\x00\x01\x00\x00" "\x00\xdd\x00\x04"
	"\xca\x7c\x7f\x26" "\xc0\x0c\x00\x02" "\x00\x01\x00\x00" "\xa9\x80\x00\x06" "\x03\x6e\x73\x33" "\xc0\x0c\xc0\x0c" "\x00\x02\x00\x01"
	"\x00\x00\xa9\x80" "\x00\x06\x03\x6e" "\x73\x32\xc0\x0c" "\xc0\x0c\x00\x02" "\x00\x01\x00\x00" "\xa9\x80\x00\x06" "\x03\x6e\x73\x31"
	"\xc0\x0c\xc0\x0c" "\x00\x02\x00\x01" "\x00\x00\xa9\x80" "\x00\x06\x03\x6e" "\x73\x34\xc0\x0c" "\xc1\x4c\x00\x01" "\x00\x01\x00\x00"
	"\xa9\x80\x00\x04" "\xd8\xef\x20\x0a" "\xc1\x3a\x00\x01" "\x00\x01\x00\x00" "\xa9\x80\x00\x04" "\xd8\xef\x22\x0a" "\xc1\x28\x00\x01"
	"\x00\x01\x00\x00" "\xa9\x80\x00\x04" "\xd8\xef\x24\x0a" "\xc1\x5e\x00\x01" "\x00\x01\x00\x00" "\xa9\x80\x00\x04" "\xd8\xef\x26\x0a"
	"\x00\x00\x29\x10" "\x00\x00\x00\x00" "\x00\x00\x00";

const unsigned char peer1_0[] = {
	0xe2, 0x50, 0x81, 0x80, 0x00, 0x01, 0x00, 0x02,
	0x00, 0x00, 0x00, 0x00, 0x03, 0x77, 0x77, 0x77,
	0x06, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x02,
	0x63, 0x6f, 0x02, 0x6e, 0x7a, 0x00, 0x00, 0x01,
	0x00, 0x01, 0x03, 0x77, 0x77, 0x77, 0x06, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x02, 0x63, 0x6f,
	0x06, 0x6e, 0x7a, 0x15, 0xbc, 0x09, 0x11, 0x00,
	0x00, 0x05, 0x00, 0x01, 0x00, 0x00, 0x1c, 0x20,
	0x00, 0x18, 0x0b, 0x6e, 0x6f, 0x73, 0x73, 0x6c,
	0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x06, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x03, 0x63, 0x6f,
	0x6d, 0x00, 0x0b, 0x6e, 0x6f, 0x73, 0x73, 0x6c,
	0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x06, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x05, 0x63, 0x6f,
	0x6d, 0x6f, 0x6d, 0x00, 0x00, 0x01, 0x00, 0x01,
	0x00, 0x00, 0x1c, 0x20, 0x00, 0x04, 0xd8, 0xef,
	0x20, 0x14
};

TEST(DnsType, testFlagExtraction) {
	dns_packet_header *parsed = (dns_packet_header *) dnsquery;

	printf("         : |QR| Opcode |AA|TC|RD|RA| Zero | Rcode  | == | Flags |\n");
	printf("Expecting: |%2d|%8d|%2d|%2d|%2d|%2d|     0|%8d| == |0x%5x|\n", 0, 0, 0, 0, 1, 0, 0, 0x100);
	printf("Got      : |%2d|%8d|%2d|%2d|%2d|%2d|     0|%8d| == |0x%5x|\n",
		   dns_ex_qr(parsed->flags),
		   dns_ex_opcode(parsed->flags),
		   dns_ex_aa(parsed->flags),
		   dns_ex_tc(parsed->flags),
		   dns_ex_rd(parsed->flags),
		   dns_ex_ra(parsed->flags),
		   dns_ex_rcode(parsed->flags),
		   ntohs(parsed->flags)
		  );

	ASSERT_EQ(DnsFlags::DNS_QR_QUERY, dns_ex_qr(parsed->flags));
	ASSERT_EQ(DnsOpCodes::QUERY, dns_ex_opcode(parsed->flags));
	ASSERT_EQ(0, dns_ex_aa(parsed->flags));
	ASSERT_EQ(DnsFlags::DNS_TC_NOT_TRUNCATED, dns_ex_tc(parsed->flags));
	ASSERT_EQ(DnsFlags::DNS_RECURSION_DESIRED, dns_ex_rd(parsed->flags));
	ASSERT_EQ(0, dns_ex_ra(parsed->flags));
	ASSERT_EQ(DnsResponseCodes::NOERROR, dns_ex_rcode(parsed->flags));
}

TEST(DnsType, testFlagExtractionResponse) {
	dns_packet_header *parsed = (dns_packet_header *) dnsresponse;

	printf("         : |QR| Opcode |AA|TC|RD|RA| Zero | Rcode  | == | Flags |\n");
	printf("Expecting: |%2d|%8d|%2d|%2d|%2d|%2d|     0|%8d| == |0x%5x|\n", 0, 0, 0, 0, 1, 0, 0, 0x8180);
	printf("Got      : |%2d|%8d|%2d|%2d|%2d|%2d|     0|%8d| == |0x%5x|\n",
		   dns_ex_qr(parsed->flags),
		   dns_ex_opcode(parsed->flags),
		   dns_ex_aa(parsed->flags),
		   dns_ex_tc(parsed->flags),
		   dns_ex_rd(parsed->flags),
		   dns_ex_ra(parsed->flags),
		   dns_ex_rcode(parsed->flags),
		   ntohs(parsed->flags)
		  );

	ASSERT_EQ(DnsFlags::DNS_QR_RESPONSE, dns_ex_qr(parsed->flags));
	ASSERT_EQ(DnsOpCodes::QUERY, dns_ex_opcode(parsed->flags));
	ASSERT_EQ(DnsFlags::DNS_AA_NON_AUTHORITATIVE, dns_ex_aa(parsed->flags));
	ASSERT_EQ(DnsFlags::DNS_TC_NOT_TRUNCATED, dns_ex_tc(parsed->flags));
	ASSERT_EQ(DnsFlags::DNS_RECURSION_DESIRED, dns_ex_rd(parsed->flags));
	ASSERT_EQ(DnsFlags::DNS_RECURSION_AVAILABLE, dns_ex_ra(parsed->flags));
	ASSERT_EQ(DnsResponseCodes::NOERROR, dns_ex_rcode(parsed->flags));
}

TEST(DnsType, testQuestionExtraction) {
	ASSERT_STREQ("smashmystack.com", dns_get_q_nth((dns_packet_header *) dnsquery, 0));
	ASSERT_STREQ("www.northeastern.edu", dns_get_q_nth((dns_packet_header *) dnsresponse, 0));
}

TEST(DnsType, testFullRequestMessageExtraction) {
	DnsMessage *m1 = new DnsMessage((char *) dnsquery);
	DnsMessage *m3 = new DnsMessage((char *) dnsAForFacebook);
	size_t m1clen = 0;
	char *m1cstr = m1->toCString(&m1clen);

	ASSERT_STREQ("smashmystack.com", m1->getQuestions().front()->getQName().c_str());
	size_t m3clen = 0;
	char *m3cstr = m3->toCString(&m3clen);

	ASSERT_EQ(sizeof(dnsquery) - 1, m1clen);
	ASSERT_EQ(sizeof(dnsAForFacebook) - 1, m3clen);

	ASSERT_TRUE(!memcmp(dnsquery, m1cstr, m1clen));
	ASSERT_TRUE(!memcmp(dnsAForFacebook, m3cstr, m3clen));
}

TEST(DnsType, testFullResponseMessageExtraction) {
	DnsMessage *m2 = new DnsMessage((char *) dnsresponse);
	DnsMessage *m4 = new DnsMessage((char *) dnsCNAMEForFacebookA);

	size_t m2clen = 0;
	ASSERT_STREQ("www.northeastern.edu", m2->getAnswers().front()->getQName().c_str());
	char *m2cstr = m2->toCString(&m2clen);

	size_t m4clen = 0;
	ASSERT_STREQ("star.c10r.facebook.com",
				 ((DnsCNAMERecord *)m4->getAnswers().front()->getResourceRecord())->getCanonicalName().c_str()
				);
	char *m4cstr = m4->toCString(&m4clen);

	DnsMessage *m1 = new DnsMessage(m2cstr);
	DnsMessage *m3 = new DnsMessage(m4cstr);

	size_t m1clen = 0;
	size_t m3clen = 0;
	char *m1cstr = m1->toCString(&m1clen);
	char *m3cstr = m3->toCString(&m3clen);

	ASSERT_EQ(m1clen, m2clen);
	ASSERT_TRUE(!memcmp(m1cstr, m2cstr, m1clen));

	ASSERT_EQ(m3clen, m4clen);
	ASSERT_TRUE(!memcmp(m4cstr, m3cstr, m4clen));
}


TEST(DnsType, testFullResponseMessageExtractionWithGoogle) {
	size_t m1len = 0, m2len = 0, m3len = 0;
	char *m1str = NULL, *m2str = NULL, *m3str = NULL;
	DnsMessage *m1 = new DnsMessage((char *) dnsResponseForGoogle_com);
	m1str = m1->toCString(&m1len);
	DnsMessage *m2 = new DnsMessage((char *) m1str);
	m2str = m2->toCString(&m2len);
	DnsMessage *m3 = new DnsMessage((char *) m2str);
	m3str = m3->toCString(&m3len);

	ASSERT_EQ(m1len, m2len);
	ASSERT_EQ(m2len, m3len);
	ASSERT_TRUE(!memcmp(m1str, m2str, m2len));
	ASSERT_TRUE(!memcmp(m2str, m3str, m3len));
}

TEST(DnsType, testAnswerNameSanity) {
	DnsMessage *m = new DnsMessage();
	m->getQuestions().push_back(
		new DnsQuestion(
			"www.google.co.nz",
			DnsRecordTypes::A,
			DnsRecordClasses::INTERNET
		)
	);
	m->getAnswers().push_back(
		new DnsAnswer(
			"www.google.co.nz",
			DnsRecordTypes::CNAME,
			DnsRecordClasses::INTERNET,
			7200,
			new DnsCNAMERecord("nosslsearch.google.com")
		)
	);
	m->getAnswers().push_back(
		new DnsAnswer(
			"nosslsearch.google.com",
			DnsRecordTypes::A,
			DnsRecordClasses::INTERNET,
			7200,
			new DnsARecord(htonl(IP4Addr::stringToIP4Addr("216.239.32.20")))
		)
	);
	size_t len = 0;
	char *dump = m->toCString(&len);
	hexdump(std::cout, dump, len, 32);
}
