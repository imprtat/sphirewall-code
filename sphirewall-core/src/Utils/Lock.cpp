/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;
#include "Utils/Lock.h"

Lock::Lock() {
	pthread_mutex_init(&mlock, NULL);
}

void Lock::lock() {
	pthread_mutex_lock(&mlock);
}

void Lock::unlock() {
	pthread_mutex_unlock(&mlock);
}

bool Lock::tryLock() {
	if (pthread_mutex_trylock(&mlock) == 0) {
		return true;
	}

	return false;
}
