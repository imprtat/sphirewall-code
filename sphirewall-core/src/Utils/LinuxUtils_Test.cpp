#include <gtest/gtest.h>
#include <iostream>

#include "Utils/LinuxUtils.h"

using namespace std;

TEST(LinuxUtils, exec_ls){
	std::string buffer;
	int ret = LinuxUtils::exec("ls", buffer);	
	EXPECT_TRUE(ret == 0);
}	

TEST(LinuxUtils, exec_missing_command){
	std::string buffer;
	int ret = LinuxUtils::exec("sdfsdffsdffsf", buffer);	
	EXPECT_TRUE(ret != 0);
}	

