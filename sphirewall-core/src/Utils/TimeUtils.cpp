/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

#include "Utils/TimeUtils.h"

std::string TimeUtils::dateNow() {
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, 80, "%d-%m-%Y", timeinfo);

	return std::string(buffer);
}

void TimeUtils::convertToString(time_t rawtime, const char* format, std::string& _return) {
	struct tm * timeinfo;
	char buffer [80];

	timeinfo = gmtime(&rawtime);
	strftime(buffer, 80, format, timeinfo);

	_return += buffer;
}

int TimeUtils::extractHour(time_t rawtime) {
	int ret = 0;
	string temp;
	convertToString(rawtime, "%H", temp);

	ret = stoi(temp);

	return ret;
}

