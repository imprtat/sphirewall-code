/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <net/if.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <resolv.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <signal.h>
#include <map>
#include <poll.h>
#include <linux/types.h>
#include <sys/socket.h>
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <sys/ioctl.h>
#include <linux/rtnetlink.h>
#include <curl/curl.h>
#include <sstream>
#include <iostream>

#include "Utils/IP4Addr.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Utils/Dhcp.h"

using namespace std;

#include "Utils/DhcpServer.h"

std::list<DhcpServerLeasePtr> DDInterfaceConfiguration::list_leases(){
	return leases;
}

void DDInterfaceConfiguration::add_leases(DhcpServerLeasePtr lease){
	for(DhcpServerLeasePtr potential_duplicate : leases){
		if(potential_duplicate->mac_address.compare(lease->mac_address) == 0 && potential_duplicate->ip == lease->ip){
			potential_duplicate->state = DHCP_SERVER_LEASE_PERMANENT;
			return;
		}
	}

	leases.push_back(lease);
}

void DDInterfaceConfiguration::del_leases(DhcpServerLeasePtr lease){
	leases.remove(lease);
}

DhcpServerLeasePtr DDInterfaceConfiguration::find_lease_by_hw(std::string hw){
	for(DhcpServerLeasePtr lease : leases){
		if(lease->mac_address.compare(hw) == 0){
			return lease;
		}
	}

	return DhcpServerLeasePtr();
}

DhcpServerLeasePtr DDInterfaceConfiguration::find_lease_by_ip(unsigned int ip){
	for(DhcpServerLeasePtr lease : leases){
		if(lease->ip == ip){
			return lease;
		}
	}

	return DhcpServerLeasePtr();
}

void DDInterfaceConfiguration::dhcp_cleanup(){
	for(DhcpServerLeasePtr lease : list_leases()){
		if(lease){
			if(lease->state == DHCP_SERVER_LEASE_ESTABLISHED && (time(NULL) - lease->lease_time) > max_lease_time){
				Logger::instance()->log("sphirewalld.ddserver.dhcp_cleanup", INFO, "removing expired lease '%s'", lease->to_string());
				del_leases(lease);
			}else if (lease->state == DHCP_SERVER_LEASE_OFFER && (time(NULL) - lease->lease_time) > max_offered_lease_time){
				Logger::instance()->log("sphirewalld.ddserver.dhcp_cleanup", INFO, "removing unaccepted lease '%s'", lease->to_string());
				del_leases(lease);
			}
		}
	}
}

DhcpServerLeasePtr DDInterfaceConfiguration::find_lease_by_mac_ip(unsigned int ip, std::string hw){
	for(DhcpServerLeasePtr lease : leases){
		if(lease && lease->ip == ip && lease->mac_address.compare(hw) == 0){
			return lease;
		}
	}

	return DhcpServerLeasePtr();
}

unsigned int DDInterfaceConfiguration::find_free_address(){
	for(unsigned int start = start_ip; start <= end_ip; start++){
		if(!find_lease_by_ip(start)){
			return start;
		}
	}

	return 0;
}

const char* DhcpServerLease::to_string(){
	stringstream ss;
	ss << "mac: " << mac_address << " ip: " << IP4Addr::ip4AddrToString(ip) << " lease_time: " << lease_time;
	return ss.str().c_str();
}

void DDInterfaceConfiguration::__handle_dhcp_request__discover(struct dhcp_builder_context* ctx){
	//Check for existing leases:
	DhcpServerLeasePtr lease = find_lease_by_hw(IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr));
	if(!lease){
		//Create a new lease
		unsigned int new_address = find_free_address();
		if(new_address == 0){
			Logger::instance()->log("sphirewalld.ddserver.dhcp", INFO, "Could not find free address to allocate, attempting to cleanup stale entries");
			return;
		}else{
			lease = DhcpServerLeasePtr(new DhcpServerLease());
			lease->mac_address = IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr);
			lease->state = DHCP_SERVER_LEASE_OFFER;
			lease->ip = find_free_address();
			lease->lease_time = time(NULL);
			leases.push_back(lease);
		}
	}

	//Build the response:
	struct dhcp_builder_context* response_context = dhcp_init_context();
	response_context->header.yiaddr = htonl(lease->ip);
	response_context->header.siaddr = htonl(interface_ptr->get_primary_ipv4_address());
	memcpy(&response_context->header.chaddr, &ctx->header.chaddr, 6);

	dhcp_set_int8_options(response_context, DHCP_OPTION_MESSAGE_TYPE, DHCPOFFER);

	//Other options:
	dhcp_set_int32_options(response_context, DHCP_OPTION_SERVER_IP, htonl(interface_ptr->get_primary_ipv4_address()));
	dhcp_set_int32_options(response_context, DHCP_OPTION_LEASE_TIME, htonl(max_lease_time));
	dhcp_set_int32_options(response_context, DHCP_OPTION_SUBNET_MASK, htonl(interface_ptr->get_primary_ipv4_netmask()));
	dhcp_set_int32_options(response_context, DHCP_OPTION_BROADCAST, htonl(interface_ptr->get_primary_ipv4_broadcast()));

	if(tftp_boot_enabled){
		if(tftp_boot_server.size() > 0){
			dhcp_set_blob_options(response_context, DHCP_OPTION_TFTP_SERVER, (char*) tftp_boot_server.c_str(), tftp_boot_server.size());
		}

		if(tftp_boot_filename.size() > 0){
			dhcp_set_blob_options(response_context, DHCP_OPTION_TFTP_FILENAME, (char*) tftp_boot_filename.c_str(), tftp_boot_filename.size());
		}
	}

	if(dns_mode == DNS_FORWARD){
		dhcp_set_int32_options(response_context, DHCP_OPTION_DNS, htonl(interface_ptr->get_primary_ipv4_address()));
	}else if(dns_mode == DNS_DIRECT){
		dhcp_set_int32_options(response_context, DHCP_OPTION_DNS, htonl(dns_server));
	}
	dhcp_set_int32_options(response_context, DHCP_OPTION_ROUTER, htonl(interface_ptr->get_primary_ipv4_address()));

	response_context->header.op = 2;
	response_context->header.xid = ctx->header.xid;

	char outgoing_buffer[1024];
	int response_s_len = dhcp_serialize(response_context, (char*) outgoing_buffer, 1024);
	if(response_s_len){
		dhcp_udp_send(interface_ptr->name.c_str(), (unsigned char*) outgoing_buffer, response_s_len, SERVER);
		Logger::instance()->log("sphirewalld.ddserver.dhcp", INFO, "DHCPDISCOVER on '%s' for '%s', offerred lease '%s'",
				interface.c_str(), IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr).c_str(), lease->to_string());
	}
	dhcp_free_context(response_context);
}

void DDInterfaceConfiguration::__handle_dhcp_request__request(struct dhcp_builder_context* ctx){
	struct dhcp_builder_context* response_context = dhcp_init_context();
	unsigned int request_ip = 0;
	dhcp_get_int32_option(ctx, DHCP_OPTION_REQUEST_CLIENT_IP, &request_ip);

	DhcpServerLeasePtr lease = find_lease_by_mac_ip(ntohl(request_ip), IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr));
	if(!lease){
		//Decline
		memcpy(&response_context->header.chaddr, &ctx->header.chaddr, 6);
		response_context->header.siaddr = htonl(interface_ptr->get_primary_ipv4_address());
		dhcp_set_int8_options(response_context, DHCP_OPTION_MESSAGE_TYPE, DHCPNAK);
		response_context->header.op = 2;
		response_context->header.xid = ctx->header.xid;

		char outgoing_buffer[1024];
		int response_s_len = dhcp_serialize(response_context, (char*) outgoing_buffer, 1024);
		if(response_s_len){
			dhcp_udp_send(interface_ptr->name.c_str(), (unsigned char*) outgoing_buffer, response_s_len, SERVER);

			Logger::instance()->log("sphirewalld.ddserver.dhcp", INFO, "DHCPNAK on '%s' for '%s'", 
					interface.c_str(), IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr).c_str());
		}
	}else{
		response_context->header.yiaddr = htonl(lease->ip);
		response_context->header.siaddr = htonl(interface_ptr->get_primary_ipv4_address());
		memcpy(&response_context->header.chaddr, &ctx->header.chaddr, 6);

		dhcp_set_int8_options(response_context, DHCP_OPTION_MESSAGE_TYPE, DHCPACK);
		dhcp_set_int32_options(response_context, DHCP_OPTION_SERVER_IP, htonl(interface_ptr->get_primary_ipv4_address()));
		dhcp_set_int32_options(response_context, DHCP_OPTION_LEASE_TIME, htonl(max_lease_time));
		dhcp_set_int32_options(response_context, DHCP_OPTION_SUBNET_MASK, htonl(interface_ptr->get_primary_ipv4_netmask()));
		dhcp_set_int32_options(response_context, DHCP_OPTION_BROADCAST, htonl(interface_ptr->get_primary_ipv4_broadcast()));

		if(tftp_boot_enabled){
			if(tftp_boot_server.size() > 0){
				dhcp_set_blob_options(response_context, DHCP_OPTION_TFTP_SERVER, (char*) tftp_boot_server.c_str(), tftp_boot_server.size());
			}

			if(tftp_boot_filename.size() > 0){
				dhcp_set_blob_options(response_context, DHCP_OPTION_TFTP_FILENAME, (char*) tftp_boot_filename.c_str(), tftp_boot_filename.size());
			}
		}

		if(dns_mode == DNS_FORWARD){
			dhcp_set_int32_options(response_context, DHCP_OPTION_DNS, htonl(interface_ptr->get_primary_ipv4_address()));
		}else if(dns_mode == DNS_DIRECT){
			dhcp_set_int32_options(response_context, DHCP_OPTION_DNS, htonl(dns_server));
		}
		dhcp_set_int32_options(response_context, DHCP_OPTION_ROUTER, htonl(interface_ptr->get_primary_ipv4_address()));

		response_context->header.op = 2;
		response_context->header.xid = ctx->header.xid;

		char outgoing_buffer[1024];
		int response_s_len = dhcp_serialize(response_context, (char*) outgoing_buffer, 1024);
		if(response_s_len){
			dhcp_udp_send(interface_ptr->name.c_str(), (unsigned char*) outgoing_buffer, response_s_len, SERVER);

			Logger::instance()->log("sphirewalld.ddserver.dhcp", INFO, "DHCPREQUEST on '%s' for '%s', client accepted lease '%s'", 
					interface.c_str(), IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr).c_str(), lease->to_string());
		}

		if(lease->state == DHCP_SERVER_LEASE_ESTABLISHED || lease->state == DHCP_SERVER_LEASE_PERMANENT){
			EventParams params;
			params["mac"] = lease->mac_address;
			params["ip"] = IP4Addr::ip4AddrToString(lease->ip);
			System::getInstance()->getEventDb()->add(new Event(NETWORK_DHCPSERVER_LEASE_RENEWED, params));

			lease->lease_time = time(NULL);
		}else{
			lease->state = DHCP_SERVER_LEASE_ESTABLISHED;
			lease->lease_time = time(NULL);

			EventParams params; 
			params["mac"] = lease->mac_address;
			params["ip"] = IP4Addr::ip4AddrToString(lease->ip);
			System::getInstance()->getEventDb()->add(new Event(NETWORK_DHCPSERVER_LEASE_NEW, params));
		}
	}

	dhcp_free_context(response_context);

}

void DDInterfaceConfiguration::__handle_dhcp_request__decline(struct dhcp_builder_context* ctx){
	unsigned int request_ip = 0;
	dhcp_get_int32_option(ctx, DHCP_OPTION_REQUEST_CLIENT_IP, &request_ip);
	DhcpServerLeasePtr lease = find_lease_by_mac_ip(ntohl(request_ip), IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr));
	if(lease && lease->state != DHCP_SERVER_LEASE_PERMANENT){
		leases.remove(lease);
	}
}

void DDInterfaceConfiguration::__handle_dhcp_request__release(struct dhcp_builder_context* ctx){
	DhcpServerLeasePtr lease = find_lease_by_mac_ip(ntohl(ctx->header.ciaddr), IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr));
	if(lease && lease->state != DHCP_SERVER_LEASE_PERMANENT){
		Logger::instance()->log("sphirewalld.ddserver.dhcp", INFO, "DHCPRELEASE on '%s' for '%s'", 
				interface.c_str(), IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr).c_str());

		EventParams params;
		params["mac"] = lease->mac_address;
		params["ip"] = IP4Addr::ip4AddrToString(lease->ip);
		System::getInstance()->getEventDb()->add(new Event(NETWORK_DHCPSERVER_LEASE_RELEASED, params));

		leases.remove(lease);
	}
}

void DDInterfaceConfiguration::handle_dhcp_request(struct dhcp_builder_context* ctx){
	if(interface_ptr && dhcp_mode == 2){
		u_int8_t dhcp_message_type = 0;
		dhcp_get_int8_option(ctx, DHCP_OPTION_MESSAGE_TYPE, &dhcp_message_type);
		if(dhcp_message_type == DHCPDISCOVER){
			__handle_dhcp_request__discover(ctx);
		}else if(dhcp_message_type == DHCPREQUEST){
			__handle_dhcp_request__request(ctx);
		}else if(dhcp_message_type == DHCPDECLINE){
			__handle_dhcp_request__decline(ctx);
		}else if(dhcp_message_type == DHCPRELEASE){
			__handle_dhcp_request__release(ctx);
		}
	}else if(interface_ptr && relay_route_interface_ptr && dhcp_mode == 3){
		ctx->header.giaddr = htonl(interface_ptr->get_primary_ipv4_address());

		Logger::instance()->log("sphirewalld.ddserver.dhcp", INFO, "DHCPRELAY_REQUEST on '%s' for '%s', forwarding to dhcp server at '%s' on '%s'",
				interface.c_str(), IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr).c_str(), IP4Addr::ip4AddrToString(relay_server).c_str(), relay_route_interface_ptr->name.c_str());

		char outgoing_buffer[1024];
		int response_s_len = dhcp_serialize(ctx, (char*) outgoing_buffer, 1024);
		if(response_s_len){
			dhcp_udp_send(relay_route_interface_ptr->name.c_str(), (unsigned char*) outgoing_buffer,
					response_s_len, SERVER_TO_SERVER, interface_ptr->get_primary_ipv4_address(),relay_server);
		}
	}
}

void DDInterfaceConfiguration::handle_dhcp_response(struct dhcp_builder_context* ctx){
	if(dhcp_mode == 3 && relay_route_interface_ptr && interface_ptr->get_primary_ipv4_address() == ntohl(ctx->header.giaddr)){
		Logger::instance()->log("sphirewalld.ddserver.dhcp", INFO, "DHCPRELAY_RESPONSE on '%s' for '%s'",
				relay_route_interface_ptr->name.c_str(), IP4Addr::convertHw((unsigned char*) &ctx->header.chaddr).c_str());

		char outgoing_buffer[1024];
		int response_s_len = dhcp_serialize(ctx, (char*) outgoing_buffer, 1024);
		if(response_s_len){
			dhcp_udp_send(interface_ptr->name.c_str(), (unsigned char*) outgoing_buffer, response_s_len, SERVER);
		}
	}
}

void DDInterfaceConfiguration::refresh_interfaces(IntMgr* interfaceManager){
	Logger::instance()->log("sphirewalld.ddserver.refresh_interfaces", DEBUG, "refreshing interface configuration on %s", interface.c_str());
	if(dhcp_mode == DHCP_SERVER_NONE){
		interface_ptr = InterfacePtr();
		relay_route_interface_ptr = InterfacePtr();
	}else if(dhcp_mode == DHCP_SERVER_SERVICE){
		relay_route_interface_ptr = InterfacePtr();
		interface_ptr = interfaceManager->get_interface(interface);
	}else if(dhcp_mode == DHCP_SERVER_RELAY){
		interface_ptr = interfaceManager->get_interface(interface);
		//We need to determine the interface that the target server is sitting on
		for(InterfacePtr i : interfaceManager->get_all_interfaces()){
			if(i->get_primary_ipv4_address() != 0){
				if(IP4Addr::matchNetwork(relay_server, i->get_primary_ipv4_address(), i->get_primary_ipv4_netmask()) == 0){
					relay_route_interface_ptr = i;
					break;
				}
			}
		}
	}
}

int DDInterfaceConfiguration::__dns_open_socket(){
	int sock;
	struct sockaddr_in dns_listener;
	memset(&dns_listener, 0, sizeof(dns_listener));
	dns_listener.sin_family = AF_INET;
	dns_listener.sin_port = htons(53);
	dns_listener.sin_addr.s_addr = htonl(interface_ptr->get_primary_ipv4_address()); 

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0){
		Logger::instance()->log("sphirewalld.dhsforwarder.run", ERROR, "Could not create listening dns socket, error '%s'", strerror(sock));
		return -1;
	}

	if(::bind(sock, (struct sockaddr*) &dns_listener, sizeof(dns_listener)) < 0){
		Logger::instance()->log("sphirewalld.dhsforwarder.run", ERROR, "Could not bind to dns socket");
		return -1;
	}
	return sock;
}

int DDInterfaceConfiguration::__tcp_query(void *query, char* buffer, int len) {
	struct sockaddr_in socks_server;

	memset(&socks_server, 0, sizeof(socks_server));
	socks_server.sin_family = AF_INET;
	socks_server.sin_port = htons(53);
	socks_server.sin_addr.s_addr = htonl(dns_server);

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0){
		Logger::instance()->log("sphirewalld.dhsforwarder.query", ERROR, "Error creating the outgoing tcp socket, error '%s'", strerror(sock));
		return -1;
	}

	if (connect(sock, (struct sockaddr*)&socks_server, sizeof(socks_server)) < 0){
		Logger::instance()->log("sphirewalld.dhsforwarder.query", ERROR, "Could not connect to dns server at %s", IP4Addr::ip4AddrToString(dns_server).c_str());
		return -1;
	}

	if(send(sock, query, len, 0) < 0){
		Logger::instance()->log("sphirewalld.dhsforwarder.query", ERROR, "Could not send dns request to %s", IP4Addr::ip4AddrToString(dns_server).c_str());
		return -1;
	}

	int ret = recv(sock, buffer, 2048, 0);
	close(sock);
	return ret;
}

void DDInterfaceConfiguration::__handle_dns_query(struct sockaddr_in* dns_client, char* query_buffer, int len, int sock){
	Logger::instance()->log("sphirewalld.dhsforwarder.run", DEBUG, "handling dns query from client");

	char* query = (char*) malloc(len + 3);
	query[0] = 0;
	query[1] = len;
	memcpy(query + 2, query_buffer, len);

	char* buffer_response = (char*) malloc(2048);
	int response_len = __tcp_query(query, buffer_response, len + 2);
	if(response_len){
		if(sendto(sock, buffer_response + 2, response_len - 2, 0, (struct sockaddr *)dns_client, sizeof(struct sockaddr_in)) < 0){
			Logger::instance()->log("sphirewalld.dhsforwarder.run", ERROR, "Could not deliver dns response to client");
		}
	}

	free(query);
	free(buffer_response);
	free(query_buffer);
	free(dns_client);
	Logger::instance()->log("sphirewalld.dhsforwarder.run", DEBUG, "finished dealing with dns query, cleaning up");
}

void DDInterfaceConfiguration::__run_dns_forwarder_listener(){
	if(!interface_ptr){
		return;
	}

	Logger::instance()->log("sphirewalld.dhcpserver.run", INFO, "starting dns forwarder");
	dns_forwarder_listener_running = true;
	int sock = __dns_open_socket();
	if(sock < 0){
		sleep(5);
		dns_forwarder_listener_running = false;
		return;	
	}

	struct pollfd fds[1];
	fds[0].fd = sock; 
	fds[0].events = POLLIN;

	while(poll(fds, 1, 2000) != -1 && interface_ptr && dns_mode == DNS_FORWARD){
		if((fds[0].revents & POLLIN) != 0 && dns_mode == DNS_FORWARD) {
			Logger::instance()->log("sphirewalld.dhcpserver.run", DEBUG, "poll() received POLLIN event on listening socket");

			struct sockaddr_in* dns_client = (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));
			char* query_buffer = (char*) malloc(2048);
			socklen_t dns_client_size = sizeof(struct sockaddr_in);
			int len = recvfrom(sock, query_buffer, 2048, 0, (struct sockaddr *)dns_client, &dns_client_size);
			if (len < 0) {
				Logger::instance()->log("sphirewalld.dhcpserver.run", ERROR, "Request was to small, ignoring");
				free(query_buffer);
				continue;
			}

			char* query = (char*) malloc(len + 3);
			query[0] = 0;
			query[1] = len;
			memcpy(query + 2, query_buffer, len);

			boost::thread(boost::bind(&DDInterfaceConfiguration::__handle_dns_query, this, dns_client, query_buffer, len, sock));
		}
	}

	close(sock);
	dns_forwarder_listener_running = false;
}

bool DDServerManager::__should_dhcp_listen(){
	for(DDInterfaceConfigurationPtr instance : get_instances()){
		if(instance && instance->dhcp_mode != DHCP_SERVER_NONE){
			return true;
		}
	}		
	return false;
}

#define SIZE 1024
void DDServerManager::__run_dhcp_listener(){
	Logger::instance()->log("sphirewalld.dhsforwarder.run", INFO, "Dhcp listener starting");

	while(true){
		//Something might have errored out, lets sleep a bit
		sleep(5);
		if(!__should_dhcp_listen()){
			continue;
		}

		char buffer[SIZE];
		char crap[SIZE];

		int fd=0;
		int iface_index=-1;
		struct iovec io;
		struct sockaddr_in address;
		int broadcast = 1;
		int numbytes;
		struct msghdr msgh;

		if( (fd = socket(AF_INET,SOCK_DGRAM,0)) == -1 )
		{
			Logger::instance()->log("sphirewalld.ddserver._run_dhcp_listener", ERROR, "Could not create socket");
			continue;
		}

		address.sin_family = AF_INET;
		address.sin_port = htons(67);
		address.sin_addr.s_addr = INADDR_ANY;
		if( ::bind(fd, (struct sockaddr *)&address, sizeof(address)) == -1 )
		{
			Logger::instance()->log("sphirewalld.ddserver._run_dhcp_listener", ERROR, "Could not bind to socket");
			close(fd);
			continue;
		}

		if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &broadcast, sizeof broadcast) == -1)
		{
			Logger::instance()->log("sphirewalld.ddserver._run_dhcp_listener", ERROR, "Could not set SO_REUSEADDR on socket");
			close(fd);
			continue;
		}
		int oneopt = 1, mtuopt = IP_PMTUDISC_DONT;
		if (setsockopt(fd, SOL_IP, IP_PKTINFO, &oneopt, sizeof(oneopt)) == -1 ||
				setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &oneopt, sizeof(oneopt)) == -1 ||
				setsockopt(fd, SOL_IP, IP_MTU_DISCOVER, &mtuopt, sizeof(mtuopt)) == -1)
		{
			Logger::instance()->log("sphirewalld.ddserver._run_dhcp_listener", ERROR, "Could not set misc options on socket");
			continue;
		}

		struct pollfd fds[1];
		fds[0].fd = fd;
		fds[0].events = POLLIN;

		while(poll(fds, 1, 2000) != -1)
		{
			if((fds[0].revents & POLLIN) != 0) {
				io.iov_base=buffer;
				io.iov_len=SIZE;
				memset(&msgh,0,sizeof(msgh));
				msgh.msg_iov=&io;
				msgh.msg_iovlen=1;
				msgh.msg_control=&crap;
				msgh.msg_controllen=sizeof(crap);

				//Receive the packet
				if((numbytes = recvmsg(fd,&msgh,0)) == -1)
				{
					Logger::instance()->log("sphirewalld.ddserver._run_dhcp_listener", ERROR, "recvmsg failed, and reported an error");
					break;
				}

				for (struct cmsghdr* cmptr = CMSG_FIRSTHDR(&msgh); cmptr; cmptr = CMSG_NXTHDR(&msgh, cmptr)){
					if (cmptr->cmsg_level == SOL_IP && cmptr->cmsg_type == IP_PKTINFO)
					{
						union {
							unsigned char *c;
							struct in_pktinfo *p;
						} p;
						p.c = CMSG_DATA(cmptr);
						iface_index = p.p->ipi_ifindex;
					}
				}

				//Find interface configuration
				struct dhcp_builder_context* ctx = dhcp_init_context();
				if(dhcp_deserialize(ctx, (char*) &buffer, numbytes) != -1){
					if(ctx->header.op == 1){
						DDInterfaceConfigurationPtr dhcp_instance = instances[iface_index];
						if(dhcp_instance){
							dhcp_instance->handle_dhcp_request(ctx);
						}		
					}else if(ctx->header.op == 2){
						//Find relay reply interface
						for(pair<int, DDInterfaceConfigurationPtr> server_pair: instances){
							if(server_pair.second){
								server_pair.second->handle_dhcp_response(ctx);
							}
						}
					}
				}
				dhcp_free_context(ctx);
			}

			if((time(NULL) - last_cleanup) > CLEANUP_PERIOD){
				Logger::instance()->log("sphirewalld.ddserver._run_dhcp_listener", INFO, "cleaning up state and invalid dhcp leases");
				for(DDInterfaceConfigurationPtr instance : get_instances()){
					if(instance){
						instance->dhcp_cleanup();
					}
				}
				last_cleanup = time(NULL);
			}
		}
		close(fd);
	}
	Logger::instance()->log("sphirewalld.dhsforwarder.run", INFO, "Dhcp server threads closing");
}

void DDServerManager::__run_dns_listeners(){
	while(true){
		for(pair<int, DDInterfaceConfigurationPtr> instance : instances){
			DDInterfaceConfigurationPtr server_instance = instance.second;
			if(server_instance){
				if(!server_instance->dns_forwarder_listener_running && server_instance->dns_mode == DNS_FORWARD){
					boost::thread(boost::bind(&DDInterfaceConfiguration::__run_dns_forwarder_listener, server_instance.get()));
				}
			}	
		}

		sleep(5);
	}
}

void DDServerManager::start(){
	Logger::instance()->log("sphirewalld.dhsforwarder.run", INFO, "Starting ddserver threads");

	for(DDInterfaceConfigurationPtr instance : get_instances()){
		instance->refresh_interfaces(System::getInstance()->get_interface_manager());	
	}

	boost::thread(boost::bind(&DDServerManager::__run_dhcp_listener, this));
	boost::thread(boost::bind(&DDServerManager::__run_dns_listeners, this));

}

bool DDServerManager::load(){
	if (configurationManager->has("ddserver")) {
		ObjectContainer *configRoot = configurationManager->getElement("ddserver");
		ObjectContainer *configDevices = configRoot->get("interfaces")->container();	
		for (int x = 0; x < configDevices->size(); x++) {
			ObjectContainer *configDevice = configDevices->get(x)->container();
			InterfacePtr interface = System::getInstance()->get_interface_manager()->get_interface(configDevice->get("interface")->string());
			if(interface){
				DDInterfaceConfigurationPtr dd_interface = get_server_for_interface(interface);
				dd_interface->dhcp_mode = configDevice->get("dhcp_mode")->number();
				dd_interface->dns_mode = configDevice->get("dns_mode")->number();
				dd_interface->start_ip = IP4Addr::stringToIP4Addr(configDevice->get("start_ip")->string());
				dd_interface->end_ip = IP4Addr::stringToIP4Addr(configDevice->get("end_ip")->string());
				dd_interface->dns_server = IP4Addr::stringToIP4Addr(configDevice->get("dns_server")->string());
				dd_interface->relay_server = IP4Addr::stringToIP4Addr(configDevice->get("relay_server")->string());
				dd_interface->interface = configDevice->get("interface")->string();

				if(configDevice->has("tftp_boot_enabled")){
					dd_interface->tftp_boot_enabled = configDevice->get("tftp_boot_enabled")->boolean();
					dd_interface->tftp_boot_server = configDevice->get("tftp_boot_server")->string();
					dd_interface->tftp_boot_filename = configDevice->get("tftp_boot_filename")->string();
				}

				if(configDevice->has("max_lease_time")){
					dd_interface->max_lease_time = configDevice->get("max_lease_time")->number();
				}

				ObjectContainer* leases = configDevice->get("leases")->container();
				for(int y = 0; y < leases->size(); y++){
					ObjectContainer* lease = leases->get(y)->container();
					DhcpServerLease* new_lease = new DhcpServerLease();
					new_lease->mac_address = lease->get("mac")->string();
					new_lease->ip = IP4Addr::stringToIP4Addr(lease->get("ip")->string());
					new_lease->state = DHCP_SERVER_LEASE_PERMANENT;
					dd_interface->add_leases(DhcpServerLeasePtr(new_lease));
				}

				dd_interface->refresh_interfaces(System::getInstance()->get_interface_manager());	
			}
		}
	}
}

void DDServerManager::save(){
	ObjectContainer *root= new ObjectContainer(CREL); 
	ObjectContainer *configDevices = new ObjectContainer(CARRAY); 

	for(DDInterfaceConfigurationPtr instance : get_instances()){
		ObjectContainer* configDevice = new ObjectContainer(CREL);	
		configDevice->put("dhcp_mode", new ObjectWrapper((double) instance->dhcp_mode));
		configDevice->put("max_lease_time", new ObjectWrapper((double) instance->max_lease_time));
		configDevice->put("dns_mode", new ObjectWrapper((double)instance->dns_mode));
		configDevice->put("start_ip", new ObjectWrapper(IP4Addr::ip4AddrToString(instance->start_ip)));
		configDevice->put("end_ip", new ObjectWrapper(IP4Addr::ip4AddrToString(instance->end_ip)));
		configDevice->put("dns_server", new ObjectWrapper(IP4Addr::ip4AddrToString(instance->dns_server)));
		configDevice->put("relay_server", new ObjectWrapper(IP4Addr::ip4AddrToString(instance->relay_server)));
		configDevice->put("interface", new ObjectWrapper(instance->interface));

		configDevice->put("tftp_boot_enabled", new ObjectWrapper((bool) instance->tftp_boot_enabled));
		configDevice->put("tftp_boot_server", new ObjectWrapper(instance->tftp_boot_server));
		configDevice->put("tftp_boot_filename", new ObjectWrapper(instance->tftp_boot_filename));

		ObjectContainer* leases = new ObjectContainer(CARRAY);
		for(DhcpServerLeasePtr lease : instance->list_leases()){
			if(lease->state == DHCP_SERVER_LEASE_PERMANENT){
				ObjectContainer* lease_oc = new ObjectContainer(CREL);
				lease_oc->put("mac", new ObjectWrapper(lease->mac_address));
				lease_oc->put("ip", new ObjectWrapper(IP4Addr::ip4AddrToString(lease->ip)));
				leases->put(new ObjectWrapper(lease_oc));
			}
		}	

		configDevice->put("leases", new ObjectWrapper(leases));
		configDevices->put(new ObjectWrapper(configDevice));
	}

	root->put("interfaces", new ObjectWrapper(configDevices));

	configurationManager->holdLock();
	configurationManager->setElement("ddserver", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

