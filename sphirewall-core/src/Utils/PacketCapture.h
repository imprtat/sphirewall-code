#ifndef PACKET_CAPTURE_H
#define PACKET_CAPTURE_H

#include <boost/shared_ptr.hpp>
#include <list>
#include <pcap.h>

class CapturedPacket {
	public:
		CapturedPacket(const struct pcap_pkthdr *header, const unsigned char* packet);
		~CapturedPacket();

		const struct pcap_pkthdr *header;
		const u_char *packet;

		std::string to_string();
	private:
		std::string __ipv4_to_string();
		std::string __ipv6_to_string();

};

typedef boost::shared_ptr<CapturedPacket> CapturedPacketPtr;

class PacketCaptureEngine {
	public:
		PacketCaptureEngine();	

		void start_capture();
		void stop_capture();

		std::string capture_filter;
		std::string capture_interface;

		void handle_packet_input(const struct pcap_pkthdr *header, const unsigned char* packet);
		int no_packets_captured(){
			return captured_packets.size();
		}

		bool is_running(){
			return running;
		}

		list<CapturedPacketPtr> get_captured_packets(){
			return captured_packets;
		}

		std::string get_base64_encoded_pcap_string();

		static int CAPTURE_LIMIT;
	private:
		list<CapturedPacketPtr> captured_packets;

		void do_start_capture();
		bool running;
		pcap_t *handle;
};

#endif
