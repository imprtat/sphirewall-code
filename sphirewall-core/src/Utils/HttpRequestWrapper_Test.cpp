/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtest/gtest.h>

#include "Utils/HttpRequestWrapper.h"

/*TEST(HttpRequestWrapper, base){
	HttpRequestWrapper* req = new HttpRequestWrapper("http://mirror.sphirewall.net/resources/list.txt", GET);
	std::string response = req->execute();
 	EXPECT_TRUE(response.compare("10.1.1.1/255.255.255.255\n10.1.1.2/255.255.255.255\n10.1.1.3/255.255.255.255\n") == 0);
	delete req;
}*/
