#
# SYNOPSIS
#
#   AX_SPHIREWALL_LIBS([MINIMUM-VERSION], [ACTION-IF-FOUND], [ACTION-IF-NOT-FOUND])
#
# DESCRIPTION
#
#   Test for the Sphirewall libraries.
#
#   If no path to the installed sphirewall library is given the macro searchs
#   under /usr, /usr/local, /opt and /opt/local and evaluates the
#   $SPHIREWALL_ROOT environment variable. Further documentation is available at
#
#   This macro calls:
#
#     AC_SUBST(SPHIREWALL_CPPFLAGS) / AC_SUBST(SPHIREWALL_LDFLAGS)
#
#   And sets:
#
#     HAVE_SPHIREWALL
#

#serial 1

AC_DEFUN([AX_SPHIREWALL_LIBS],
[
AC_ARG_WITH([sphirewall],
  [AS_HELP_STRING([--with-sphirewall@<:@=ARG@:>@],
    [use Sphirewall Common library from a standard location (ARG=yes),
     from the specified location (ARG=<path>),
     or disable it (ARG=no)
     @<:@ARG=yes@:>@ ])],
    [
    if test "$withval" = "no"; then
        want_sphirewall="no"
    elif test "$withval" = "yes"; then
        want_sphirewall="yes"
        ac_sphirewall_path=""
    else
        want_sphirewall="yes"
        ac_sphirewall_path="$withval"
    fi
    ],
    [want_sphirewall="yes"])


AC_ARG_WITH([sphirewall-libdir],
        AS_HELP_STRING([--with-sphirewall-libdir=LIB_DIR],
        [Force given directory for sphirewall libraries. Note that this will override library path detection, so use this parameter only if default library detection fails and you know exactly where your sphirewall libraries are located.]),
        [
        if test -d "$withval"
        then
                ac_sphirewall_lib_path="$withval"
        else
                AC_MSG_ERROR(--with-sphirewall-libdir expected directory name)
        fi
        ],
        [ac_sphirewall_lib_path=""]
)

if test "x$want_sphirewall" = "xyes"; then
    AC_MSG_CHECKING(for sphirewall-libs)
    succeeded=no

    dnl On 64-bit systems check for system libraries in both lib64 and lib.
    dnl The former is specified by FHS, but e.g. Debian does not adhere to
    dnl this (as it rises problems for generic multi-arch support).
    dnl The last entry in the list is chosen by default when no libraries
    dnl are found, e.g. when only header-only libraries are installed!
    libsubdirs="lib"
    ax_arch=`uname -m`
    if test $ax_arch = x86_64 -o $ax_arch = ppc64 -o $ax_arch = s390x -o $ax_arch = sparc64; then
        libsubdirs="lib64 lib/x86_64-linux-gnu lib"
    fi
    
    dnl first we check the system location for sphirewall libraries
    dnl this location ist chosen if sphirewall libraries are installed with the --layout=system option
    dnl or if you install sphirewall with RPM
    if test "$ac_sphirewall_path" != ""; then
        SPHIREWALL_CPPFLAGS="-I$ac_sphirewall_path/include/sphirewall"
        for ac_sphirewall_path_tmp in $libsubdirs; do
                if test -d "$ac_sphirewall_path"/"$ac_sphirewall_path_tmp" ; then
                        SPHIREWALL_LDFLAGS="-L$ac_sphirewall_path/$ac_sphirewall_path_tmp"
                        break
                fi
        done
    elif test "$cross_compiling" != yes; then
        for ac_sphirewall_path_tmp in /usr /usr/local /opt /opt/local ; do
            if test -d "$ac_sphirewall_path_tmp/include/sphirewall" && test -r "$ac_sphirewall_path_tmp/include/sphirewall"; then
                for libsubdir in $libsubdirs ; do
                    if ls "$ac_sphirewall_path_tmp/$libsubdir/libsphirewallcommon"* >/dev/null 2>&1 ; then break; fi
                done
                SPHIREWALL_LDFLAGS="-L$ac_sphirewall_path_tmp/$libsubdir"
                SPHIREWALL_CPPFLAGS="-I$ac_sphirewall_path_tmp/include/sphirewall"
                break;
            fi
        done
    fi
    dnl overwrite ld flags if we have required special directory with
    dnl --with-sphirewall-libdir parameter
    if test "$ac_sphirewall_lib_path" != ""; then
       SPHIREWALL_LDFLAGS="-L$ac_sphirewall_lib_path"
    fi

    CPPFLAGS_SAVED="$CPPFLAGS"
    CPPFLAGS="$CPPFLAGS $SPHIREWALL_CPPFLAGS"
    export CPPFLAGS

    LDFLAGS_SAVED="$LDFLAGS"
    LDFLAGS="$LDFLAGS $SPHIREWALL_LDFLAGS"
    export LDFLAGS

    AC_REQUIRE([AC_PROG_CXX])
    AC_LANG_PUSH(C++)
        AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
    @%:@include <Core/Logger.h>
    ]], [[]])],[
        AC_MSG_RESULT(yes)
    succeeded=yes
    found_system=yes
        ],[
        ])
    AC_LANG_POP([C++])


    if test "$succeeded" != "yes" ; then
        AC_MSG_NOTICE([[We could not detect the sphirewall libraries.]])
        # execute ACTION-IF-NOT-FOUND (if present):
        ifelse([$3], , :, [$3])
    else
        AC_SUBST(SPHIREWALL_CPPFLAGS)
        AC_SUBST(SPHIREWALL_LDFLAGS)
        AC_DEFINE(HAVE_SPHIREWALL,,[define if the sphirewall library is available])
        # execute ACTION-IF-FOUND (if present):
        ifelse([$2], , :, [$2])
    fi

    CPPFLAGS="$CPPFLAGS_SAVED"
    LDFLAGS="$LDFLAGS_SAVED"
fi

])
