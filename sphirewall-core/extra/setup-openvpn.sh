#!/bin/bash

COUNTRY=$1
PROVINCE=$2
CITY=$3
ORG=$4
EMAIL=$5
NAME=$6

mkdir $NAME
cd $NAME 
mkdir easy-rsa
cp -R /usr/share/doc/openvpn/examples/easy-rsa/2.0/* easy-rsa/
chmod -R +x easy-rsa/

cd easy-rsa/
echo "Overriding envs"
echo '#Envs added by sphirewall' >> vars
echo "export KEY_COUNTRY=\"${COUNTRY}\"" >> vars
echo "export KEY_PROVINCE=\"${PROVINCE}\"" >> vars
echo "export KEY_CITY=\"${CITY}\"" >> vars
echo "export KEY_ORG=\"${ORG}\"" >> vars
echo "export KEY_EMAIL=\"${EMAIL}\"" >> vars

. ./vars
./clean-all
export EASY_RSA="${EASY_RSA:-.}"
"$EASY_RSA/pkitool" --initca $*
"$EASY_RSA/pkitool" --server server

./build-dh

echo "Finished installing openvpn, should now be able to do stuff"

