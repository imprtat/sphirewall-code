from setuptools import setup, find_packages

setup(
    name='sphirewallcp',
    version='1.0.0.6',
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=['Flask', 'gunicorn', 'sphirewallapi']
)
