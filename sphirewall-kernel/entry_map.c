/*
Copyright Michael Lawson
This file is part of sphirewall-kernel for Sphirewall.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/*
 * Id based hash map implementation for holding packet entry structures in memory
 * while we wait for a response form the client.
 *
 * O(1) time, all the time for gets, inserts _can_ cost more, with a max being O(n)
*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <net/netfilter/nf_queue.h>

#include "queue.h"

/*Internal functions*/
static spinlock_t entry_lock;

static const int MAX = 50240;
void* bucket[50240];

int add(void* ptr) {
	static int cursor = 0;
	int c = 0;
	if(cursor >= MAX) {
		cursor = 0;
	}

	cursor++;
	for(; cursor < MAX; cursor++) {
		if(!bucket[cursor]) {
			bucket[cursor] = ptr;
			return cursor;
		}

		c++;
	}

	return -1;
}

void* get(int x) {
	void* ret = bucket[x];
	bucket[x] = NULL;
	return ret;
}
/*End of internal functions*/

/*Exported functions*/
int entry_map_add(struct nf_queue_entry* entry) {
	int ret = -1;
	spin_lock_bh(&entry_lock);

	ret = add((void*) entry);

	spin_unlock_bh(&entry_lock);
	return ret;
}

struct nf_queue_entry* entry_map_get(int id) {
	struct nf_queue_entry* entry = NULL;
	spin_lock_bh(&entry_lock);

	entry = (struct nf_queue_entry*) get(id);

	spin_unlock_bh(&entry_lock);
	return entry;
}
/*EOF*/
