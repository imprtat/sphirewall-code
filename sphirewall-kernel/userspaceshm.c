/*
Copyright Michael Lawson
This file is part of sphirewall-kernel for Sphirewall.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/debugfs.h>
#include <linux/mm.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <net/sock.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <asm/types.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <net/netfilter/nf_queue.h>
#include <linux/msg.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/interrupt.h>

#include <linux/vmalloc.h>
#include <linux/mm.h>
#ifdef MODVERSIONS
#  include <linux/modversions.h>
#endif
#include <asm/io.h>

#include "queue.h"
#include "sphirewall_queue.h"
#include "deadlock.h"

struct dentry  *send_shm_file;
struct dentry  *recv_shm_file;

void mmap_open(struct vm_area_struct *vma)
{
}

void mmap_close(struct vm_area_struct *vma)
{
}

int mmap_fault(struct vm_area_struct *vma, struct vm_fault *vmf){
        vmf->page = virt_to_page(vma->vm_private_data + (vmf->pgoff << PAGE_SHIFT));
	get_page(vmf->page);
	
	return 0;
}

struct vm_operations_struct mmap_vm_ops = {
	.open =     mmap_open,
	.close =    mmap_close,
	.fault=   mmap_fault,
};

int my_mmap(struct file *filp, struct vm_area_struct *vma)
{
	vma->vm_ops = &mmap_vm_ops;
	vma->vm_flags |= VM_RESERVED;
	vma->vm_private_data = filp->private_data;
	mmap_open(vma);

        return 0;
}

void clear_outstanding_packets(void){
	int i = 0, count=0;
	struct nf_queue_entry* entry = NULL;

	PMESSAGE("Clearing outstanding packets from buffers\n");
	for(i = 0; i < 50240; i++){
		entry = entry_map_get(i);
		if(entry != NULL){
			nf_reinject(entry, NF_ACCEPT);
			count++;
			
		}
	}

	PMESSAGE("Removed %d packets from buffers\n", count);
}

int send_shm_open(struct inode *inode, struct file *filep)
{
	filep->private_data = (char *)__get_free_pages(GFP_KERNEL | __GFP_COMP,6);
	qmgr_send->block = queue_init(62, sizeof(struct message) + RAWSIZE, filep->private_data);

	PMESSAGE("Send shared memory queue was opened\n");
	return 0;
}

int send_shm_close(struct inode *inode, struct file *filep)
{
	PMESSAGE("Send shared memory close requested\n");
	qmgr_send->state = STATE_OFFLINE;
	qmgr_send->block= NULL;

	free_pages((unsigned long)filep->private_data, 6);

	PMESSAGE("Send shared memory queue was closed\n");	
	clear_outstanding_packets();
	flush_workqueue(outgoing_packet_worker_wq);
	return 0;
}

int recv_shm_open(struct inode *inode, struct file *filep)
{
	init_deadlock_detection();
	filep->private_data = (char *)__get_free_pages(GFP_KERNEL | __GFP_COMP, 6);
	qmgr_recv->block= queue_init(62, sizeof(struct message) + RAWSIZE, filep->private_data);

	PMESSAGE("Recv shared memory queue was opened\n");
	return 0;
}

int recv_shm_close(struct inode *inode, struct file *filep)
{
	PMESSAGE("Recv shared memory close requested\n");
	qmgr_recv->state = STATE_OFFLINE;
	qmgr_recv->block= NULL;
	free_pages((unsigned long)filep->private_data, 6);
	PMESSAGE("Recv shared memory queue was closed\n");

	flush_workqueue(incomming_packets_worker_wq);
	clear_outstanding_packets();
	
	return 0;
}

long send_queue_ioctl_notify(struct file *filep, unsigned int cmd, unsigned long arg) {
        if(cmd == 101){
		PMESSAGE("Received ONLINE event from userspace for send queue\n");
                qmgr_send->state = STATE_ONLINE; 
        }
        return 1;
}

long recv_queue_ioctl_notify(struct file *filep, unsigned int cmd, unsigned long arg) {
	if(cmd == 100){
		queue_work(incomming_packets_worker_wq, &incomming_packets_worker);
	}else if(cmd == 101){
		PMESSAGE("Received ONLINE event from userspace for recv queue\n");
		qmgr_recv->state = STATE_ONLINE;	
	}
	return 1;
}

static const struct file_operations send_shm_ops= {
	.open = send_shm_open,
	.release = send_shm_close,
	.mmap = my_mmap,
	.unlocked_ioctl =send_queue_ioctl_notify,
};

static const struct file_operations recv_shm_ops= {
	.open = recv_shm_open,
	.release = recv_shm_close,
	.mmap = my_mmap,
	.unlocked_ioctl= recv_queue_ioctl_notify,
};


int open_shm(void){
	PMESSAGE("Creating shared memory segments\n");
	send_shm_file= debugfs_create_file("sphirewall_send", 0644, NULL, NULL, &send_shm_ops);
	recv_shm_file = debugfs_create_file("sphirewall_recv", 0644, NULL, NULL, &recv_shm_ops);

	if(send_shm_file == NULL || send_shm_file == ERR_PTR(-ENODEV) || recv_shm_file == NULL || recv_shm_file == ERR_PTR(-ENODEV)){
		return -1;
	}

	return 0;
}

void close_shm(void)
{
	PMESSAGE("Closing shared memory segments\n");
	debugfs_remove(send_shm_file);
	debugfs_remove(recv_shm_file);
}
