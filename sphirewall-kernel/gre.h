#ifndef GRE_H
#define GRE_H

struct grehdr {
        __u8   flags;           /* bitfield */
        __u8   version;         /* should be GRE_VERSION_PPTP */
        __be16 protocol;        /* should be GRE_PROTOCOL_PPTP */
        __be16 payload_len;     /* size of ppp payload, not inc. gre header */
        __be16 call_id;         /* peer's call_id for this session */
        __be32 seq;             /* sequence number.  Present if S==1 */
        __be32 ack;             /* seq number of highest packet received by */
                                /*  sender in this session */
};

#endif

