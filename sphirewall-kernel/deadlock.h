#ifndef DEADLOCK_H
#define DEADLOCK_H

extern void update_deadlock_detection(void);
extern void poll_deadlock_detection(void);
extern void init_deadlock_detection(void);

#endif
