from subprocess import CalledProcessError
from flask import render_template, request, session, flash
from flask.ext.login import login_required, current_user
from sphirewallapi.sphirewall_api import SphirewallClient
from sphirewallconnector import app
from flask import redirect
from sphirewallconnector.routes_authentication import get_sphirewall_client
from flask import jsonify


@app.route("/", methods=["GET"])
@login_required
def root():
    device_id = get_sphirewall_client(current_user.get_id()).general().configuration("cloud:deviceid")
    device_key = get_sphirewall_client(current_user.get_id()).general().configuration("cloud:key")
    network_devices = get_sphirewall_client(current_user.get_id()).network().devices()
    cloud_connected = get_sphirewall_client(current_user.get_id()).general().cloud_connected()
    captive_portal_enabled = get_sphirewall_client(current_user.get_id()).general().advanced_value(key="CAPTURE_PORTAL_FORCE")

    network_configured = False
    for interface in network_devices:
        if interface["interface"] != "lo":
            if len(interface["ipv4"]) > 0:
                network_configured = True
                break

    return render_template(
        "home.html",
        device_id=device_id, device_key=device_key,
        network_devices=network_devices, cloud_connected=cloud_connected,
        captive_portal_enabled=captive_portal_enabled, network_configured=network_configured
    )


@app.route("/", methods=["POST"])
@login_required
def root_savedevice():
    device_id = request.form.get("device_id")
    device_key = request.form.get("device_key")

    #password set
    password = request.form.get("password")
    repassword = request.form.get("repassword")
    if password and repassword:
        if repassword == password:
            get_sphirewall_client(current_user.get_id()).general().user_set_password("admin", password)
        else:
            flash("Passwords did not match")

    if device_key and device_id:
        get_sphirewall_client(current_user.get_id()).general().configuration("cloud:enabled", True)
        get_sphirewall_client(current_user.get_id()).general().configuration("cloud:deviceid", device_id)
        get_sphirewall_client(current_user.get_id()).general().configuration("cloud:key", device_key)

    return redirect("/")


PATH = "export PATH=/usr/bin:/bin:/usr/sbin:/sbin"
NON_INTERACTIVE = "export DEBIAN_FRONTEND=noninteractive"


@app.route("/upgrade", methods=["GET"])
@login_required
def upgrade():
    return render_template("upgrade.html")


@app.route("/dump", methods=["GET"])
@login_required
def dump():
    return render_template("dump.html")


@app.route("/ajax/upgrade", methods=["GET"])
@login_required
def ajax_upgrade():
    import subprocess

    output = ""
    try:
        output += subprocess.check_output("scli2 --connect False update", stderr=subprocess.STDOUT, shell=True)
        return jsonify(output=output.replace("\n", "<br>"), success=True)
    except CalledProcessError as e:
        output += e.output
        return jsonify(output=output, success=False)


@app.route("/ajax/dump", methods=["GET"])
@login_required
def ajax_dump():
    import subprocess

    output = ""
    try:
        output += subprocess.check_output("scli2 --connect False dump --upload ftp://sphirewalldumps:sphirewalldumps131211@sphirewall.net/", stderr=subprocess.STDOUT, shell=True)
        return jsonify(output=output.replace("\n", "<br>"), success=True)
    except CalledProcessError as e:
        output += e.output
        return jsonify(output=output, success=False)
