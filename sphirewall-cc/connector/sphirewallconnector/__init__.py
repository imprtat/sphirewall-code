from flask import Flask, session
from flask.ext.login import LoginManager
from sphirewallapi.sphirewall_api import SphirewallClient
from sphirewallapi.sphirewall_connection import SphirewallSocketTransportProvider

__author__ = 'michaellawson'

app = Flask(__name__)
app.secret_key = "1234"

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

HOST, PORT = "localhost", 8001

def get_sphirewall_client(token=None):
    transport_provider = SphirewallSocketTransportProvider(HOST, PORT)
    return SphirewallClient(transport_provider, token)

from sphirewallconnector import routes
from sphirewallconnector import routes_authentication
