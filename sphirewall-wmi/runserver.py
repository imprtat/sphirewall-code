#!/usr/bin/env python2

from sphirewallwmi import app
app.run(host='0.0.0.0', port=5610, debug=False, threaded=True)
