import json
from string import replace
import uuid
from flask import jsonify, flash
from flask.ext.login import login_required

from flask.globals import request, session, current_app
from flask.templating import render_template
from flask.wrappers import Response
from werkzeug.utils import redirect

from sphirewallwmi import app
from sphirewallwmi.api.utils import checkbox_value, text_value, int_value
from sphirewallwmi.middleware import service


@login_required
@app.route("/device/network/devices")
def network_devices():
    return render_template(
        "device_network_devices.html", routes=current_app.sphirewall().network().routes(),
        devices=current_app.sphirewall().network().devices(configured=True), dns_config=current_app.sphirewall().network().dns_config()
    )


@login_required
@app.route("/device/network/devices/<device>/up")
def network_devices_up(device):
    current_app.sphirewall().network().devices_up(device)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/<device>/down")
def network_devices_down(device):
    current_app.sphirewall().network().devices_down(device)
    return redirect("/device/network/devices")


@app.route("/device/network/dns", methods=["POST"])
def network_dns():
    current_app.sphirewall().network().dns_config_set(text_value("domain"), text_value("search"), text_value("ns1"), text_value("ns2"))
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/<device>/delete")
def network_devices_delete(device):
    current_app.sphirewall().network().devices_remove(device)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/create", methods=["GET", "POST"])
@app.route("/device/network/devices/<device>", methods=["GET", "POST"])
def network_devices_edit(device=None):
    if request.method == "POST":
        device_params = {}

        existing_device_configuration = None
        if device:
            existing_device_configuration = current_app.sphirewall().network().devices(device, configured=True)

        device_params["name"] = text_value("name")
        device_params["persisted"] = checkbox_value("persisted")
        if text_value("type") == "physical":
            device_params["physical"] = True
            device_params["physicalInterface"] = text_value("device")

        if text_value("type") == "vlan":
            device_params["vlan"] = True
            device_params["vlanId"] = int(text_value("vlanId"))
            device_params["vlanInterface"] = text_value("vlanInterface")

        if text_value("type") == "bridge" or (existing_device_configuration and existing_device_configuration.get("bridge")):
            device_params["bridge"] = True
            device_params["bridgeDevices"] = request.form.getlist('bridgeDevices')

        if text_value("type") == "lacp" or (existing_device_configuration and existing_device_configuration.get("lacp")):
            device_params["lacp"] = True
            device_params["lacpDevices"] = request.form.getlist('lacpDevices')

            device_params["lacpMode"] = int(text_value("lacpMode", "0", False))
            device_params["lacpLinkStatePollFrequency"] = int(text_value("lacpLinkStatePollFrequency", "0", False))
            device_params["lacpLinkStateDownDelay"] = int(text_value("lacpLinkStateDownDelay", "0", False))
            device_params["lacpLinkStateUpDelay"] = int(text_value("lacpLinkStateUpDelay", "0", False))

        ipv4_addresses = []
        if text_value("aliases"):
            for raw_address in text_value("aliases").split("\n"):
                if raw_address and len(raw_address.split("/")) == 3:
                    address = {
                        "ip": raw_address.split("/")[0].strip(),
                        "mask": raw_address.split("/")[1].strip(),
                        "broadcast": raw_address.split("/")[2].strip()
                    }
                    ipv4_addresses.append(address)

        address_params = {
            "ipv4": ipv4_addresses,
            "dhcp": checkbox_value("dhcp"),
            "gateway": text_value("gateway"),
        }

        dhcp_server = {
            "dhcpServerMode": int(text_value("dhcpServerMode", default="1")),
            "dhcpServerStart": text_value("dhcpServerStart"),
            "dhcpServerEnd": text_value("dhcpServerEnd"),
            "dhcpServerRelay": text_value("dhcpServerRelay"),
            "dhcpServerLeaseTime": int_value("dhcpServerLeaseTime", default=7200),

            "dhcpServerDnsMode": int(text_value("dhcpServerDnsMode", default="0")),
            "dhcpServerDnsServer": text_value("dhcpServerDnsServer"),

            "dhcpServerTftpEnabled": checkbox_value("dhcpServerTftpEnabled"),
            "dhcpServerTftpServer": text_value("dhcpServerTftpServer"),
            "dhcpServerTftpFilename": text_value("dhcpServerTftpFilename")
        }

        current_app.sphirewall().network().devices_set(
            device, device_params=device_params, address_params=address_params, dhcp_server=dhcp_server
        )

        if device:flash("Saved interface configuration")
        else:flash("Created a new virtual device")
        return redirect("/device/network/devices")

    current_device_stored = None
    if device:
        current_device_stored = current_app.sphirewall().network().devices(device, configured=True)
    return render_template(
        "device_network_devices_edit.html", device=current_device_stored,
        devices=current_app.sphirewall().network().devices(configured=True)
    )


@login_required
@app.route("/device/network/devices/<device>/leases", methods=["GET", "POST"])
def network_devices_dhcp(device):
    if request.method == "POST":
        flash("Created new static dhcp lease for '%s'" % text_value("mac"))
        current_app.sphirewall().network().devices_dhcp_static_add(device, text_value("ip"), text_value("mac"))
    device = current_app.sphirewall().network().devices(device=device, configured=True)
    return render_template("device_network_devices_edit_dhcp.html", device=device)


@login_required
@app.route("/device/network/devices/<device>/leases/delete/<ip>/<mac>", methods=["GET"])
def network_devices_dhcp_lease_delete(device, ip, mac):
    flash("Removed dhcp lease for '%s'" % mac)
    current_app.sphirewall().network().devices_dhcp_static_del(device, ip, mac)
    return redirect("/device/network/devices/%s/leases" % device)


@login_required
@app.route("/device/network/devices/routes", methods=["POST"])
def network_devices_routes():
    flash("Added new static route")
    current_app.sphirewall().network().routes_add(text_value("destination"), text_value("destination_cidr"), text_value("route_device"), text_value("route_nexthop"))
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/routes/del/<id>", methods=["GET"])
def network_devices_routes_del(id):
    flash("Deleted static route")
    current_app.sphirewall().network().routes_del(id)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/connections", methods=["GET", "POST"])
def network_connections():
    if request.method == "POST":
        current_app.sphirewall().network().connections_add(text_value("name"), text_value("type"))
    return render_template("device_network_connections.html", connections=current_app.sphirewall().network().connections())


@login_required
@app.route("/device/network/connections/delete/<name>", methods=["GET"])
def network_connections_del(name):
    current_app.sphirewall().network().connections_del(name)
    return redirect("/device/network/connections")


@login_required
@app.route("/device/network/connections/<name>", methods=["GET", "POST"])
def network_connections_edit(name):
    connection = current_app.sphirewall().network().connections(name)
    if request.method == "POST":
        if connection["type"] == 0:
            current_app.sphirewall().network().connections_pppoe_save(
                name, text_value("username"), text_value("password"), text_value("authenticationType"), text_value("device"))
        if connection["type"] == 1:
            current_app.sphirewall().network().connections_openvpn_save(name, text_value("keyfile"))

    connection = current_app.sphirewall().network().connections(name)
    return render_template("device_network_connection_edit.html", connection=connection,
                           log=replace(connection["log"], "\n", "<br>"))


@login_required
@app.route("/device/network/connections/<name>/connect")
def network_connections_connect(name):
    current_app.sphirewall().network().connection_connect(name)
    return redirect("/device/network/connections/" + name)


@login_required
@app.route("/device/network/connections/<name>/disconnect")
def network_connections_disconnect(name):
    current_app.sphirewall().network().connection_disconnect(name)
    return redirect("/device/network/connections/" + name)


@login_required
@app.route("/device/network/vpn", methods=["GET", "POST"])
def network_vpn():
    if request.method == "POST":
        flash("Created a new gateway to gateway vpn connection")
        current_app.sphirewall().network().vpns_ipsec_create(text_value("name"))
        return redirect("/device/network/vpn/%s" % text_value("name"))

    vpns = current_app.sphirewall().network().vpns()
    return render_template(
        "device_network_sitetositevpn.html",
        vpns=vpns
    )


@login_required
@app.route("/device/network/vpn/<name>/delete", methods=["GET", "POST"])
def network_vpn_delete(name):
    flash("Deleted a gateway to gateway vpn connection '%s" % name)
    current_app.sphirewall().network().vpns_delete(name)
    return redirect("/device/network/vpn")


def network_vpn_edit_ipsec_gw_gw_post(name):
    options = {
        "local_device": text_value("local_device"),
        "local_shared_device": text_value("local_shared_device"),
        "local_id": text_value("local_id"),
        "remote_host": text_value("remote_host"),
        "remote_network": text_value("remote_network"),
        "remote_subnet": text_value("remote_subnet"),
        "remote_id": text_value("remote_id"),
        "auto_configure_firewall": checkbox_value("auto_configure_firewall"),

        "auth_mode": int(text_value("auth_mode")),
        "secret_key": text_value("secret_key"),
        "auto_start": checkbox_value("auto_start"),

        "meta": json.dumps({"cloud_type": "manual"})
    }
    current_app.sphirewall().network().vpns_options(name, options)


def network_vpn_edit_ipsec_l2tp_gw_post(name):
    options = {
        "local_device": text_value("local_device"),
        "auth_mode": int(text_value("auth_mode")),
        "secret_key": text_value("secret_key"),
        "auto_start": checkbox_value("auto_start"),

        "vpn_assign_ip": checkbox_value("vpn_assign_ip"),
        "auto_configure_firewall": checkbox_value("auto_configure_firewall"),
        "vpn_remote_ip_range_start": text_value("vpn_remote_ip_range_start"),
        "vpn_remote_ip_range_end": text_value("vpn_remote_ip_range_end"),
        "vpn_local_ip": text_value("vpn_local_ip"),
        "user_authentication_mode": int(text_value("user_authentication_mode")),
        "groups": [int(group) for group in request.form.getlist("groups")],

        "provide_dns": checkbox_value("provide_dns"),
        "dns_ns1": text_value("dns_ns1"),
        "dns_ns2": text_value("dns_ns2")
    }
    current_app.sphirewall().network().vpns_clientvpn_options(options)


@login_required
@app.route("/device/network/vpn/<name>", methods=["GET", "POST"])
def network_vpn_edit(name):
    if request.method == "POST":
        flash("Modified the gateway to gateway vpn connection '%s" % name)
        network_vpn_edit_ipsec_gw_gw_post(name)

    vpn = current_app.sphirewall().network().vpns(name)
    network_devices = current_app.sphirewall().network().devices()
    if vpn.get("meta") and len(vpn.get("meta")) > 0:
        vpn["meta"] = json.loads(vpn["meta"])

    return render_template(
        "device_network_sitetositevpn_edit.html", vpn=vpn,
        log=replace(vpn["log"], "\n", "<br>"),
        local_devices=network_devices
    )


@login_required
@app.route("/device/network/ids", methods=["GET", "POST"])
def network_ids():
    if request.method == "POST":
        flash("Saved IDS configuration")
        current_app.sphirewall().network().ids_set(checkbox_value("enabled"), text_value("interface"))

    return render_template(
        "device_network_ids.html", ids=current_app.sphirewall().network().ids(),
        devices=current_app.sphirewall().network().devices()
    )


@login_required
@app.route("/device/network/vpn/<name>/logs", methods=["GET"])
def network_vpn_logs(name):
    vpn = current_app.sphirewall().network().vpns(name)
    return render_template(
        "device_network_sitetositevpn_logs.html", vpn=vpn,
        log=replace(vpn["log"], "\n", "<br>"),
    )


@login_required
@app.route("/device/network/clientvpn", methods=["GET", "POST"])
def network_clientvpn_edit():
    if request.method == "POST":
        flash("Saved client vpn server configuration")
        network_vpn_edit_ipsec_l2tp_gw_post("clientvpn")

    vpn = current_app.sphirewall().network().vpns_clientvpn()
    return render_template(
        "device_network_clientvpn.html", vpn=vpn,
        groups=current_app.sphirewall().general().groups(),
        devices=current_app.sphirewall().network().devices()
    )


@app.route("/device/network/clientvpn/clients")
def network_clientvpn_clients():
    vpn = current_app.sphirewall().network().vpns_clientvpn()
    return render_template("device_network_clientvpn_clients.html", vpn=vpn)


@login_required
@app.route("/device/network/vpn/<name>/start")
def network_vpn_start(name):
    flash("Attempted to start gateway to gateway vpn connection '%s" % name)
    current_app.sphirewall().network().vpns_start(name)
    return redirect("/device/network/vpn/" + name)


@login_required
@app.route("/device/network/vpn/<name>/stop")
def network_vpn_stop(name):
    flash("Attempted to stop gateway to gateway vpn connection '%s" % name)
    current_app.sphirewall().network().vpns_stop(name)
    return redirect("/device/network/vpn/" + name)


@login_required
@app.route("/device/network/wireless", methods=["GET", "POST"])
def network_wireless():
    if request.method == "POST":
        current_app.sphirewall().general().configuration("wireless:enabled", checkbox_value("enabled"))
        current_app.sphirewall().general().configuration("wireless:interface", text_value("interface"))
        current_app.sphirewall().general().configuration("wireless:ssid", text_value("ssid"))
        current_app.sphirewall().general().configuration("wireless:channel", int(text_value("channel")))
        current_app.sphirewall().general().configuration("wireless:driver", text_value("driver"))

        if checkbox_value("enabled"):
            current_app.sphirewall().network().wireless_start()
        else:
            current_app.sphirewall().network().wireless_stop()
        flash("Saved wireless access point configuration")

    enabled = current_app.sphirewall().general().configuration("wireless:enabled")
    interface = current_app.sphirewall().general().configuration("wireless:interface")
    channel = current_app.sphirewall().general().configuration("wireless:channel")
    ssid = current_app.sphirewall().general().configuration("wireless:ssid")
    driver = current_app.sphirewall().general().configuration("wireless:driver")
    online = current_app.sphirewall().network().wireless_online()
    devices = current_app.sphirewall().network().devices()

    return render_template(
        "device_network_wireless.html", enabled=enabled, interface=interface, channel=channel, ssid=ssid,
        online=online, driver=driver, devices=devices
    )


@login_required
@app.route("/device/network/dyndns", methods=["GET", "POST"])
def network_dyndns():
    if request.method == "POST":
        current_app.sphirewall().network().dyndns_configure(
            checkbox_value("enabled"), text_value("username"), text_value("password"), text_value("domain"), text_value("type")
        )
        flash("Saved dynamic dns configuration")
    return render_template("device_network_dyndns.html", config=current_app.sphirewall().network().dyndns_get())


@login_required
@app.route("/device/network/tor", methods=["GET", "POST"])
def network_tor():
    if request.method == "POST":
        current_app.sphirewall().network().tor_set(
            checkbox_value("enabled"), text_value("interface"), checkbox_value("dns")
        )
        flash("Saved tor gateway configuration")

    tor_configuration = current_app.sphirewall().network().tor()
    status = current_app.sphirewall().network().tor_status()
    return render_template(
        "device_network_tor.html", status=status["status"], devices=current_app.sphirewall().network().devices(),
        dns=tor_configuration["dns"], enabled=tor_configuration["enabled"], interface=tor_configuration["interface"], logs=status["logs"].replace("\n", "<br>")
    )


@login_required
@app.route("/device/network/tor/start")
def network_tor_start():
    flash("Starting tor gateway")
    current_app.sphirewall().network().tor_start()
    return redirect("/device/network/tor")


@login_required
@app.route("/device/network/tor/stop")
def network_tor_stop():
    flash("Stopping tor gateway")
    current_app.sphirewall().network().tor_stop()
    return redirect("/device/network/tor")


@login_required
@app.route("/device/network/mdns")
def network_mdns():
    return render_template(
        "device_network_mdns.html", bridges=current_app.sphirewall().network().mdns()
    )


@login_required
@app.route("/device/network/mdns/create", methods=["GET", "POST"])
@app.route("/device/network/mdns/<id>/edit", methods=["GET", "POST"])
def network_mdns_edit(id=None):
    if request.method == "POST":
        if not id:
            flash("Created a new Bonjour/Mdns Gateway")
        else:
            flash("Modified a Bonjour/Mdns Gateway")

        current_app.sphirewall().network().mdns_set(id, text_value("name"), request.form.getlist("slaves"))
        return redirect("/device/network/mdns")

    return render_template(
        "device_network_mdns_edit.html", bridge=current_app.sphirewall().network().mdns(id), devices=current_app.sphirewall().network().devices()
    )


@login_required
@app.route("/device/network/mdns/<id>/del", methods=["GET"])
def network_mdns_del(id):
    flash("Deleted a Mdns/Bonjour Gateway")
    current_app.sphirewall().network().mdns_del(id)
    return redirect("/device/network/mdns")

