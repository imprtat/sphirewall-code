var protocols =[
    {port:20,name:"ftp",desc:"Ftp"},
    {port:21,name:"ftp",desc:"Ftp"},
    {port:22,name:"ssh",desc:"Ssh secure login"},
    {port:23,name:"telnet",desc:"Telnet remote login"},
    {port:25,name:"smtp",desc:"SMTP Mail Sender"},
    {port:53,name:"dns",desc:"Domain name service"},
    {port:67,name:"dhcp/bootp",desc:"Dynamic address allocation"},
    {port:68,name:"dhcp/bootp",desc:"Dynamic address allocation"},
    {port:80,name:"http",desc:"Http web protocol"},
    {port:123,"name":"ntp",desc:"Network Time Protocol"},
    {port:443,"name":"https",desc:"Https web protocol"},
    {port:465,"name":"smtps",desc:"SMTP SSL Mail Sender"},
    {port:587,"name":"smtp",desc:"SMTP Mail Sender"},
    {port:3128,"name":"proxy",desc:"Http web proxy"},
    {port:8001,"name":"sphirewall",desc:"Sphirewall management port"},
    {port:8080,"name":"proxy",desc:"Http web proxy"},
    {port:1935,"name":"rtmp",desc:"Adobe Flash Media Server connection port, Real Time Messaging Protocol"}
];

function getservicenamebyport(port){
    for(var i=0; i<protocols.length; i++){
        if(protocols[i].port==port){
            return protocols[i].name;
        }
    }

    return "unknown"
}

function getservicenamebyportsafe(port){
    for(var i=0; i<protocols.length; i++){
        if(protocols[i].port==port){
            return protocols[i].name;
        }
    }

    return port;
}

function getservicedescriptionbyport(port){
    for(var i=0; i<protocols.length; i++){
        if(protocols[i].port==port){
            return protocols[i].desc;
        }
    }

    return "unknown"
}

/*Decorator object for fields market with the 'port' class*/
$(function(){
    $(".port").each(function(){
        var port = $(this).html();
        var name =getservicenamebyport(parseInt(port));
        var desc =getservicedescriptionbyport(parseInt($(this).html()));

        var markup = "<b><span title='"+desc+"'>"+name+"</span></b> ("+port+")";

        $(this).html(markup);
    });
});

