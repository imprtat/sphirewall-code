from flask import request, session, redirect, flash, current_app
import flask
from flask.ext.login import current_user, login_required, logout_user
from sphirewallapi.sphirewall_api import SessionTimeoutException, MethodNotFoundException, MissingParamException, TransportProviderException, GeneralApplicationException
from sphirewallwmi import app, navigator
from sphirewallwmi.api.utils import get_class
from sphirewallwmi.routes_login import get_session_sphirewall_hostname


def standard_error_handler(error):
    flash(str(error))
    return redirect("/login?error=" + str(error))


@app.errorhandler(SessionTimeoutException)
def on_error_session_timeout(e):
    logout_user()
    session.clear()
    return redirect("/")


@app.errorhandler(500)
def on_error(error):
    return get_class(app.config["STANDARD_ERROR_HANDLER"])(error)


@app.errorhandler(MethodNotFoundException)
def on_error_method_not_found(e):
    return get_class(app.config["STANDARD_DEVICE_ERROR_HANDLER"])("Api method '%s' could not be found, your device may be out of date, please upgrade." % e.message)


@app.errorhandler(MissingParamException)
def on_error_param_not_found(e):
    return get_class(app.config["STANDARD_DEVICE_ERROR_HANDLER"])("Api parameter '%s' was not found, this can occur if the api library is out of date. " % e.message)


@app.errorhandler(GeneralApplicationException)
def on_error_param_not_found(e):
    return get_class(app.config["STANDARD_DEVICE_ERROR_HANDLER"])("A general exception occurred, '%s'. " % e.message)


@app.errorhandler(TransportProviderException)
def on_error_param_not_found(e):
    return get_class(app.config["STANDARD_DEVICE_ERROR_HANDLER"])("Could not connect to device")


@app.before_request
def before_routes():
    flask.g.productname = app.config["PRODUCT_NAME"]

    if current_user.is_authenticated():
        flask.g.inheader = get_class(app.config["INHEADER_GENERATOR"])()
        flask.g.analytics_available = get_class(app.config["ANALYTICS_PROVIDER"])().available()
        flask.g.edgewize = get_class(app.config["ANALYTICS_PROVIDER"])().available()
        flask.g.navigator = navigator

@app.route("/")
@login_required
def home():
    flash("Welcome, you were logged in successfully")
    return redirect("/device")


def standard_inheader():
    return "Locally Managing '%s'" % get_session_sphirewall_hostname()