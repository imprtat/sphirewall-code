import json
from flask import current_app
from sphirewallapi.sphirewall_api import SphirewallClient, SphirewallSocketTransportProvider
from sphirewallwmi.routes_login import get_session_sphirewall_hostname, get_session_sphirewall_port, get_session_sphirewall_token


class SphirewallAnalyticsProvider():

    def __init__(self):
        pass

    def available(self):
        return False

    def statistics_metrics(self, params):
        return {"result": current_app.sphirewall().general().metrics()}

    def statistics_metrics_specific(self, metric, params):
        return {"result": current_app.sphirewall().general().metrics_spec(metric)}

    def events(self, params):
        events_list = get_sphirewall_direct().general().events_list()
        final_event_list = []
        for event in events_list:
            if not params.get("search") or len(params.get("search")) == 0:
                final_event_list.append(event)
                continue
            if params.get("search") in json.dumps(event.get("params")) or params.get("search") in event.get("key"):
                final_event_list.append(event)

        offset = params["offset"]
        limit = params["limit"]
        return final_event_list[int(offset): int(offset) + int(limit)]

    def alert_configuration(self):
        return []

    def update_alert_configuration(self, conditions, recipients, permission_recipients):
        return {}


def standard_analytics_provider():
    return SphirewallAnalyticsProvider()


def get_sphirewall_direct():
    transport_provider = SphirewallSocketTransportProvider(get_session_sphirewall_hostname(), get_session_sphirewall_port())
    client = SphirewallClient(transport_provider, access_token=get_session_sphirewall_token())
    return client
