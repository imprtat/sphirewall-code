import json
from string import replace
from flask.ext.login import login_required

from flask.globals import request, current_app
from flask.templating import render_template
from werkzeug.utils import redirect

from sphirewallwmi import app
from sphirewallwmi.api.utils import DateHelper, checkbox_value, text_value


@login_required
@app.route('/device')
def main():
    return render_template(
        'device_home.html',
        totalUsers=len(current_app.sphirewall().general().active_users_list()),
        totalHosts=current_app.sphirewall().general().hosts_size(),
        totalConnection=current_app.sphirewall().firewall().connections_size(),
        version=current_app.sphirewall().version(),
        events=current_app.sphirewall().general().events_size(),
        token=current_app.sphirewall().get_token())


@login_required
@app.route("/device/status/events")
def status_events():
    return render_template("device_status_events.html", params=process_params())


@app.route("/device/status/users")
@login_required
def status_users():
    return render_template("device_status_activeusers.html", params=process_params())


@app.route("/device/status/hosts")
@login_required
def status_hosts():
    return render_template("device_status_activehosts.html", params=process_params())


@app.route("/device/status/hosts/<mac>/<ip>", methods=["GET", "POST"])
@login_required
def status_hosts_edit(mac, ip):
    if request.method == "POST":
        current_app.sphirewall().general().host_quarantine(mac, ip, checkbox_value("quarantined"))
    host = current_app.sphirewall().general().host(mac, ip)
    return render_template("device_status_activehosts_edit.html", host=host, latency=current_app.sphirewall().network().ping(ip))


@login_required
@app.route("/device/status/clients/disconnect/<address>")
def status_clients_disconnect(address):
    current_app.sphirewall().general().disconnect_session(address)
    return redirect("/device/status/users")


@login_required
@app.route("/device/status/connections")
def status_connections():
    return render_template("device_status_connections.html", params=process_params())


@login_required
@app.route("/device/status/connections/terminate/<sourceIp>/<sourcePort>/<destIp>/<destPort>")
def status_connections_terminate(sourceIp, sourcePort, destIp, destPort):
    current_app.sphirewall().firewall().connection_terminate(sourceIp, sourcePort, destIp, destPort)
    return redirect("/status/connections")


def safedivide(numerator, divisor):
    if divisor == 0:
        return 0
    return numerator / divisor


@login_required
@app.route("/device/status/metrics")
def status_metrics():
    params = process_params()
    return render_template(
        "device_status_metrics.html", params=params)


@login_required
@app.route("/device/status/metrics/all")
def status_metrics_all():
    params = process_params()
    stats = current_app.service().statistics_metrics(process_params())
    return render_template("device_status_metrics_all.html", params=params, stats=stats)


@login_required
@app.route("/device/status/logs", methods=["POST", "GET"])
def status_logs():
    filter = text_value("filter", default="")
    no_lines = int(text_value("no_lines", default=100))
    if filter and len(filter) > 0:
        general__logs = current_app.sphirewall().general().logs(filter=filter, no_lines=no_lines)
    else:
        general__logs = current_app.sphirewall().general().logs(no_lines=no_lines)

    return render_template(
        "device_status_logs.html", logs=replace(general__logs, "\n", "<br>"), filter=filter, no_lines=no_lines
    )


@login_required
@app.route("/device/status/connections/states")
def status_connections_states():
    return render_template("device_status_connections_states.html")


def process_params():
    params = {}
    if "startDate" in request.args and len(request.args["startDate"]) > 0:
        params["startDate"] = request.args["startDate"]
    elif cookie_value("startDate") is not None:
        params["startDate"] = cookie_value("startDate")
    else:
        params["startDate"] = DateHelper.yesterday().isoformat()

    if "endDate" in request.args and len(request.args["endDate"]) > 0:
        params["endDate"] = request.args["endDate"]
    elif cookie_value("endDate") is not None:
        params["endDate"] = cookie_value("endDate")
    else:
        params["endDate"] = DateHelper.today().isoformat()

    if "offset" in request.args:
        params["offset"] = request.args["offset"]
    if "limit" in request.args:
        params["limit"] = request.args["limit"]
    if "search" in request.args:
        params["search"] = request.args["search"]

    return params


def cookie_value(key):
    return request.cookies.get(key)


@login_required
@app.route("/device/status/user/<username>")
def status_active_user(username):
    sessions = current_app.sphirewall().general().active_users_list()
    user_sessions = []
    for session in sessions:
        if session["user"] == username:
            user_sessions.append(session)

    user=current_app.sphirewall().general().users(username)
    return render_template(
        "device_status_activeuser_edit.html", username=username, sessions=user_sessions, user=user
    )
