import json

import operator
from flask import flash, current_app
from flask.ext.login import login_required

from flask.globals import request, session
from flask.templating import render_template
from sphirewallapi.sphirewall_api import SphirewallClient
from sphirewallapi.sphirewall_connection import SphirewallSocketTransportProvider
from werkzeug.utils import redirect

from sphirewallwmi import app
from sphirewallwmi.api.utils import checkbox_value, nint, text_value, arg, int_value
from sphirewallapi.sphirewall_api_general import QuotaInfo


@app.route("/device/configuration/general", methods=["POST", "GET"])
@login_required
def configuration_general():
    if request.method == "POST":
        current_app.sphirewall().general().configuration("general:disable_remote_management", checkbox_value("disable_remote_management"))
        flash("Saved general configuration settings")
    drm = current_app.sphirewall().general().configuration("general:disable_remote_management")

    return render_template(
        "device_configuration_general.html",
        disable_remote_management=drm,
        pools=current_app.sphirewall().firewall().aliases(),
    )


@app.route("/device/configuration/audit", methods=["POST", "GET"])
@login_required
def configuration_audit():
    if request.method == "POST":
        flash("Saved connection auditing configuration")
        current_app.sphirewall().general().configuration("audit:enabled", checkbox_value("enabled"))
        current_app.sphirewall().general().configuration("audit:directory", text_value("directory"))

        current_app.sphirewall().general().configuration("analyticslistener:enabled", checkbox_value("analyticslistener_enabled"))
        current_app.sphirewall().general().configuration("analyticslistener:url", text_value("analyticslistener_url"))
        current_app.sphirewall().general().configuration("analyticslistener:deviceid", text_value("analyticslistener_deviceid"))
        current_app.sphirewall().general().configuration("analyticslistener:token", text_value("analyticslistener_token"))

    enabled = current_app.sphirewall().general().configuration("audit:enabled")
    directory = current_app.sphirewall().general().configuration("audit:directory")

    analyticslistener_enabled = current_app.sphirewall().general().configuration("analyticslistener:enabled")
    analyticslistener_url = current_app.sphirewall().general().configuration("analyticslistener:url")
    analyticslistener_deviceid = current_app.sphirewall().general().configuration("analyticslistener:deviceid")
    analyticslistener_token = current_app.sphirewall().general().configuration("analyticslistener:token")

    return render_template(
        "device_configuration_audit.html",
        enabled=enabled, directory=directory,
        analyticslistener_enabled=analyticslistener_enabled,
        analyticslistener_url=analyticslistener_url,
        analyticslistener_deviceid=analyticslistener_deviceid,
        analyticslistener_token=analyticslistener_token
    )


@app.route("/device/configuration/authentication", methods=["GET", "POST"])
@login_required
def configuration_authentication():
    if request.method == "POST":
        current_app.sphirewall().firewall().captiveportal_set(
            int_value("mode"), int_value("endpoint"), text_value("url"),
            inclusions=text_value("inclusions").split("\n"), exclusions=text_value("exclusions").split("\n"),
            object_inclusions=request.form.getlist('object_inclusions'), object_exclusions=request.form.getlist('object_exclusions')
        )
        flash("Saved captive portal authentication configuration")

    return render_template(
        "device_configuration_authentication.html",
        captiveportal_opts=current_app.sphirewall().firewall().captiveportal(),
        pools=current_app.sphirewall().firewall().aliases(),
    )


@app.route("/device/configuration/authentication/providers", methods=["GET", "POST"])
@login_required
def configuration_authentication_providers():
    if request.method == "POST":
        #WMI Configuration:
        current_app.sphirewall().general().configuration("authentication:wmic:username", request.form["wmic_username"])
        current_app.sphirewall().general().configuration("authentication:wmic:password", request.form["wmic_password"])
        current_app.sphirewall().general().configuration("authentication:wmic:domain", request.form["wmic_domain"])
        current_app.sphirewall().general().configuration("authentication:wmic:hostname", request.form["wmic_host"])
        current_app.sphirewall().general().configuration("authentication:wmic:enabled", checkbox_value("wmic_enabled"))

        current_app.sphirewall().general().configuration("authentication:ldap:enabled", checkbox_value("ldap_enabled"))
        current_app.sphirewall().general().configuration("authentication:pam:enabled", checkbox_value("pam_enabled"))
        current_app.sphirewall().general().configuration("authentication:basic:enabled", checkbox_value("http_enabled"))
        current_app.sphirewall().general().configuration("authentication:basic:baseurl", request.form["http_baseurl"])

        ldap_settings = {
            "authentication:ldap:hostname": text_value("ldapserver"),
            "authentication:ldap:port": int(text_value("port", -1)),
            "authentication:ldap:basedn": text_value("ldapbasedn"),
            "authentication:ldap:ldapusername": text_value("ldapuser"),
            "authentication:ldap:ldappassword": text_value("ldappassword"),
            "authentication:ldap:type": text_value("ldaptype")
        }

        if text_value("ldaptype") == "ad":
            ldap_settings["authentication:ldap:userownedgroupmembership"] = True
            ldap_settings["authentication:ldap:usernameattribute"] = "samAccountName"
            ldap_settings["authentication:ldap:userbase"] = "CN=Users"
            ldap_settings["authentication:ldap:usergroupattribute"] = "memberOf"
            ldap_settings["authentication:ldap:authenticateuserusingsupplieddn"] = True
            ldap_settings["authentication:ldap:userdnattribute"] = "distinguishedName"

        elif text_value("ldaptype") == "ed":
            ldap_settings["authentication:ldap:userownedgroupmembership"] = True
            ldap_settings["authentication:ldap:usernameattribute"] = "CN"
            ldap_settings["authentication:ldap:userbase"] = ""
            ldap_settings["authentication:ldap:usergroupattribute"] = "groupMembership"
            ldap_settings["authentication:ldap:authenticateuserusingsupplieddn"] = True
            ldap_settings["authentication:ldap:userdnattribute"] = "entryDN"

        elif text_value("ldaptype") == "ol":
            ldap_settings["authentication:ldap:userownedgroupmembership"] = False
            ldap_settings["authentication:ldap:usernameattribute"] = "uid"
            ldap_settings["authentication:ldap:userbase"] = "ou=users"
            ldap_settings["authentication:ldap:usergroupattribute"] = ""
            ldap_settings["authentication:ldap:authenticateuserusingsupplieddn"] = False
            ldap_settings["authentication:ldap:userdnattribute"] = ""
            ldap_settings["authentication:ldap:groupbase"] = "ou=groups"
            ldap_settings["authentication:ldap:groupnameattribute"] = "cn"
            ldap_settings["authentication:ldap:groupmembernameattribute"] = "memberUid"
        else:
            ldap_settings["authentication:ldap:userownedgroupmembership"] = checkbox_value("userownedgroupmembership")
            ldap_settings["authentication:ldap:usernameattribute"] = text_value("usernameattribute")
            ldap_settings["authentication:ldap:userbase"] = text_value("userbase")
            ldap_settings["authentication:ldap:usergroupattribute"] = text_value("usergroupattribute")
            ldap_settings["authentication:ldap:userdnattribute"] = text_value("userdnattribute")
            ldap_settings["authentication:ldap:authenticateuserusingsupplieddn"] = checkbox_value("authenticateuserusingsupplieddn")
            ldap_settings["authentication:ldap:groupbase"] = text_value("groupbase")
            ldap_settings["authentication:ldap:groupnameattribute"] = text_value("groupnameattribute")
            ldap_settings["authentication:ldap:groupmembernameattribute"] = text_value("groupmembernameattribute")

        bulk_insert = []
        for key, value in ldap_settings.iteritems():
            bulk_insert.append({"key": key, "value": value})

        current_app.sphirewall().general().configuration_bulk(bulk_insert)
        flash("Saved authentication provider configuration")

    pam_enabled = current_app.sphirewall().general().configuration("authentication:pam:enabled")
    http_enabled = current_app.sphirewall().general().configuration("authentication:basic:enabled")
    http_baseurl = current_app.sphirewall().general().configuration("authentication:basic:baseurl")

    #View WMI Information
    ldap = dict(
        server=current_app.sphirewall().general().configuration("authentication:ldap:hostname"),
        port=current_app.sphirewall().general().configuration("authentication:ldap:port"),
        baseDn=current_app.sphirewall().general().configuration("authentication:ldap:basedn"),
        bindUsername=current_app.sphirewall().general().configuration("authentication:ldap:ldapusername"),
        bindPassword=current_app.sphirewall().general().configuration("authentication:ldap:ldappassword"),
        enabled=current_app.sphirewall().general().configuration("authentication:ldap:enabled"),
        ldaptype=current_app.sphirewall().general().configuration("authentication:ldap:type"),
        userownedgroupmembership=current_app.sphirewall().general().configuration("authentication:ldap:userownedgroupmembership"),
        usernameattribute=current_app.sphirewall().general().configuration("authentication:ldap:usernameattribute"),
        userbase=current_app.sphirewall().general().configuration("authentication:ldap:userbase"),
        usergroupattribute=current_app.sphirewall().general().configuration("authentication:ldap:usergroupattribute"),
        userdnattribute=current_app.sphirewall().general().configuration("authentication:ldap:userdnattribute"),
        authenticateuserusingsupplieddn=current_app.sphirewall().general().configuration("authentication:ldap:authenticateuserusingsupplieddn"),
        groupbase=current_app.sphirewall().general().configuration("authentication:ldap:groupbase"),
        groupnameattribute=current_app.sphirewall().general().configuration("authentication:ldap:groupnameattribute"),
        groupmembernameattribute=current_app.sphirewall().general().configuration("authentication:ldap:groupmembernameattribute"),
        status=current_app.sphirewall().general().ldap()
    )

    wmic = dict(
        wmic_username = current_app.sphirewall().general().configuration("authentication:wmic:username"),
        wmic_password = current_app.sphirewall().general().configuration("authentication:wmic:password"),
        wmic_domain = current_app.sphirewall().general().configuration("authentication:wmic:domain"),
        wmic_host = current_app.sphirewall().general().configuration("authentication:wmic:hostname"),
        wmic_enabled = current_app.sphirewall().general().configuration("authentication:wmic:enabled"),
        wmic_status = current_app.sphirewall().general().wmi_status()
    )

    return render_template(
        "device_configuration_authentication_providers.html",
        pam_enabled=pam_enabled,
        pools=current_app.sphirewall().firewall().aliases(),
        http_enabled=http_enabled,
        http_baseurl=http_baseurl,
        ldap=ldap,
        wmic=wmic
    )


@app.route("/device/configuration/authentication/wmi/resetcursor")
@login_required
def configuration_authentication_wmi_resetcursor():
    current_app.sphirewall().general().wmi_reset_cursor()
    return redirect("/device/configuration/authentication/wmi")


@app.route("/device/configuration/ldap/sync")
@login_required
def configuration_ldap_sync():
    sync_result = current_app.sphirewall().general().ldap_sync_groups()
    if "err" in sync_result:
        flash("Ldap: Could not synchronize with your ldap service, '%s', check the provided details" % sync_result.get("err"))
        return redirect("/device/configuration/general/ldap")
    flash("Connected and synchronized '%s' ldap groups" % sync_result.get("count"))
    return redirect("/device/configuration/general/ldap")


@app.route("/device/configuration/events", methods=["POST", "GET"])
@login_required
def configuration_events():
    webfilter_hit_condition = {"type": "event", "event": "event.firewall.webfilter.hit"}

    if request.method == "POST":
        if checkbox_value("log"):
            current_app.sphirewall().general().event_handler_add("event", "handler.log")
        else:
            current_app.sphirewall().general().event_handler_delete("event", "handler.log")

        current_app.sphirewall().general().configuration("general:remote_syslog_enabled", checkbox_value("remote_syslog_enabled"))
        current_app.sphirewall().general().configuration("general:remote_syslog_hostname", text_value("remote_syslog_hostname"))
        current_app.sphirewall().general().configuration("general:remote_syslog_port", nint(text_value("remote_syslog_port")))
        current_app.sphirewall().general().configuration("watchdog:monitorGoogle", checkbox_value("monitorGoogle"))

        recipients = []
        if text_value("additional_alert_recipients"):
            recipients = text_value("additional_alert_recipients").split(',')

        permission_recipients = []
        if checkbox_value("all_device_owners"):
            permission_recipients.append("owner")
        if checkbox_value("all_device_admins"):
            permission_recipients.append("admin")

        conditions = []
        if checkbox_value("webfilter_hit"):
            conditions.append(webfilter_hit_condition)

        current_app.service().update_alert_configuration(conditions, recipients, permission_recipients)
        flash("Saved alerting and remote logging settings")

    logging = current_app.sphirewall().general().event_handler_exists("event", "handler.log")
    remote_syslog_enabled = current_app.sphirewall().general().configuration("general:remote_syslog_enabled")
    remote_syslog_hostname = current_app.sphirewall().general().configuration("general:remote_syslog_hostname")
    remote_syslog_port = current_app.sphirewall().general().configuration("general:remote_syslog_port")
    monitorGoogle = current_app.sphirewall().general().configuration("watchdog:monitorGoogle")

    alert_config = None
    for existing_alert in current_app.service().alert_configuration():
        if existing_alert.get('name') == 'edgewize_email_alert':
            alert_config = existing_alert
            break

    alert = {}
    if alert_config:
        action = alert_config['actions'][0]
        alert['all_device_owners'] = action.get('permission_recipients') and 'owner' in action['permission_recipients']
        alert['all_device_admins'] = action.get('permission_recipients') and 'admin' in action['permission_recipients']
        alert['additional_alert_recipients'] = ', '.join(action.get('recipients', []))
        alert['webfilter_hit'] = webfilter_hit_condition in alert_config['conditions']

    return render_template(
        "device_configuration_events.html",
        logging=logging,
        remote_syslog_enabled=remote_syslog_enabled,
        remote_syslog_hostname=remote_syslog_hostname,
        remote_syslog_port=remote_syslog_port,
        alert=alert,
        monitorGoogle=monitorGoogle
    )


@app.route("/device/configuration/users", methods=["GET", "POST"])
@login_required
def configuration_users():
    if request.method == "POST":
        flash("Added new user called '%s'" % request.form.get("username"))
        current_app.sphirewall().general().users_add(request.form.get("username"), "", "")

    unsorted_users = current_app.sphirewall().general().users()
    users = sorted(unsorted_users, key=operator.itemgetter("username"))
    return render_template(
        "device_configuration_users.html", users=users, groups=current_app.sphirewall().general().groups()
    )


@app.route("/device/configuration/users/delete/<username>")
@login_required
def configuration_users_delete(username):
    flash("Deleted user called '%s'" % username)
    current_app.sphirewall().general().users_delete(username)
    return redirect("/device/configuration/users")


@app.route("/device/configuration/users/<username>", methods=["GET", "POST"])
@login_required
def configuration_users_edit(username):
    if request.method == "POST":
        quota = QuotaInfo()
        quota.dailyQuota = checkbox_value("dailyQuota")
        quota.dailyQuotaLimit = nint(text_value("dailyQuotaLimit"))

        quota.weeklyQuota = checkbox_value("weeklyQuota")
        quota.weeklyQuotaLimit = nint(text_value("weeklyQuotaLimit"))

        quota.monthQuota = checkbox_value("monthQuota")
        quota.monthQuotaLimit = nint(text_value("monthQuotaLimit"))

        quota.timeQuota = checkbox_value("timeQuota")
        quota.timeQuotaLimit = nint(text_value("timeQuotaLimit"))

        quota.totalQuota = checkbox_value("totalQuota")
        quota.totalQuotaLimit = nint(text_value("totalQuotaLimit"))

        # configure groups:
        group_list = request.form.getlist('groups')
        groups = []
        for id in group_list:
            groups.append(id)

        current_app.sphirewall().general().users_groups_merge(username, groups)
        current_app.sphirewall().general().users_save(
            username, request.form["fname"], request.form["lname"], request.form["email"], quota
        )

        if len(request.form["password"]) > 0 and request.form["password"] == request.form["repassword"]:
            current_app.sphirewall().general().user_set_password(username, request.form["password"])
        flash("Saved user configuration")
        return redirect("/device/configuration/users")

    return render_template(
        "device_configuration_users_edit.html",user=current_app.sphirewall().general().users(username), groups=current_app.sphirewall().general().groups()
    )


@app.route("/device/configuration/users/<username>/disable")
@login_required
def configuration_users_disable(username):
    current_app.sphirewall().general().user_disable(username)
    return redirect("/device/configuration/users/" + username)


@app.route("/device/configuration/users/<username>/enable")
@login_required
def configuration_users_enable(username):
    current_app.sphirewall().general().user_enable(username)
    return redirect("/device/configuration/users/" + username)


@login_required
@app.route("/device/configuration/users/<username>/groups/remove/<groupid>", methods=["GET", "POST"])
def configuration_users_remove_group(username, groupid):
    current_app.sphirewall().general().user_groups_remove(username, groupid)
    return redirect("/device/configuration/users/" + username)


@app.route("/device/configuration/users/<username>/groups/add", methods=["POST"])
@login_required
def configuration_users_add_group(username):
    current_app.sphirewall().general().user_groups_add(username, request.form["group"])
    return redirect("/device/configuration/users/" + username)


@app.route("/device/configuration/groups", methods=["GET", "POST"])
@login_required
def configuration_groups():
    if request.method == "POST":
        flash("Created a new group called '%s'" % request.form.get("name"))
        current_app.sphirewall().general().group_add(request.form.get("name"))

    unsorted_groups = current_app.sphirewall().general().groups()
    groups = sorted(unsorted_groups, key=operator.itemgetter("name"))
    return render_template("device_configuration_groups.html", groups=groups)


@app.route("/device/configuration/groups/delete/<id>")
@login_required
def configuration_groups_delete(id):
    flash("Deleted a group")
    current_app.sphirewall().general().groups_delete(id)
    return redirect("/device/configuration/groups")


@app.route("/device/configuration/groups/<id>", methods=["GET", "POST"])
@login_required
def configuration_groups_edit(id):
    if request.method == "POST":
        quota = QuotaInfo()

        quota.dailyQuota = checkbox_value("dailyQuota")
        quota.dailyQuotaLimit = nint(text_value("dailyQuotaLimit"))

        quota.weeklyQuota = checkbox_value("weeklyQuota")
        quota.weeklyQuotaLimit = nint(text_value("weeklyQuotaLimit"))

        quota.monthQuota = checkbox_value("monthQuota")
        quota.monthQuotaLimit = nint(text_value("monthQuotaLimit"))

        quota.timeQuota = checkbox_value("timeQuota")
        quota.timeQuotaLimit = nint(text_value("timeQuotaLimit"))

        quota.totalQuota = checkbox_value("totalQuota")
        quota.totalQuotaLimit = nint(text_value("totalQuotaLimit"))

        current_app.sphirewall().general().groups_save(
            id, request.form["manager"], checkbox_value("mui"), request.form["desc"], quota
        )
        flash("Saved group details and configuration")
        return redirect("/device/configuration/groups")

    return render_template("device_configuration_groups_edit.html", group=current_app.sphirewall().general().groups(id))


@app.route("/device/configuration/advanced", methods=["GET", "POST"])
@login_required
def configuration_advanced():
    if request.method == "POST":
        for fieldname, value in request.form.items():
            current_app.sphirewall().general().advanced(fieldname, nint(value))
        flash("Saved advanced configuration variables")
    return render_template("device_configuration_advanced.html", vars=current_app.sphirewall().general().advanced())


@app.route("/device/configuration/quotas", methods=["GET", "POST"])
@login_required
def configuration_quotas():
    if request.method == "POST":
        dailyQuotaLimit = nint(text_value("dailyQuotaLimit"))
        weeklyQuotaLimit = nint(text_value("weeklyQuotaLimit"))
        monthQuotaBillingDay = nint(text_value("monthQuotaBillingDay"))
        monthQuotaLimit = nint(text_value("monthQuotaLimit"))

        current_app.sphirewall().general().quotas_set(
            checkbox_value("dailyQuota"),
            dailyQuotaLimit, checkbox_value("weeklyQuota"),
            weeklyQuotaLimit, checkbox_value("monthQuota"),
            monthQuotaBillingDay,
            checkbox_value("monthQuotaIsSmart"), monthQuotaLimit
        )

    return render_template("device_configuration_quotas.html", settings=current_app.sphirewall().general().quotas())


@app.route("/device/configuration/logging", methods=["GET", "POST"])
@login_required
def configuration_logging():
    if request.method == "POST":
        current_app.sphirewall().general().logging(text_value("context"), nint(text_value("level")))

    logging = current_app.sphirewall().general().logging()
    return render_template(
        "device_configuration_logging.html", logging=logging,
        critical=current_app.sphirewall().general().logging_critical()
    )


@app.route("/device/configuration/logging/<context>/delete", methods=["GET", "POST"])
@login_required
def configuration_logging_delete(context):
    current_app.sphirewall().general().logging_delete(context)
    return redirect("/device/configuration/logging")


@app.route("/device/configuration/logging/critical", methods=["POST"])
@login_required
def configuration_logging_critical():
    current_app.sphirewall().general().logging_critical(checkbox_value("critical"))
    return redirect("/device/configuration/logging")


@app.route("/device/configuration/sessions/persist", methods=["POST"])
@login_required
def status_clients_persist():
    flash("Created new persisted user session for '%s' on device '%s'" % (text_value("username"), text_value("hw")))
    current_app.sphirewall().general().create_persisted_session(text_value("username"), text_value("hw"))
    return redirect("/device/configuration/sessions")


@app.route("/device/configuration/sessions/persist/<username>/<mac>/remove")
@login_required
def status_clients_persist_remove(username, mac):
    flash("Removed persisted user session for '%s' on device '%s'" % (username, mac))
    current_app.sphirewall().general().delete_persisted_session(username, mac)
    return redirect("/device/configuration/sessions")


@app.route("/device/configuration/sessions", methods=["GET", "POST"])
@login_required
def configuration_sessions():
    if request.method == "POST":
        flash("Updated session configuration")
        current_app.sphirewall().general().configuration("authentication:session_timeout", nint(request.form["session_timeout"]))

    persisted_sessions = current_app.sphirewall().general().list_persisted_session()
    timeouts = current_app.sphirewall().general().session_network_based_timeouts()
    default_timeout = current_app.sphirewall().general().configuration("authentication:session_timeout")
    return render_template(
        "device_configuration_persisted_sessions.html",
        available_users=current_app.sphirewall().general().users(),
        persisted_sessions=persisted_sessions, timeouts=timeouts,
        default_timeout=default_timeout
    )


@app.route("/device/configuration/authentication/timeouts/manage", methods=["POST", "GET"])
@app.route("/device/configuration/authentication/timeouts/<id>/manage", methods=["POST", "GET"])
@login_required
def configuration_authentication_timeouts_manage(id=None):
    if request.method == "POST":
        if not id:
            flash("Added new timeout configuration")
        else:
            flash("Updated timeout configuration")

        current_app.sphirewall().general().session_network_based_timeouts_create_or_modify(
            id, int(text_value("timeout")), request.form.getlist("networks"), request.form.getlist("groups")
        )
        return redirect("/device/configuration/sessions")

    # Find the targetted timeout
    target_timeout = None
    for timeout in current_app.sphirewall().general().session_network_based_timeouts():
        if timeout["id"] == id:
            target_timeout = timeout
            break

    return render_template(
        "device_configuration_authentication_manage.html",
        timeout=target_timeout,
        networks=current_app.sphirewall().firewall().aliases(),
        groups=current_app.sphirewall().general().groups()
    )


@app.route("/device/configuration/authentication/timeouts/<id>/delete")
@login_required
def configuration_authentication_timeouts_delete(id):
    flash("Deleted timeout configuration")
    current_app.sphirewall().general().session_network_based_timeouts_remove(id)
    return redirect("/device/configuration/sessions")
