import datetime
import json
from flask import flash, jsonify, current_app
from flask.ext.login import login_required

from flask.globals import request
from flask.templating import render_template
import re
from werkzeug.utils import redirect

from sphirewallwmi import app
from sphirewallwmi.api.utils import checkbox_value, int_value, arg
from sphirewallwmi.middleware import service
from sphirewallwmi.routes_device_network import text_value


@login_required
@app.route("/device/firewall/aliases", methods=["POST", "GET"])
def firewall_aliases():
    if request.method == "POST":
        current_app.sphirewall().firewall().aliases_create(
            text_value("name"),
            text_value("type"),
            text_value("source"),
            text_value("detail")
        )
        flash("Created new aliases called '%s'" % text_value("name"))

    aliases = current_app.sphirewall().firewall().aliases()
    periods = current_app.sphirewall().firewall().periods()
    signatures = current_app.sphirewall().firewall().signatures()

    return render_template("device_firewall_aliases.html", aliases=aliases, periods=periods, signatures=signatures)


@login_required
@app.route("/device/firewall/aliases/<id>/delete")
def firewall_aliases_delete(id):
    current_app.sphirewall().firewall().aliases_delete(id)
    flash("Alias was deleted")
    return redirect("/device/firewall/aliases")


@login_required
@app.route("/device/firewall/aliases/<id>/sync")
def firewall_aliases_sync(id):
    current_app.sphirewall().firewall().aliases_sync(id)
    flash("Remote alias was synced")
    return redirect("/device/firewall/aliases")


@login_required
@app.route("/device/firewall/aliases/<id>", methods=["POST", "GET"])
def firewall_aliases_get(id):
    if request.method == "POST":
        item_count = 0
        values = text_value("value")
        value_list = values.strip().replace(" ", ",").replace("\n", ",").replace("\r", ",").split(",")
        for value in value_list:
            trimmed_value = value.strip()
            if len(trimmed_value) > 0:
                current_app.sphirewall().firewall().aliases_list_add(id, trimmed_value)
                item_count += 1
        flash("Added %d items to alias" % item_count)
    return render_template(
        "device_firewall_aliases_edit.html",
        alias=current_app.sphirewall().firewall().aliases(id),
        items=current_app.sphirewall().firewall().aliases_list(id)
    )


@login_required
@app.route("/device/firewall/aliases/<id>/deletevalue")
def firewall_aliases_del(id):
    current_app.sphirewall().firewall().aliases_list_del(id, request.args["value"])
    flash("Removed value '%s' from alias" % request.args["value"])
    return redirect("/device/firewall/aliases/" + id)


def find_criteria_with_key(key, criterias):
    if not criterias:
        return None

    for criteria in criterias:
        if key == criteria["type"]:
            return criteria
    return None


protocol_number_mappings = {
    1: "ICMP",
    2: "IGMP",
    3: "GGP",
    6: "TCP",
    8: "EGP",
    9: "IGP",
    17: "UDP",
    47: "GRE",
    50: "ESP",
    51: "AH",
    58: "ICMPv6",
    88: "EIGRP",
    89: "OSPF"
}


def source_string(rule):
    ret = ""
    for criteria in rule["criteria"]:
        if criteria["type"] == "source.ipv4":
            if criteria.get("negate", False):
                ret += ' source ip not "%s/%s"' % (criteria["conditions"]["ip"], criteria["conditions"]["mask"])
            else:
                ret += ' source ip "%s/%s"' % (criteria["conditions"]["ip"], criteria["conditions"]["mask"])
        elif criteria["type"] == "source.ipv6":
            ret += ' source ip "%s/%s"' % (criteria["conditions"]["ip"], criteria["conditions"]["cidr"])
        elif criteria["type"] == "source.transport.port":
            ret += ' source port "%s"' % (criteria["conditions"])
        elif criteria["type"] == "source.transport.portrange":
            ret += ' source port "%s-%s"' % (criteria["conditions"]["startPort"], criteria["conditions"]["endPort"])
        elif criteria["type"] == "source.device":
            ret += ' source interface "%s"' % criteria["conditions"]["name"]
        elif criteria["type"] == "group":
            ret += ' multiple groups'
        elif criteria["type"] == "source.user":
            ret += ' multiple users'
        elif criteria["type"] == "protocol":
            ret += " %s" % protocol_number_mappings[criteria["conditions"]["type"]]
        elif criteria["type"] == "timeperiod":
            ret += " multiple periods"
        elif criteria["type"] == "source.quarantined":
            ret += " source quarantined"
        elif criteria["type"] == "session.active":
            ret += " authenticated users"
        elif criteria["type"] == "session.inactive":
            ret += " unauthenticated users"
    return ret


def destination_string(rule):
    ret = ""
    for criteria in rule["criteria"]:
        if criteria["type"] == "destination.ipv4":
            if criteria.get("negate", False):
                ret += ' destination ip not "%s/%s"' % (criteria["conditions"]["ip"], criteria["conditions"]["mask"])
            else:
                ret += ' destination ip "%s/%s"' % (criteria["conditions"]["ip"], criteria["conditions"]["mask"])
        elif criteria["type"] == "destination.ipv6":
            ret += ' destination ip "%s/%s"' % (criteria["conditions"]["ip"], criteria["conditions"]["cidr"])
        elif criteria["type"] == "destination.transport.port":
            ret += ' destination port "%s"' % (criteria["conditions"])
        elif criteria["type"] == "destination.transport.portrange":
            ret += ' destination port "%s-%s"' % (criteria["conditions"]["startPort"], criteria["conditions"]["endPort"])
        elif criteria["type"] == "application.http.hostname":
            ret += " filter certain websites"
        elif criteria["type"] == "destination.device":
            ret += ' destination interface "%s"' % criteria["conditions"]["name"]
    return ret


def resolve_rule(rule):
    resolved_rule = rule
    if find_criteria_with_key("source.ipv4", rule.get("criteria")):
        match = find_criteria_with_key("source.ipv4", rule["criteria"])
        resolved_rule["sourceIp"] = match["conditions"]["ip"]
        resolved_rule["sourceMask"] = match["conditions"]["mask"]
        resolved_rule["sourceNetworkCriteriaEnabled"] = True
        resolved_rule["sourceIpNegated"] = match.get("negate", False)

    if find_criteria_with_key("group", rule.get("criteria")):
        match = find_criteria_with_key("group", rule["criteria"])
        resolved_rule["groups"] = match["conditions"]
        resolved_rule["groupCriteriaEnabled"] = True

    if find_criteria_with_key("timeperiod", rule.get("criteria")):
        match = find_criteria_with_key("timeperiod", rule["criteria"])
        resolved_rule["timePeriods"] = match["conditions"]
        resolved_rule["timePeriodsCriteriaEnabled"] = True

    if find_criteria_with_key("source.ipv4.alias", rule.get("criteria")):
        match = find_criteria_with_key("source.ipv4.alias", rule["criteria"])
        resolved_rule["sourceAlias"] = match["conditions"]
        resolved_rule["sourceAliasCriteriaEnabled"] = True

    if find_criteria_with_key("source.mac.pool", rule.get("criteria")):
        match = find_criteria_with_key("source.mac.pool", rule["criteria"])
        resolved_rule["sourceMacAddressPool"] = match["conditions"]
        resolved_rule["sourceMacAddressPoolCriteriaEnabled"] = True

    if find_criteria_with_key("source.user", rule.get("criteria")):
        match = find_criteria_with_key("source.user", rule["criteria"])
        resolved_rule["users"] = match["conditions"]
        resolved_rule["userCriteriaEnabled"] = True

    if find_criteria_with_key("source.mac", rule.get("criteria")):
        match = find_criteria_with_key("source.mac", rule["criteria"])
        resolved_rule["macAddresses"] = match["conditions"]
        resolved_rule["macCriteriaEnabled"] = True

    if find_criteria_with_key("destination.ipv4", rule.get("criteria")):
        match = find_criteria_with_key("destination.ipv4", rule.get("criteria"))
        resolved_rule["destIp"] = match["conditions"]["ip"]
        resolved_rule["destMask"] = match["conditions"]["mask"]
        resolved_rule["destNetworkCriteriaEnabled"] = True
        resolved_rule["destIpNegated"] = match.get("negate", False)

    if find_criteria_with_key("destination.ipv6", rule.get("criteria")):
        resolved_rule["destV6"] = find_criteria_with_key("destination.ipv6", rule.get("criteria"))["conditions"]["ip"]
        resolved_rule["destV6Cidr"] = find_criteria_with_key("destination.ipv6", rule.get("criteria"))["conditions"]["cidr"]
        resolved_rule["destV6CriteriaEnabled"] = True

    if find_criteria_with_key("source.ipv6", rule.get("criteria")):
        resolved_rule["sourceV6"] = find_criteria_with_key("source.ipv6", rule.get("criteria"))["conditions"]["ip"]
        resolved_rule["sourceV6Cidr"] = find_criteria_with_key("source.ipv6", rule.get("criteria"))["conditions"]["cidr"]
        resolved_rule["sourceV6CriteriaEnabled"] = True

    if find_criteria_with_key("destination.ipv4.alias", rule.get("criteria")):
        resolved_rule["destAlias"] = find_criteria_with_key("destination.ipv4.alias", rule.get("criteria"))["conditions"]
        resolved_rule["destAliasCriteriaEnabled"] = True

    if find_criteria_with_key("source.transport.port", rule.get("criteria")):
        resolved_rule["sport"] = find_criteria_with_key("source.transport.port", rule.get("criteria"))["conditions"]
        resolved_rule["sourcePortCriteriaEnabled"] = True

    if find_criteria_with_key("destination.transport.port", rule.get("criteria")):
        resolved_rule["dport"] = find_criteria_with_key("destination.transport.port", rule.get("criteria"))["conditions"]
        resolved_rule["destPortCriteriaEnabled"] = True

    if find_criteria_with_key("source.transport.portrange", rule.get("criteria")):
        resolved_rule["sport_start"] = find_criteria_with_key("source.transport.portrange", rule.get("criteria"))["conditions"]["startPort"]
        resolved_rule["sport_end"] = find_criteria_with_key("source.transport.portrange", rule.get("criteria"))["conditions"]["endPort"]
        resolved_rule["sourcePortCriteriaEnabled"] = True

    if find_criteria_with_key("destination.transport.portrange", rule.get("criteria")):
        resolved_rule["dport_start"] = find_criteria_with_key("destination.transport.portrange", rule.get("criteria"))["conditions"]["startPort"]
        resolved_rule["dport_end"] = find_criteria_with_key("destination.transport.portrange", rule.get("criteria"))["conditions"]["endPort"]
        resolved_rule["destPortCriteriaEnabled"] = True

    if find_criteria_with_key("source.device", rule.get("criteria")):
        resolved_rule["sourceInterface"] = find_criteria_with_key("source.device", rule.get("criteria"))["conditions"]["name"]
        resolved_rule["sourceInterfaceCriteriaEnabled"] = True

    if find_criteria_with_key("destination.device", rule.get("criteria")):
        resolved_rule["destInterface"] = find_criteria_with_key("destination.device", rule.get("criteria"))["conditions"]["name"]
        resolved_rule["destInterfaceCriteriaEnabled"] = True

    if find_criteria_with_key("protocol", rule.get("criteria")):
        resolved_rule["protocol"] = find_criteria_with_key("protocol", rule.get("criteria"))["conditions"]["type"]
        resolved_rule["protocolCriteriaEnabled"] = True

    if find_criteria_with_key("global.quota", rule.get("criteria")):
        resolved_rule["globalQuotaExceededEnabled"] = True

    if find_criteria_with_key("source.quarantined", rule.get("criteria")):
        resolved_rule["sourceQuarantinedEnabled"] = True

    if find_criteria_with_key("user.quota", rule.get("criteria")):
        resolved_rule["userQuotaExceededEnabled"] = True

    if find_criteria_with_key("session.active", rule.get("criteria")):
        resolved_rule["authenticatedUsersEnabled"] = True

    if find_criteria_with_key("session.inactive", rule.get("criteria")):
        resolved_rule["unAuthenticatedUsersEnabled"] = True

    if find_criteria_with_key("geoip.source", rule.get("criteria")):
        resolved_rule["sourceCountry"] = find_criteria_with_key("geoip.source", rule.get("criteria"))["conditions"]["country"]
        resolved_rule["sourceCountryCriteriaEnabled"] = True

    if find_criteria_with_key("geoip.destination", rule.get("criteria")):
        resolved_rule["destinationCountry"] = find_criteria_with_key("geoip.destination", rule.get("criteria"))["conditions"]["country"]
        resolved_rule["destinationCountryCriteriaEnabled"] = True

    if find_criteria_with_key("application.http", rule.get("criteria")):
        resolved_rule["allHttp"] = True

    return resolved_rule


def generate_criteria():
    criteria = []
    if checkbox_value("sourceNetworkCriteriaEnabled") and text_value("sourceIp") and text_value("sourceMask"):
        criteria.append({"type": "source.ipv4", "negate": text_value("sourceIpNegated") == "not", "conditions": {"ip": text_value("sourceIp"), "mask": text_value("sourceMask")}})

    if checkbox_value("groupCriteriaEnabled") and len(request.form.getlist('group')) > 0:
        criteria.append({"type": "group", "conditions": [int(gid) for gid in request.form.getlist('group')]})

    if checkbox_value("sourceAliasCriteriaEnabled") and len(request.form.getlist('sourceAlias')) > 0:
        criteria.append({"type": "source.ipv4.alias", "conditions": request.form.getlist('sourceAlias')})

    if checkbox_value("sourceMacAddressPoolCriteriaEnabled") and len(request.form.getlist('sourceMacAlias')) > 0:
        criteria.append({"type": "source.mac.pool", "conditions": request.form.getlist('sourceMacAlias')})

    if checkbox_value("timePeriodsCriteriaEnabled") and len(request.form.getlist('timePeriods')) > 0:
        criteria.append({"type": "timeperiod", "conditions": request.form.getlist('timePeriods')})
    if checkbox_value("userCriteriaEnabled") and len(request.form.getlist('users')) > 0:
        criteria.append({"type": "source.user", "conditions": [user for user in request.form.getlist('users')]})
    if checkbox_value("macCriteriaEnabled") and text_value('macAddress'):
        criteria.append({"type": "source.mac", "conditions": text_value('macAddress').split(",")})

    # New items
    if checkbox_value("sourceV6CriteriaEnabled") and text_value("sourceV6") and text_value("sourceV6Cidr"):
        criteria.append({"type": "source.ipv6", "conditions": {"ip": text_value("sourceV6"), "cidr": int_value("sourceV6Cidr")}})
    if checkbox_value("destAliasCriteriaEnabled") and len(request.form.getlist("destAlias")):
        criteria.append({"type": "destination.ipv4.alias", "conditions": request.form.getlist("destAlias")})

    if checkbox_value("sourcePortCriteriaEnabled") and int_value("sport_start", default=None) and int_value("sport_end", default=None):
        criteria.append({"type": "source.transport.portrange", "conditions": {"startPort": int_value("sport_start"), "endPort": int_value("sport_end")}})
    if checkbox_value("destPortCriteriaEnabled") and int_value("dport_start", default=None) and int_value("dport_end", default=None):
        criteria.append({"type": "destination.transport.portrange", "conditions": {"startPort": int_value("dport_start"), "endPort": int_value("dport_end")}})
    if checkbox_value("sourcePortCriteriaEnabled") and text_value("sport"):
        criteria.append({"type": "source.transport.port", "conditions": [int(port) for port in text_value("sport").split(",") if len(port) > 0]})
    if checkbox_value("destPortCriteriaEnabled") and text_value("dport"):
        criteria.append({"type": "destination.transport.port", "conditions": [int(port) for port in text_value("dport").split(",") if len(port) > 0]})

    if checkbox_value("sourceInterfaceCriteriaEnabled") and text_value("sourceInterface"):
        criteria.append({"type": "source.device", "conditions": {"name": text_value("sourceInterface")}})
    if checkbox_value("destNetworkCriteriaEnabled") and text_value("destIp") and text_value("destMask"):
        criteria.append({"type": "destination.ipv4", "negate": text_value("destIpNegated") == "not", "conditions": {"ip": text_value("destIp"), "mask": text_value("destMask")}})
    if checkbox_value("destV6CriteriaEnabled") and text_value("destV6") and int_value("destV6Cidr"):
        criteria.append({"type": "destination.ipv6", "conditions": {"ip": text_value("destV6"), "cidr": int_value("destV6Cidr")}})
    if checkbox_value("destInterfaceCriteriaEnabled") and text_value("destInterface"):
        criteria.append({"type": "destination.device", "conditions": {"name": text_value("destInterface")}})

    if checkbox_value("protocolCriteriaEnabled") and text_value("protocol", default=None):
        criteria.append({"type": "protocol", "conditions": {"type": int_value("protocol")}})

    if checkbox_value("globalQuotaExceededEnabled"):
        criteria.append({"type": "global.quota", "conditions": {}})
    if checkbox_value("userQuotaExceededEnabled"):
        criteria.append({"type": "user.quota", "conditions": {}})
    if checkbox_value("sourceQuarantinedEnabled"):
        criteria.append({"type": "source.quarantined", "conditions": {}})

    if checkbox_value("authenticatedUsersEnabled"):
        criteria.append({"type": "session.active", "conditions": {}})
    if checkbox_value("unAuthenticatedUsersEnabled"):
        criteria.append({"type": "session.inactive", "conditions": {}})

    if checkbox_value("sourceCountryCriteriaEnabled") and text_value("sourceCountry"):
        criteria.append({"type": "geoip.source", "conditions": {"country": text_value("sourceCountry")}})
    if checkbox_value("destinationCountryCriteriaEnabled") and text_value("destinationCountry"):
        criteria.append({"type": "geoip.destination", "conditions": {"country": text_value("destinationCountry")}})

    if checkbox_value("allHttp"):
        criteria.append({"type": "application.http", "conditions": {}})

    return criteria


def resolve_webfiltering_rule(rule):
    resolved_rule = resolve_rule(rule)
    resolved_rule["source_string"] = source_string(rule)
    resolved_rule["destination_string"] = destination_string(rule)

    if find_criteria_with_key("application.http.hostname", rule.get("criteria")):
        match = find_criteria_with_key("application.http.hostname", rule["criteria"])
        resolved_rule["lists"] = match["conditions"]
        resolved_rule["listEnabled"] = True

    if find_criteria_with_key("signature", rule.get("criteria")):
        match = find_criteria_with_key("signature", rule["criteria"])
        resolved_rule["signature_enabled"] = True
        resolved_rule["signature"] = match["conditions"]

    if find_criteria_with_key("application.http", rule.get("criteria")):
        resolved_rule["allHttp"] = True

    resolved_rule["fireEvent"] = rule["fireEvent"]
    resolved_rule["action"] = rule["action"]
    resolved_rule["redirect"] = rule["redirect"]
    resolved_rule["redirectUrl"] = rule["redirectUrl"]
    resolved_rule["name"] = rule["name"]
    resolved_rule["id"] = rule["id"]
    resolved_rule["enabled"] = rule["enabled"]
    return resolved_rule


@login_required
@app.route("/device/firewall/webfilter/add", methods=["GET", "POST"])
@app.route("/device/firewall/webfilter/add/<id>", methods=["GET", "POST"])
def firewall_webfilter_add(id=None):
    if request.method == "POST":
        criteria = generate_criteria()

        if text_value("app_type") == "app_websites":
            criteria.append({"type": "application.http.hostname", "conditions": request.form.getlist('list')})
        elif text_value("app_type") == "app_signatures":
            criteria.append({"type": "signature", "conditions": request.form.getlist("signature")})
        elif text_value("app_type") == "app_all_websites":
            criteria.append({"type": "application.http", "conditions": []})

        current_app.sphirewall().firewall().webfilter_add(
            id, text_value("name"), criteria, text_value("action"), checkbox_value("fireEvent"), checkbox_value("redirect"), text_value("redirectUrl"), False
        )

        if not id:flash("A new application layer filtering rule was created")
        else: flash("Rule was modified")
        return redirect("/device/firewall/appfiltering")

    rule = current_app.sphirewall().firewall().webfilter_get(id)
    resolved_rule = resolve_webfiltering_rule(rule) if rule else {}

    return render_template(
        "device_firewall_webfilter_add.html",
        rule=resolved_rule,
        pools=current_app.sphirewall().firewall().aliases(),
        devices=current_app.sphirewall().network().devices(),
        groups=current_app.sphirewall().general().groups(),
        periods=current_app.sphirewall().firewall().periods(),
        users=current_app.sphirewall().general().users(),
        signatures=current_app.sphirewall().firewall().signatures()
    )


@login_required
@app.route("/device/firewall/webfilter/<id>/delete")
def firewall_webfilter_delete(id):
    flash("Application layer rule was removed")
    current_app.sphirewall().firewall().webfilter_delete(id)
    return redirect("/device/firewall/appfiltering")


@login_required
@app.route("/device/firewall/webfilter/<id>/moveup")
def firewall_webfilter_moveup(id):
    flash("Application layer rule was moved up")
    current_app.sphirewall().firewall().webfilter_moveup(id)
    return redirect("/device/firewall/appfiltering")


@login_required
@app.route("/device/firewall/webfilter/<id>/movedown")
def firewall_webfilter_movedown(id):
    flash("Application layer rule was moved down")
    current_app.sphirewall().firewall().webfilter_movedown(id)
    return redirect("/device/firewall/appfiltering")


@login_required
@app.route("/device/firewall/webfilter/disable/<id>", methods=["POST", "GET"])
def firewall_webfilter_disable(id):
    flash("Application layer rule was disabled")
    current_app.sphirewall().firewall().webfilter_disable(id)
    return redirect("/device/firewall/appfiltering")


@login_required
@app.route("/device/firewall/webfilter/enable/<id>", methods=["POST", "GET"])
def firewall_webfilter_enable(id):
    flash("Application layer rule was enabled")
    current_app.sphirewall().firewall().webfilter_enable(id)
    return redirect("/device/firewall/appfiltering")


@login_required
@app.route("/device/firewall/filtering")
def firewall_acls():
    acls = current_app.sphirewall().firewall().acls()
    for acl in acls:
        acl = resolve_rule(acl)
        acl["source_string"] = source_string(acl)
        acl["destination_string"] = destination_string(acl)

    return render_template(
        "device_firewall_filtering.html", filter=acls, pools=current_app.sphirewall().firewall().aliases(),
        groups=current_app.sphirewall().general().groups()
    )

@login_required
@app.route("/device/firewall/appfiltering")
def firewall_appacls():
    acls = current_app.sphirewall().firewall().acls()
    resolved_layer7_rules = []
    for rule in current_app.sphirewall().firewall().webfilter():
        resolved_layer7_rules.append(resolve_webfiltering_rule(rule))

    return render_template(
        "device_firewall_appfiltering.html", pools=current_app.sphirewall().firewall().aliases(), layer7_rules=resolved_layer7_rules,
        groups=current_app.sphirewall().general().groups()
    )


@login_required
@app.route("/device/ajax/signatures")
def ajax_signatures():
    return jsonify(
        signatures=current_app.sphirewall().firewall().signatures()
    )


@login_required
@app.route("/device/firewall/signatures/sync")
def firewall_signatures_import():
    if current_app.sphirewall().firewall().signatures_import():
        flash("Imported custom signatures")
    else:
        flash("Could not import signatures, check link and content format")
    return redirect("/device/firewall/aliases")


@login_required
@app.route("/device/firewall/qos")
def firewall_qos():
    acls = current_app.sphirewall().firewall().qos()
    for acl in acls:
        acl = resolve_rule(acl)
        acl["source_string"] = source_string(acl)
        acl["destination_string"] = destination_string(acl)

    return render_template(
        "device_firewall_limiterqos.html",
        qos=acls, groups=current_app.sphirewall().general().groups(),
        enabled=current_app.sphirewall().general().advanced_value("QOS_ENABLED"),
        priority=current_app.sphirewall().firewall().priority()
    )


@login_required
@app.route("/device/firewall/forwarding/edit", methods=["GET", "POST"])
@app.route("/device/firewall/forwarding/edit/<id>", methods=["GET", "POST"])
def firewall_nat_edit(id=None):
    if request.method == "POST":
        criteria = generate_criteria()

        current_app.sphirewall().firewall().forwarding_rules_add(
            criteria=criteria,
            forwardingDestination=text_value("forwardingDestination"),
            forwardingDestinationPort=int_value("forwardingDestinationPort"),
            id=id
        )
        if not id:
            flash("Created a new Port Forwarding Nat rule")
        else:
            flash("Modified port forwarding rule")
        return redirect("/device/firewall/masquerading")

    resolved_rule = current_app.sphirewall().firewall().forwarding_rules(id)
    if id: resolved_rule = resolve_rule(resolved_rule)
    return render_template("device_firewall_forwarding_edit.html",
                           rule=resolved_rule,
                           devices=current_app.sphirewall().network().devices(),
                           pools=current_app.sphirewall().firewall().aliases(),
                           groups=current_app.sphirewall().general().groups(),
                           periods=current_app.sphirewall().firewall().periods(),
                           users=current_app.sphirewall().general().users())


@login_required
@app.route("/device/firewall/forwarding/<id>/delete", methods=["GET"])
def firewall_nat_delete(id):
    flash("Deleted Port Forwarding Nat rule")
    current_app.sphirewall().firewall().forwarding_rules_delete(id)
    return redirect("/device/firewall/masquerading")


@login_required
@app.route("/device/firewall/forwarding/<id>/enable", methods=["POST", "GET"])
def firewall_nat_enable(id):
    flash("Enabled Port Forwarding rule")
    current_app.sphirewall().firewall().forwarding_rules_enable(id)
    return redirect("/device/firewall/masquerading")


@login_required
@app.route("/device/firewall/forwarding/<id>/disable", methods=["GET", "POST"])
def firewall_nat_disable(id):
    flash("Disabled Port Forwarding rule")
    current_app.sphirewall().firewall().forwarding_rules_disable(id)
    return redirect("/device/firewall/masquerading")


@login_required
@app.route("/device/firewall/masquerading", methods=["POST", "GET"])
def firewall_masquerading():
    if request.method == "POST":
        flash("Saved WAN configuration")
        mode = int(text_value("mode"))
        if mode == 0:
            interface = text_value("interface")
            current_app.sphirewall().firewall().autowan_set(mode, interface)
        else:
            current_app.sphirewall().firewall().autowan_set(mode)

    autowan = current_app.sphirewall().firewall().autowan()
    autowan["device_configuration"] = autowan.get("interfaces")

    forwarding_rules = current_app.sphirewall().firewall().forwarding_rules()
    for rule in forwarding_rules:
        rule = resolve_rule(rule)
        rule["source_string"] = source_string(rule)
        rule["destination_string"] = destination_string(rule)

    return render_template("device_firewall_masquerading.html", autowan=autowan, devices=current_app.sphirewall().network().devices(), forwarding_rules=forwarding_rules)


@login_required
@app.route("/device/firewall/masquerading/add", methods=["POST"])
def firewall_masquerading_add():
    interface = text_value("interface")
    if interface:
        flash("Added WAN interface to pool")
        current_app.sphirewall().firewall().autowan_set_interface(interface)
    return redirect("/device/firewall/masquerading")


@login_required
@app.route("/device/firewall/masquerading/del/<interface>", methods=["GET"])
def firewall_masquerading_delete(interface):
    flash("Removed WAN interface from pool")
    current_app.sphirewall().firewall().autowan_del_interface(interface)
    return redirect("/device/firewall/masquerading")


@login_required
@app.route("/device/firewall/masquerading/edit/<interface>", methods=["GET", "POST"])
def firewall_masquerading_edit(interface):
    if request.method == "POST":
        current_app.sphirewall().firewall().autowan_set_interface(interface, int_value("failover_index"), generate_criteria())
        flash("Adjusted wan interface pool settings")

    all_autowan_interfaces = current_app.sphirewall().firewall().autowan()
    target = None
    for potential in all_autowan_interfaces["interfaces"]:
        if potential["interface"] == interface:
            target = potential
            target = resolve_rule(target)
            break

    return render_template(
        "device_firewall_masquerading_interface.html", rule=target,
        groups=current_app.sphirewall().general().groups(),
        devices=current_app.sphirewall().network().devices(),
        aliases=current_app.sphirewall().firewall().aliases(),
        users=current_app.sphirewall().general().users(),
        periods=current_app.sphirewall().firewall().periods(),
        pools=current_app.sphirewall().firewall().aliases(),
    )


@login_required
@app.route("/device/firewall/masquerading/advanced", methods=["POST", "GET"])
def firewall_masquerading_advanced():
    rules = current_app.sphirewall().firewall().masquerading_rules()
    for rule in rules:
        rule = resolve_rule(rule)
        rule["source_string"] = source_string(rule)
        rule["destination_string"] = destination_string(rule)
    return render_template("device_firewall_masquerading_advanced.html", forwarding_rules=rules, devices=current_app.sphirewall().network().devices())


@login_required
@app.route("/device/firewall/masquerading/advanced/edit", methods=["GET", "POST"])
@app.route("/device/firewall/masquerading/advanced/edit/<id>", methods=["GET", "POST"])
def firewall_masquerading_advanced_edit(id=None):
    if request.method == "POST":
        current_app.sphirewall().firewall().masquerading_rules_add(
            criteria=generate_criteria(),
            natTargetDevice=text_value("natTargetDevice"),
            natTargetIp=text_value("natTargetIp"),
            id=id
        )
        return redirect("/device/firewall/masquerading/advanced")

    rule = current_app.sphirewall().firewall().masquerading_rules(id)
    if id: rule = resolve_rule(rule)
    return render_template(
        "device_firewall_masquerading_advanced_edit.html",
        rule=rule,
        pools=current_app.sphirewall().firewall().aliases(),
        devices=current_app.sphirewall().network().devices(),
        groups=current_app.sphirewall().general().groups(),
        periods=current_app.sphirewall().firewall().periods(),
        users=current_app.sphirewall().general().users()
    )


@login_required
@app.route("/device/firewall/masquerading/advanced/<id>/delete", methods=["GET"])
def firewall_masquerading_advanced_delete(id):
    current_app.sphirewall().firewall().masquerading_rules_delete(id)
    return redirect("/device/firewall/masquerading/advanced")


@login_required
@app.route("/device/firewall/masquerading/advanced/<id>/enable", methods=["POST", "GET"])
def firewall_masquerading_advanced_enable(id):
    current_app.sphirewall().firewall().masquerading_rules_enable(id)
    return redirect("/device/firewall/masquerading/advanced")


@login_required
@app.route("/device/firewall/masquerading/advanced/<id>/disable", methods=["GET", "POST"])
def firewall_masquerading_advanced_disable(id):
    current_app.sphirewall().firewall().masquerading_rules_disable(id)
    return redirect("/device/firewall/masquerading/advanced")


@login_required
@app.route("/device/firewall/acls/qos/add", methods=["GET", "POST"])
@app.route("/device/firewall/acls/qos/add/<id>", methods=["GET", "POST"])
def firewall_acls_qos_add(id=None):
    if request.method == "POST":
        if not id:
            flash("Created a new Rate Limiting Traffic Shaping rule")
        else:
            flash("Modified a Rate Limiting Traffic Shaping rule")
        criteria = generate_criteria()
        current_app.sphirewall().firewall().qos_add(id, text_value("name"), criteria, checkbox_value("cumulative"), text_value("upload"), text_value("download"))
        return redirect("/device/firewall/qos")

    rule = None
    if id: rule = resolve_rule(current_app.sphirewall().firewall().qos(id))
    return render_template(
        "device_firewall_acls_qos_add.html",
        groups=current_app.sphirewall().general().groups(),
        devices=current_app.sphirewall().network().devices(),
        aliases=current_app.sphirewall().firewall().aliases(),
        users=current_app.sphirewall().general().users(),
        periods=current_app.sphirewall().firewall().periods(),
        pools=current_app.sphirewall().firewall().aliases(),
        rule=rule)


@login_required
@app.route("/device/firewall/acls/qos/<id>/delete")
def firewall_acls_qos_delete(id):
    flash("Rule was deleted")
    current_app.sphirewall().firewall().qos_del(id)
    return redirect("/device/firewall/qos")


@login_required
@app.route("/device/firewall/acls/normal/add", methods=["GET", "POST"])
@app.route("/device/firewall/acls/normal/add/<id>", methods=["GET", "POST"])
def firewall_acls_normal_add(id=None):
    if request.method == "POST":
        if id:
            flash("The rule was modified")
        else:
            flash("A new rule was created")

        criteria = generate_criteria()
        current_app.sphirewall().firewall().normal_add(
            id, criteria,
            checkbox_value("ignoreconntrack"),
            text_value("comment"),
            text_value("action"),
            checkbox_value("log"),
            text_value("nice"),
        )
        return redirect("/device/firewall/filtering")

    rule = None
    if id: rule = resolve_rule(current_app.sphirewall().firewall().get(id))
    return render_template(
        "device_firewall_acls_normal_add.html",
        rule=rule,
        groups=current_app.sphirewall().general().groups(),
        devices=current_app.sphirewall().network().devices(),
        aliases=current_app.sphirewall().firewall().aliases(),
        users=current_app.sphirewall().general().users(),
        mode=arg("mode"),
        periods=current_app.sphirewall().firewall().periods(),
        pools=current_app.sphirewall().firewall().aliases()
    )


@login_required
@app.route("/device/firewall/acls/priority/add", methods=["GET", "POST"])
@app.route("/device/firewall/acls/priority/add/<id>", methods=["GET", "POST"])
def firewall_acls_priority_add(id=None):
    if request.method == "POST":
        if not id:
            flash("Created a new Priority Traffic Shaping rule")
        else:
            flash("Modified a Priority Traffic Shaping rule")

        criteria = generate_criteria()
        current_app.sphirewall().firewall().normal_add(
            id, criteria,
            checkbox_value("ignoreconntrack"),
            text_value("comment"),
            text_value("action"),
            checkbox_value("log"),
            text_value("nice"),
        )
        return redirect("/device/firewall/qos")

    rule = None
    if id: rule = resolve_rule(current_app.sphirewall().firewall().get(id))
    return render_template(
        "device_firewall_acls_priority_add.html",
        rule=rule,
        groups=current_app.sphirewall().general().groups(),
        devices=current_app.sphirewall().network().devices(),
        aliases=current_app.sphirewall().firewall().aliases(),
        users=current_app.sphirewall().general().users(),
        periods=current_app.sphirewall().firewall().periods(),
        pools=current_app.sphirewall().firewall().aliases()
    )


@login_required
@app.route("/device/firewall/acls/up/<id>")
def firewall_acls_up(id):
    flash("The rule was moved up")
    current_app.sphirewall().firewall().normal_up(id)
    if "caller" in request.args:
        return redirect(request.args["caller"])

    return redirect("/device/firewall/filtering")


@login_required
@app.route("/device/firewall/acls/down/<id>")
def firewall_acls_down(id):
    flash("The rule was moved down")
    current_app.sphirewall().firewall().normal_down(id)
    if "caller" in request.args:
        return redirect(request.args["caller"])

    return redirect("/device/firewall/filtering")


@login_required
@app.route("/device/firewall/acls/delete/<id>")
def firewall_acls_delete(id):
    flash("The rule was deleted")
    current_app.sphirewall().firewall().normal_delete(id)
    if "caller" in request.args:
        return redirect(request.args["caller"])
    return redirect("/device/firewall/filtering")


@login_required
@app.route("/device/firewall/acls/enable/<id>", methods=["GET", "POST"])
def firewall_acls_enable(id):
    flash("The rule was enabled")
    current_app.sphirewall().firewall().normal_enable(id)
    if "caller" in request.args:
        return redirect(request.args["caller"])

    return redirect("/device/firewall/nat")


@login_required
@app.route("/device/firewall/acls/toggle/<id>", methods=["GET", "POST"])
def firewall_acls_acls_toggle(id):
    if current_app.sphirewall().firewall().get(id)["enabled"]:
        flash("Disabled the requested rule")
        firewall_acls_acls_disable(id)
    else:
        flash("Enabled the requested rule")
        firewall_acls_enable(id)
    return redirect("/device/firewall/filtering")


@login_required
@app.route("/device/firewall/acls/disable/<id>", methods=["GET", "POST"])
def firewall_acls_acls_disable(id):
    current_app.sphirewall().firewall().normal_disable(id)
    flash("Disabled the requested rule")
    if "caller" in request.args:
        return redirect(request.args["caller"])
    return redirect("/device/firewall/nat")


@login_required
@app.route("/device/firewall/priority/disable")
def firewall_priority_disable():
    current_app.sphirewall().general().advanced("QOS_ENABLED", 0)
    return redirect("/device/firewall/qos")


@login_required
@app.route("/device/firewall/priority/enable")
def firewall_priority_enable():
    current_app.sphirewall().general().advanced("QOS_ENABLED", 1)
    return redirect("/device/firewall/qos")


@login_required
@app.route("/device/firewall/qos/disable")
def firewall_qos_disable():
    current_app.sphirewall().general().advanced("QOS_ENABLED", 0)
    return redirect("/device/firewall/qos")


@login_required
@app.route("/device/firewall/qos/enable")
def firewall_qos_enable():
    current_app.sphirewall().general().advanced("QOS_ENABLED", 1)
    return redirect("/device/firewall/qos")


@login_required
@app.route("/device/firewall/balancer/disable")
def firewall_balancer_disable():
    current_app.sphirewall().general().advanced("MULTIPATH_WAN_LOADBALANCING", 0)
    return redirect("/device/firewall/balancer")


@login_required
@app.route("/device/firewall/balancer/enable")
def firewall_balancer_enable():
    current_app.sphirewall().general().advanced("MULTIPATH_WAN_LOADBALANCING", 1)
    return redirect("/device/firewall/balancer")


@login_required
@app.route("/device/firewall/periods", methods=["POST"])
def firewall_periods():
    flash("Created new time period '%s'" % text_value("name"))
    current_app.sphirewall().firewall().periods_create(name=text_value("name"))
    return redirect("/device/firewall/aliases")


@login_required
@app.route("/device/firewall/periods/<id>/delete")
def firewall_periods_delete(id):
    current_app.sphirewall().firewall().periods_del(id=id)
    flash("Deleted time period")
    return redirect("/device/firewall/aliases")


@login_required
@app.route("/device/firewall/periods/<id>", methods=["GET", "POST"])
def firewall_periods_modify(id):
    if request.method == "POST":
        flash("Saved time period")
        startTime, endTime = parse_timeRange(text_value("timeRange"))
        startDate, endDate = parse_date(text_value("startDate")), parse_date(text_value("endDate"))
        current_app.sphirewall().firewall().periods_modify(
            id=id,
            startTime=startTime,
            endTime=endTime,
            startDate=startDate,
            endDate=endDate,
            any=checkbox_value('any'),
            mon=checkbox_value('mon'),
            tue=checkbox_value('tue'),
            wed=checkbox_value('wed'),
            thu=checkbox_value('thu'),
            fri=checkbox_value('fri'),
            sat=checkbox_value('sat'),
            sun=checkbox_value('sun')
        )
        return redirect("/device/firewall/aliases")

    period = current_app.sphirewall().firewall().periods(id=id)
    period["startDate"] = parse_timestamp(period.get("startDate"))
    period["endDate"] = parse_timestamp(period.get("endDate"))
    period["startTime"] = parse_24hour(period.get("startTime"))
    period["endTime"] = parse_24hour(period.get("endTime"))
    return render_template("device_firewall_periods_modify.html", period=period)


def parse_timeRange(trstring):
    return [int(''.join(a.split(":"))) for a in trstring.split(" - ")]


def parse_date(datestr):
    if len(datestr) == 0:
        return -1
    return int(datetime.date(*[int(a) for a in datestr.split("-")]).strftime("%s"))


def parse_timestamp(ts):
    if int(ts) == -1:
        return ""
    return datetime.date.fromtimestamp(int(ts)).isoformat()


def parse_24hour(h24):
    if h24 != -1:
        h24 = str(h24)
        h24 = "0" * (4 - len(h24)) + h24
        return float(str(h24)[:-2]) + float(str(h24)[-2:]) / 60
    return -1

