import base64
from flask import render_template, jsonify, current_app, redirect, request, send_file
from flask.ext.login import login_required
import io
from sphirewallwmi import app
from sphirewallwmi.api.utils import text_value


@app.route("/device/tools/ajax/ping/<hostname>")
def ping(hostname):
    return jsonify(online=current_app.sphirewall().network().ping(hostname))


@app.route("/device/tools/ajax/dig/<hostname>")
def dig(hostname):
    return jsonify(result=current_app.sphirewall().network().dig(hostname).replace("\n", "<br>"))


@app.route("/device/tools/ajax/traceroute/<hostname>")
def traceroute(hostname):
    return jsonify(result=current_app.sphirewall().network().traceroute(hostname).replace("\n", "<br>"))


@app.route("/device/tools/ping")
def tools_ping():
    return render_template("device_tools_ping.html")


@app.route("/device/tools/dig")
def tools_dig():
    return render_template("device_tools_dig.html")


@app.route("/device/tools/traceroute")
def tools_traceroute():
    return render_template("device_tools_traceroute.html")


@app.route("/device/ajax/tools/capture", methods=["GET"])
@login_required
def network_capture_info():
    return jsonify(result=current_app.sphirewall().network().capture())


@app.route("/device/tools/capture", methods=["GET", "POST"])
@login_required
def network_capture():
    if request.method == "POST":
        current_app.sphirewall().network().capture_start(interface=text_value("interface"), filter=text_value("filter"))
    return render_template(
        "device_tools_capture.html",
        devices=current_app.sphirewall().network().devices(),
        capture=current_app.sphirewall().network().capture()
    )


@app.route("/device/tools/capture/stop", methods=["GET"])
@login_required
def network_capture_stop():
    current_app.sphirewall().network().capture_stop()
    return redirect("/device/tools/capture")


@app.route("/device/tools/capture/start", methods=["GET"])
@login_required
def network_capture_start():
    current_app.sphirewall().network().capture_start()
    return redirect("/device/tools/capture")


@app.route("/device/tools/capture/download")
@login_required
def network_capture_download():
    capture=current_app.sphirewall().network().capture()
    capture_raw = current_app.sphirewall().network().capture_raw()
    return send_file(
        io.BytesIO(base64.b64decode(capture_raw["raw_pcap"])),
        attachment_filename='capture_%s.pcap' % capture["interface"], as_attachment=True)
