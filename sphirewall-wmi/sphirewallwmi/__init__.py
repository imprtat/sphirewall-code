import logging
import uuid
from flask import current_app

from flask.app import Flask
import os
from flask.ext.login import LoginManager
from sphirewallwmi.api.utils import get_class

app = Flask(__name__)
app.secret_key = str("testin")

default_config = os.path.join(app.root_path, "configuration.cfg")
if os.path.exists(default_config):
    app.config.from_pyfile(default_config)
else:
    app.config["LOGIN_PROVIDER"] = "sphirewallwmi.routes_login.standard_login"
    app.config["LOGOUT_PROVIDER"] = "sphirewallwmi.routes_login.standard_logout"
    app.config["LOAD_USER_PROVIDER"] = "sphirewallwmi.routes_login.standard_load_user"
    app.config["SPHIREWALL_CONNECTION_PROVIDER"] = "sphirewallwmi.middleware.service.get_sphirewall_direct"
    app.config["STANDARD_ERROR_HANDLER"] = "sphirewallwmi.routes_base.standard_error_handler"
    app.config["STANDARD_DEVICE_ERROR_HANDLER"] = "sphirewallwmi.routes_base.standard_error_handler"
    app.config["ANALYTICS_PROVIDER"] = "sphirewallwmi.middleware.service.standard_analytics_provider"
    app.config["INHEADER_GENERATOR"] = "sphirewallwmi.routes_base.standard_inheader"
    app.config["PRODUCT_NAME"] = "Open Edgewize"

if app.config.get("IMPORT_PATH"):
    import sys
    sys.path.append(app.config["IMPORT_PATH"])

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
logging.basicConfig(level=logging.DEBUG)

class NavigationManager:
    def __init__(self):
        self.registered_structure = []

    def register_section(self, id, name, default_path):
        self.registered_structure.append({"id": id, "name": name, "default_path": default_path, "children": []})

    def register_section_child(self, section_id, id, path, name):
        for section in self.registered_structure:
            if section["id"] == section_id:
                section["children"].append({"id": id, "path": path, "name": name})

navigator = NavigationManager()
navigator.register_section("status", "Status", "/device")
navigator.register_section_child("status", "dashboard", "/device", "Dashboard")
navigator.register_section_child("status", "users", "/device/status/users", "Active Users")
navigator.register_section_child("status", "hosts", "/device/status/hosts", "Active Hosts")
navigator.register_section_child("status", "connections", "/device/status/connections", "Active Connections")
navigator.register_section_child("status", "metrics", "/device/status/metrics", "Performance")
navigator.register_section_child("status", "events", "/device/status/events", "Events and Alerts")
navigator.register_section_child("status", "logs", "/device/status/logs", "System Logs")

navigator.register_section("configure", "Configure", "/device/configuration/general")
navigator.register_section_child("configure", "general", "/device/configuration/general", "General Options")
navigator.register_section_child("configure", "audit", "/device/configuration/audit", "Auditing")
navigator.register_section_child("configure", "alerting", "/device/configuration/events", "Alerting")
navigator.register_section_child("configure", "quotas", "/device/configuration/quotas", "Global Quotas")
navigator.register_section_child("configure", "captiveportal", "/device/configuration/authentication", "Captive Portal")
navigator.register_section_child("configure", "sso", "/device/configuration/authentication/providers", "SSO Providers")
navigator.register_section_child("configure", "users", "/device/configuration/users", "Users and People")
navigator.register_section_child("configure", "groups", "/device/configuration/groups", "Groups and Roles")
navigator.register_section_child("configure", "persistedsessions", "/device/configuration/sessions", "Persisted Sessions")
navigator.register_section_child("configure", "advanced", "/device/configuration/advanced", "Advanced Config")

navigator.register_section("network", "Networking", "/device/network/devices")
navigator.register_section_child("network", "devices", "/device/network/devices", "Network")
navigator.register_section_child("network", "firewall", "/device/firewall/filtering", "Firewall Filtering")
navigator.register_section_child("network", "appfirewall", "/device/firewall/appfiltering", "Application Filtering")
navigator.register_section_child("network", "wan", "/device/firewall/masquerading", "Wan Config")
navigator.register_section_child("network", "qos", "/device/firewall/qos", "Traffic Shaping")
navigator.register_section_child("network", "ids", "/device/network/ids", "Ids")
navigator.register_section_child("network", "wireless", "/device/network/wireless", "Wireless")
navigator.register_section_child("network", "mdns", "/device/network/mdns", "Mdns/Bonjour")
navigator.register_section_child("network", "vpn", "/device/network/vpn", "Site-to-Site Vpn")
navigator.register_section_child("network", "clientvpn", "/device/network/clientvpn", "Client Vpn")
navigator.register_section_child("network", "dyndns", "/device/network/dyndns", "Global Dyndns")
navigator.register_section_child("network", "objects", "/device/firewall/aliases", "Objects")

navigator.register_section("tools", "Tools", "/device/tools/ping")
navigator.register_section_child("tools", "ping", "/device/tools/ping", "Ping")
navigator.register_section_child("tools", "dig", "/device/tools/dig", "Dns Lookup")
navigator.register_section_child("tools", "traceroute", "/device/tools/traceroute", "Traceroute")
navigator.register_section_child("tools", "capture", "/device/tools/capture", "Packet Capture")

if app.config.get("STARTUP_METHOD"):
   get_class(app.config.get("STARTUP_METHOD"))(app, navigator)

with app.test_request_context():
    current_app.service = get_class(app.config.get("ANALYTICS_PROVIDER"))
    current_app.sphirewall = get_class(app.config["SPHIREWALL_CONNECTION_PROVIDER"])

    from sphirewallwmi import routes_base
    from sphirewallwmi import routes_login
    from sphirewallwmi import routes_device_configuration
    from sphirewallwmi import routes_device_firewall
    from sphirewallwmi import routes_device_network
    from sphirewallwmi import routes_device_status
    from sphirewallwmi import routes_analytics_ajax
    from sphirewallwmi import routes_device_tools
