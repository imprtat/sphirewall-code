import unittest
from selenium_base import SeleniumTester


class ConfigurationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Users and Groups')]").click()

    def test_set_general_options(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Configuration')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Captive Portal')]").click()

        self.tester.driver.find_element_by_name("force_login").click()
        self.tester.setValue("cp_url", "http://google.com")
        self.tester.setValue("session_timeout", "700")

        self.tester.driver.find_element_by_id("save-button").click()

        self.tester.check_selected("force_login")
        self.tester.check_form_element_value("cp_url", "http://google.com")
        self.tester.check_form_element_value("session_timeout", "700")

    def tearDown(self):
        self.tester.disconnect()
