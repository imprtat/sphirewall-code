This is the Sphirewall Integration Test Suite:

+ You will need to have Firefox, and the Selenium python drivers installed
https://pypi.python.org/pypi/selenium
http://simeonvisser.com/2/running-tests-in-python-with-selenium-2-and-webdriver/

+ You also must have sphirewall and the management interface running
+ Then just run the python guy

REQUIREMENTS:
+ You must enable QOS
+ You must have an interface eth1 that is NOT live, vmware/virtualbox can do this easily
