import unittest
from selenium_base import SeleniumTester


class ConfigurationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def clear_and_send(self, name, value):
        element = self.tester.driver.find_element_by_name(name)
        element.clear()
        element.send_keys(value)

    def test_set_general_options(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Configuration')]").click()
        self.tester.driver.find_element_by_name("disable_remote_management").click()
        self.tester.driver.find_element_by_name("disable_remote_management").click()
        self.tester.driver.find_element_by_name("snort_enabled").click()
        self.clear_and_send("snort_file", "/snort-file")
        self.tester.driver.find_element_by_name("enforcegss").click()
        self.tester.driver.find_element_by_id("save-button").click()

        name = self.tester.driver.find_element_by_name("snort_file")
        self.assertEquals(name.get_attribute("value"), "/snort-file")
        self.tester.check_selected("snort_enabled")
        self.tester.check_selected("enforcegss")

    def tearDown(self):
        self.tester.disconnect()
