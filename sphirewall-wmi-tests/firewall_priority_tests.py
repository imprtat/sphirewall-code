#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester


class FirewallAclsTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def gotoRules(self):
        self.clickFirewall()
        self.clickAcl()

    def clickAcl(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Priority Qos')]").click()

    def clickFirewall(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Policy')]").click()

    def test_BasicCriteria_Accept(self):
        self.gotoRules()
        self.tester.find_element_by_text("Create a rule").click()

        self.tester.add_criteria("Source Port/s")
        self.tester.driver.find_element_by_name("sport").send_keys("80")

        self.tester.driver.find_element_by_xpath("//input[@value='PRIORITISE']").click()
        self.tester.driver.find_element_by_xpath("//select[@name='nice']/option[text()='5']").click()
        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]//ancestor::tr/td[contains(normalize-space(), 'priority 5')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]//ancestor::tr/td/a[contains(@Class, 'delete-link')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]"))

    def test_Enable_Disable(self):
        self.gotoRules()
        self.tester.find_element_by_text("Create a rule").click()

        self.tester.add_criteria("Source Port/s")
        self.tester.driver.find_element_by_name("sport").send_keys("80")

        self.tester.driver.find_element_by_xpath("//input[@value='PRIORITISE']").click()
        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]"))

        enabled = self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]//ancestor::tr/td/form/input")
        self.assertFalse(enabled.is_selected())

        enabled.click()
        enabled = self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]//ancestor::tr/td/form/input")
        self.assertTrue(enabled.is_selected())

        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]//ancestor::tr/td/a[contains(@Class, 'delete-link')]").click()

    def tearDown(self):
        self.tester.disconnect()
