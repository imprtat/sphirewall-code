#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester


class ConfigurationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Users and Groups')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Persisted Sessions')]").click()

    def test_add_remove_persisted_session(self):
        self.tester.select_option("username", "admin")
        self.tester.setValue("hw", "aa:bb:cc:dd:ee:ff")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'admin')]"
            "//ancestor::tr/td[contains(normalize-space(), 'aa:bb:cc:dd:ee:ff')]"))

        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Forget')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'aa:bb:cc:dd:ee:ff')]"))

    def tearDown(self):
        self.tester.disconnect()
