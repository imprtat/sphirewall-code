import unittest
import time
from selenium_base import SeleniumTester


class NetDevicesTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def gotoDevices(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Networking')]").click()

    def test_displayed_correctly(self):
        self.gotoDevices()
        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(text(), 'lo')]//ancestor::tr/td[contains(text(), '127.0.0.1')]//ancestor::tr/td[contains(text(), '255.0.0.0')]//ancestor::tr/td[contains(text(), 'UP')]"))

    def click_add_device_button(self):
        self.tester.get_element(id="add-device").click()

    def click_save_device(self):
        self.tester.get_element(id="save-button").click()

    def remove_device(self, name):
        enabled = self.tester.get_element(xpath="//table//tr/td[contains(normalize-space(), '%s')]//ancestor::tr/td/a[contains(@Class, 'delete-link')]" % name)
        enabled.click()

    def create_standard_physical_device(self, name, interface):
        self.tester.setValue("name", name)
        self.tester.select_option("type", "Physical Interface")
        self.tester.setValue("device", interface)

    def test_create_vlan_interface(self):
        self.gotoDevices()
        self.click_add_device_button()

        self.tester.select_option("type", "VLAN Interface")
        self.tester.setValue("vlanId", "101")
        self.tester.select_option("vlanInterface", "eth0")

        self.click_save_device()

        assert self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth0.101')]")
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth0.101')]").click()

        assert self.tester.get_element(name="type").get_attribute("value") == "vlan"
        assert self.tester.get_element(name="vlanId").get_attribute("value") == "101"
        assert self.tester.get_element(name="vlanInterface").get_attribute("value") == "eth0"

        self.click_save_device()
        self.remove_device("eth0.101")

    def test_create_bridged_interface(self):
        self.gotoDevices()
        self.click_add_device_button()

        self.tester.select_option("type", "Network Bridge")
        self.click_save_device()

        assert self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'br0')]")

        #adding a bridge interface can take a few seconds
        time.sleep(2)
        self.gotoDevices()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'br0')]").click()

        self.tester.set_choserval("bridgeDevices", "eth1")
        self.click_save_device()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'br0')]").click()
        assert self.tester.get_element(name="type").get_attribute("value") == "bridge"
        self.tester.check_select_current_value("bridgeDevices", "eth1")

        self.click_save_device()
        self.remove_device("br0")

    def test_create_lacp_interface(self):
        self.gotoDevices()
        self.click_add_device_button()

        self.tester.select_option("type", "LACP Interface")
        self.click_save_device()

        assert self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'bond0')]")
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'bond0')]").click()

        self.tester.select_option("lacpMode", "balance-rr")
        self.tester.set_choserval("lacpDevices", "eth1")
        self.click_save_device()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'bond0')]").click()
        assert self.tester.get_element(name="type").get_attribute("value") == "lacp"
        assert self.tester.get_element(name="lacpMode").get_attribute("value") == "0"
        self.tester.check_select_current_value("lacpDevices", "eth1")

        self.click_save_device()

        #Removing bonded links is broken in the kernel, a new event is triggered
        self.remove_device("bond0")


    def test_creating_and_saving_device_dhcp_enabled(self):
        self.gotoDevices()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth1')]").click()
        self.tester.select("dhcp")
        self.click_save_device()

        assert self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth1')]")
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth1')]").click()

        assert self.tester.check_checkbox_selected("dhcp")
        self.click_save_device()


    def test_bring_down_up(self):
        self.gotoDevices()
        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td/span[contains(@Class, 'up')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td/a[contains(@Class, 'down-link')]").click()
        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td/span[contains(text(), 'DOWN')]"))

        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td/a[contains(@Class, 'up-link')]").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td/span[contains(text(), 'UP')]"))

    def test_dhcp_server_enabled(self):
        self.gotoDevices()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.tester.select_option("dhcpServerMode", "Respond to DHCP requests")
        self.tester.setValue("dhcpServerStart", "10.1.1.10")
        self.tester.setValue("dhcpServerEnd", "10.1.1.20")
        self.click_save_device()

        assert self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth1')]")
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.tester.check_form_element_value("dhcpServerStart", "10.1.1.10")
        self.tester.check_form_element_value("dhcpServerEnd", "10.1.1.20")
        self.click_save_device()

    def test_dhcp_server_relay(self):
        self.gotoDevices()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.tester.select_option("dhcpServerMode", "Forward DHCP requests")
        self.tester.setValue("dhcpServerRelay", "10.1.1.18")
        self.click_save_device()

        assert self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth1')]")
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.tester.check_form_element_value("dhcpServerRelay", "10.1.1.18")
        self.click_save_device()

    def tearDown(self):
        self.tester.disconnect()
