#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester


class RoutesTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def gotoRoutes(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Networking')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Devices')]").click()

    def test_SavingInterface(self):
        self.gotoRoutes()

        self.tester.driver.find_element_by_name("destination").send_keys("10.22.2.0")
        self.tester.driver.find_element_by_name("destination_cidr").send_keys("24")
        self.tester.driver.find_element_by_name("route_nexthop").send_keys("192.168.1.1")
        self.tester.driver.find_element_by_xpath("//select[@name='route_device']/option[text()='eth0']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(text(), '10.22.2.0/24')]//ancestor::tr/td[contains(text(), '192.168.1.1')]//ancestor::tr/td[contains(text(), 'eth0')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(text(), '10.22.2.0/24')]//ancestor::tr/td/a[contains(@Class, 'delete-link')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(text(), '10.22.2.0/24')]"))

    def tearDown(self):
        self.tester.disconnect()
