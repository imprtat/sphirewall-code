import unittest
from selenium_base import SeleniumTester


class SignaturesTester(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def gotoRules(self):
        self.clickFirewall()
        self.clickAcl()

    def clickAcl(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Signatures')]").click()

    def clickFirewall(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Policy')]").click()

    def test_add_delete(self):
        self.gotoRules()
        self.tester.driver.find_element_by_id("addSignatureButton").click()

        self.tester.setValue("name", "name")
        self.tester.setValue("description", "description")
        self.tester.setValue("expression", "expression")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'name')]"
            "//ancestor::tr/td[contains(normalize-space(), 'description')]"))

        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(),'name')]"
            "//ancestor::tr/td/a[contains(@Class, 'delete-link')]").click()

        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'name')]"))

    def tearDown(self):
        self.tester.disconnect()
