import unittest
from selenium_base import SeleniumTester


class ConfigurationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Configuration')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Captive Portal')]").click()
        self.tester.driver.find_element_by_id("timeouts_link").click()

    def goto_timeouts(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Configuration')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Captive Portal')]").click()
        self.tester.driver.find_element_by_id("timeouts_link").click()


    def create_alias(self, name):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Policy')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Pools')]").click()

        self.tester.driver.find_element_by_name("name").send_keys(name)
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='IP Range List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

    def test_create_timeout(self):
        #Create an alias first
        self.create_alias("network1")
        self.create_alias("network2")

        self.goto_timeouts()

        self.tester.driver.find_element_by_id("create").click()
        self.tester.setValue("timeout", 500)
        self.tester.set_choserval("networks", "network1")
        self.tester.set_choserval("networks", "network2")

        self.tester.driver.find_element_by_id("save").click()

        #Now check that it exists
        assert self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'network1')]//ancestor::tr/td[contains(normalize-space(), '500')]")

        #Now modify it
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'network1')]//ancestor::tr/td/a[contains(normalize-space(), 'Configure')]").click()
        self.tester.setValue("timeout", 300)
        self.tester.driver.find_element_by_id("save").click()

        #Check the mods worked
        assert self.tester.driver.find_element_by_xpath(
                    "//table//tr/td[contains(normalize-space(), 'network1')]//ancestor::tr/td[contains(normalize-space(), '300')]")

        #Delete it
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'network1')]//ancestor::tr/td/a[contains(normalize-space(), 'Delete')]").click()

        #Check the delete worked
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'network1')]//ancestor::tr/td[contains(normalize-space(), '300')]"))

    def tearDown(self):
        self.tester.disconnect()
