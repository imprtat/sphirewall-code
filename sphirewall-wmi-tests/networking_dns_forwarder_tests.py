#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester


class DnsForwarderTestCase(unittest.TestCase):

    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def gotoForwarder(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Networking')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Dns Forwarder')]").click()

    def test_save_settings(self):
        self.gotoForwarder()

        self.tester.select("enabled")
        self.tester.setValue("domain", "localdomain.local")
        self.tester.setValue("primary", "8.8.8.8")
        self.tester.setValue("secondary", "4.4.4.4")
        self.tester.driver.find_element_by_id("save-button").click()

        self.tester.check_selected("enabled")
        self.tester.check_form_element_value("domain", "localdomain.local")
        self.tester.check_form_element_value("primary", "8.8.8.8")
        self.tester.check_form_element_value("secondary", "4.4.4.4")

    def tearDown(self):
        self.tester.disconnect()
