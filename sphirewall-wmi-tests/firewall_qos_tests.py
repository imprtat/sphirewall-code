import unittest
from selenium_base import SeleniumTester


class FirewallAclsTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def gotoRules(self):
        self.clickFirewall()
        self.clickAcl()

    def clickAcl(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Rate Limiting Qos')]").click()

    def clickFirewall(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Policy')]").click()

    def test_addQos_Basic(self):
        self.gotoRules()
        self.tester.find_element_by_text("Create QOS Rule").click()

        self.tester.add_criteria("Source Port/s")
        self.tester.driver.find_element_by_name("sport").send_keys("80")

        self.tester.driver.find_element_by_name("upload").send_keys("512000")
        self.tester.driver.find_element_by_name("download").send_keys("1024000")

        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]//ancestor::tr/td[contains(normalize-space(), 'Upload: 512000B/s Download: 1024000B/s')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]//ancestor::tr/td/a[contains(@Class, 'delete-link')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port')]"))

    def tearDown(self):
        self.tester.disconnect()
