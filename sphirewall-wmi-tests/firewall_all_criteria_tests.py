import unittest
from selenium_base import SeleniumTester


class FirewallAclsTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def gotoRules(self):
        self.clickFirewall()
        self.clickAcl()

    def clickAcl(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Filtering')]").click()

    def clickFirewall(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Policy')]").click()

    def add_criteria(self, name):
        self.tester.select_option("availableCriteria", name)
        self.tester.driver.find_element_by_id("addCriteriaButton").click()

    def delete_rule_by_name(self, name):
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), '"+name+"')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), '"+name+"')]"))


    def test_ListOfPorts(self):
        self.gotoRules()
        self.tester.find_element_by_text("Create a rule").click()
        self.tester.setValue("comment", "portrule1")

        self.tester.add_criteria("Source Port/s")
        self.tester.driver.find_element_by_name("sport").send_keys("10,20,30,40,50,60")

        self.tester.add_criteria("Destination Port/s")
        self.tester.driver.find_element_by_name("dport").send_keys("1,2,3,4,6,7")

        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port \"[60, 50, 40, 30, 20, 10]\" destination port \"[7, 6, 4, 3, 2, 1]\"')]"
            "//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.delete_rule_by_name("portrule1")

    def test_PortRange(self):
        self.gotoRules()
        self.tester.find_element_by_text("Create a rule").click()
        self.tester.setValue("comment", "portrule1")

        self.tester.add_criteria("Source Port/s")
        self.tester.driver.find_element_by_name("sport_start").send_keys("10")
        self.tester.driver.find_element_by_name("sport_end").send_keys("20")

        self.tester.add_criteria("Destination Port/s")
        self.tester.driver.find_element_by_name("dport_start").send_keys("1")
        self.tester.driver.find_element_by_name("dport_end").send_keys("5")

        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source port \"10-20\"')]"
            "//ancestor::tr/td[contains(normalize-space(), 'destination port \"1-5\"')]"
            "//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))

        self.delete_rule_by_name("portrule1")

    def test_Aliases(self):
        self.gotoRules()
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Pools')]").click()

        self.tester.driver.find_element_by_name("name").send_keys("test-alias1")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[normalize-space()='IP Range List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_name("name").send_keys("test-destalias")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[normalize-space()='IP Range List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.clickAcl()
        self.tester.find_element_by_text("Create a rule").click()
        self.tester.setValue("comment", "portrule1")

        self.tester.add_criteria("Source Pool")
        self.tester.set_choserval("sourceAlias", "test-alias1")

        self.tester.add_criteria("Destination Pool")
        self.tester.set_choserval("destAlias", "test-destalias")

        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'plus more')]"
            "//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.delete_rule_by_name("portrule1")

        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Pools')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(normalize-space(), 'test-alias1')]//ancestor::tr/td/a[contains(normalize-space(), 'Delete')]").click()

    def test_Devices(self):
        self.gotoRules()
        self.tester.find_element_by_text("Create a rule").click()
        self.tester.setValue("comment", "portrule1")

        self.tester.add_criteria("Source Interface")
        self.tester.driver.find_element_by_xpath("//select[@name='sourceInterface']/option[@value='lo']").click()

        self.tester.add_criteria("Destination Interface")
        self.tester.driver.find_element_by_xpath("//select[@name='destInterface']/option[@value='eth0']").click()
        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'source interface \"lo\"')]"
            "//ancestor::tr/td[contains(normalize-space(), 'destination interface \"eth0\"')]"
            "//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.delete_rule_by_name("portrule1")

    def test_Groups(self):
        self.gotoRules()
        self.tester.find_element_by_text("Create a rule").click()
        self.tester.setValue("comment", "grouprule1")

        self.tester.add_criteria("Source User Group")
        self.tester.set_choserval("groups", "defaultadmin")
        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'grouprule1')]"
            "//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.delete_rule_by_name("grouprule1")

    def test_Users(self):
        self.gotoRules()
        self.tester.find_element_by_text("Create a rule").click()
        self.tester.setValue("comment", "userrule1")

        self.tester.add_criteria("Source User")
        self.tester.set_choserval("users", "admin")
        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'userrule1')]"
            "//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.delete_rule_by_name("userrule1")

    def test_Protocols(self):
        self.gotoRules()
        self.tester.find_element_by_text("Create a rule").click()
        self.tester.setValue("comment", "protorule")

        self.tester.add_criteria("Protocol")
        self.tester.driver.find_element_by_xpath("//select[@name='protocol']/option[normalize-space()='TCP']").click()
        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'TCP')]//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.delete_rule_by_name("protorule")


    def test_timeperiods(self):
        self.gotoRules()
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Periods')]").click()

        self.tester.driver.find_element_by_name("name").send_keys("test-period55")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.clickAcl()
        self.tester.find_element_by_text("Create a rule").click()
        self.tester.setValue("comment", "period")

        self.tester.add_criteria("Time Periods")
        self.tester.set_choserval("timePeriods", "test-period55")

        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'periods')]"
            "//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.delete_rule_by_name("period")

    def test_networks(self):
        self.gotoRules()
        self.clickAcl()
        self.tester.find_element_by_text("Create a rule").click()
        self.tester.setValue("comment", "networks")

        self.tester.add_criteria("Source Network")
        self.tester.setValue("sourceIp", "10.1.1.1")
        self.tester.setValue("sourceMask", "255.255.255.0")

        self.tester.add_criteria("Destination Network")
        self.tester.setValue("destIp", "10.1.8.7")
        self.tester.setValue("destMask", "255.255.22.254")

        self.tester.driver.find_element_by_id("save-button").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'')]"
            "//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.delete_rule_by_name("networks")

    def tearDown(self):
        self.tester.disconnect()
