#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester


class AuthenticationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def goto_openvpn(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Networking')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Vpn Servers')]").click()

    def test_manual_ipsec_gateway_to_gateway(self):
        self.goto_openvpn()
        self.tester.driver.find_element_by_name("name").send_keys("gw_to_gw")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='IPSec Gateway to Gateway']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.tester.get_element(name="auto_start").click()
        self.tester.get_element(name="secret_key").send_keys("secret123")
        self.tester.get_element(name="local_host").send_keys("localhost")
        self.tester.get_element(name="local_subnet").send_keys("172.16.0.0/24")
        self.tester.get_element(name="local_id").send_keys("@localhost-gw.local")

        self.tester.get_element(name="remote_host").send_keys("localhost")
        self.tester.get_element(name="remote_subnet").send_keys("172.16.2.0/24")
        self.tester.get_element(name="remote_id").send_keys("@remote-gw.local")

        self.tester.get_element(id="save-button").click()

        self.tester.get_element(id="start_connection").click()
        self.tester.get_element(id="stop_connection").click()

        #Check that the values have all been saved
        assert self.tester.get_element(name="secret_key").get_attribute("value") == "secret123"
        assert self.tester.get_element(name="local_host").get_attribute("value") == "localhost"
        assert self.tester.get_element(name="local_subnet").get_attribute("value") == "172.16.0.0/24"
        assert self.tester.get_element(name="local_id").get_attribute("value") == "@localhost-gw.local"
        assert self.tester.get_element(name="remote_host").get_attribute("value") == "localhost"
        assert self.tester.get_element(name="remote_subnet").get_attribute("value") == "172.16.2.0/24"
        assert self.tester.get_element(name="remote_id").get_attribute("value") == "@remote-gw.local"

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Vpn Servers')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'gw_to_gw')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'gw_to_gw')]"))

    def tearDown(self):
        self.tester.disconnect()
