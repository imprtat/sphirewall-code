import unittest
from selenium_base import SeleniumTester

class WebfilterTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()

    def gotoAliases(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Policy')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Pools')]").click()

    def deleteAlias(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Pools')]").click()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'porn')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()

    def delete_rule_by_name(self, name):
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), '"+name+"')]"
            "//ancestor::tr/td/a[contains(@Class, 'delete-link')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), '"+name+"')]"))

    def test_createWebsiteList(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("porn")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Website List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'L7 Filter')]").click()
        self.tester.find_element_by_text("Add Rule").click()
        self.tester.setValue("name", "rule name")

        #Test basic criteria
        self.tester.add_criteria("Source Network")
        self.tester.setValue("sourceIp", "10.1.1.1")
        self.tester.setValue("sourceMask", "255.255.255.0")

        self.tester.set_choserval("list", "porn")

        self.tester.driver.find_element_by_name("fireEvent").click()
        self.tester.driver.find_element_by_name("redirect").click()
        self.tester.driver.find_element_by_name("redirectUrl").send_keys("http://google.com")

        self.tester.driver.find_element_by_id("save-button").click()

        #Check Action Text
        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'source ip \"10.1.1.1/255.255.255.0\"')]"
            "//ancestor::tr/td[contains(normalize-space(), 'Deny Request and fire event and redirect')]"))

        self.delete_rule_by_name("rule name")
        self.deleteAlias()

    def test_enableDisableRule(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'L7 Filter')]").click()
        self.tester.find_element_by_text("Add Rule").click()
        self.tester.setValue("name", "rule name")

        self.tester.driver.find_element_by_id("save-button").click()
        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'rule name')]"))

        enabled = self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(text(), 'rule name')]//ancestor::tr/td/form/input")
        self.assertFalse(enabled.is_selected())

        enabled.click()
        enabled = self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(text(), 'rule name')]//ancestor::tr/td/form/input")
        self.assertTrue(enabled.is_selected())
        self.delete_rule_by_name("rule name")

    def test_select_signature(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'L7 Filter')]").click()
        self.tester.find_element_by_text("Add Rule").click()
        self.tester.setValue("name", "rule name")

        self.tester.select("signature_enabled")
        self.tester.select_option("signature", "Direct IP Http Traffic")

        self.tester.driver.find_element_by_id("save-button").click()
        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'rule name')]"))

        self.delete_rule_by_name("rule name")


    def tearDown(self):
        self.tester.disconnect()
