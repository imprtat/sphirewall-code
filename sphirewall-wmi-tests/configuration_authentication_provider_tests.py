import unittest
from selenium_base import SeleniumTester


class ConfigurationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Users and Groups')]").click()

    def test_set_general_options(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Providers')]").click()

        self.tester.driver.find_element_by_name("ldap_enabled").click()
        self.tester.driver.find_element_by_name("wmic_enabled").click()
        self.tester.driver.find_element_by_name("pam_enabled").click()
        self.tester.driver.find_element_by_name("http_enabled").click()
        self.tester.setValue("http_baseurl", "http://authhandler.com")

        self.tester.driver.find_element_by_id("save-button").click()

        self.tester.check_selected("ldap_enabled")
        self.tester.check_selected("wmic_enabled")
        self.tester.check_selected("pam_enabled")
        self.tester.check_selected("http_enabled")

    def test_ldap_base_options(self):
        self.tester.driver.get(self.tester.baseurl + "/device/configuration/general/ldap")
        self.tester.setValue("hostname", "ldap.local")
        self.tester.setValue("port", "389")
        self.tester.setValue("username", "user")
        self.tester.setValue("password", "password")

        self.tester.driver.find_element_by_id("save-button").click()

        self.tester.check_form_element_value("hostname", "ldap.local")
        self.tester.check_form_element_value("port", "389")
        self.tester.check_form_element_value("username", "user")
        self.tester.check_form_element_value("password", "password")

    def test_ldap_schema_options(self):
        self.tester.driver.get(self.tester.baseurl + "/device/configuration/general/ldap")
        self.tester.setValue("basedn", "basedn")
        self.tester.select("authenticateuserusingsupplieddn")
        self.tester.setValue("userdnattribute", "userdnattribute")
        self.tester.setValue("usernameattribute", "usernameattribute")
        self.tester.setValue("userbase", "userbase")
        self.tester.setValue("usergroupattribute", "usergroupattribute")
        self.tester.setValue("groupbase", "groupbase")
        self.tester.setValue("groupnameattribute", "groupnameattribute")
        self.tester.setValue("groupmembernameattribute", "groupmembernameattribute")

        self.tester.driver.find_element_by_id("save-button").click()

        self.tester.check_form_element_value("basedn", "basedn")
        self.tester.check_form_element_value("userdnattribute", "userdnattribute")
        self.tester.check_form_element_value("userbase", "userbase")
        self.tester.check_form_element_value("usergroupattribute", "usergroupattribute")
        self.tester.check_form_element_value("usernameattribute", "usernameattribute")
        self.tester.check_form_element_value("groupbase", "groupbase")
        self.tester.check_form_element_value("groupnameattribute", "groupnameattribute")
        self.tester.check_form_element_value("groupmembernameattribute", "groupmembernameattribute")

    def test_ldap_schema_options_users_manage_groups(self):
        self.tester.driver.get(self.tester.baseurl + "/device/configuration/general/ldap")
        self.tester.setValue("basedn", "basedn")
        self.tester.select("authenticateuserusingsupplieddn")
        self.tester.select("userownedgroupmembership")
        self.tester.setValue("userdnattribute", "userdnattribute")
        self.tester.setValue("usernameattribute", "usernameattribute")
        self.tester.setValue("userbase", "userbase")
        self.tester.setValue("usergroupattribute", "usergroupattribute")

        self.tester.driver.find_element_by_id("save-button").click()

        self.tester.check_form_element_value("basedn", "basedn")
        self.tester.check_form_element_value("userdnattribute", "userdnattribute")
        self.tester.check_form_element_value("userbase", "userbase")
        self.tester.check_form_element_value("usergroupattribute", "usergroupattribute")
        self.tester.check_form_element_value("usernameattribute", "usernameattribute")
        self.tester.check_selected("userownedgroupmembership")

    def test_wmi_authentication(self):
        self.tester.driver.get(self.tester.baseurl + "/device/configuration/authentication/wmi")
        self.tester.setValue("wmic_username", "username")
        self.tester.setValue("wmic_password", "password")
        self.tester.setValue("wmic_domain", "domain")
        self.tester.setValue("wmic_host", "host")

        self.tester.driver.find_element_by_id("save-button").click()

        self.tester.check_form_element_value("wmic_username", "username")
        self.tester.check_form_element_value("wmic_password", "password")
        self.tester.check_form_element_value("wmic_domain", "domain")
        self.tester.check_form_element_value("wmic_host", "host")

    def tearDown(self):
        self.tester.disconnect()
