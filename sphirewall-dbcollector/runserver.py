#!/usr/bin/env python2

from sphirewalldbcollector import app
app.run(host='0.0.0.0', port=5687, debug=False, threaded=True)
