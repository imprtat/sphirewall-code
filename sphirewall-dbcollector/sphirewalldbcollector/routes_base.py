import json
from flask import request, current_app
import flask
from werkzeug.local import LocalProxy
from sphirewalldbcollector import app, applogger
from sphirewalldbcollector.model import PersistedConnection, PersistedEvent


def get_db_session():
    if hasattr(flask.g, "_db_session"):
        return flask.g._db_session
    else:
        flask.g._db_session = current_app.init_db()
    return flask.g._db_session


@app.before_request
def before():
    flask.g.db_session = LocalProxy(get_db_session)


@app.after_request
def after(f):
    if app.config.get("USE_DB"):
        flask.g.db_session.commit()
        current_app.close_db(flask.g.db_session)
    return f


def persist_connections(device_id, items):
    for item in items:
        connection = PersistedConnection()
        connection.device_id = device_id
        connection.source_ip = item.get("sourceIp")
        connection.destination_ip = item.get("destIp")
        connection.source_port = item.get("sourcePort")
        connection.destination_port = item.get("destPort")
        connection.time = item.get("time")
        connection.upload = item.get("upload")
        connection.download = item.get("download")
        connection.user_agent = item.get("useragent")
        connection.content_type = item.get("contenttype")
        connection.input_device = item.get("inputDev")
        connection.output_device = item.get("outputDev")
        connection.protocol = item.get("protocol")
        connection.source_hostname = item.get("sourceHostname")
        connection.hw_address = item.get("hwAddress")
        connection.username = item.get("user")
        connection.http_hostname = item.get("httpHost")
        connection.tag = item.get("tag")

        if app.config.get("USE_DB"):
            get_db_session().add(connection)

        if app.config.get("USE_LOGFILE"):
            applogger.info("connection " + json.dumps(item))

def persist_events(device_id, items):
    for item in items:
        event = PersistedEvent()
        event.device_id = device_id
        event.type = item.get("key")
        event.time = item.get("time")
        event.params = json.dumps(item.get("params"))

        if app.config.get("USE_LOGFILE"):
            applogger.info("event " + json.dumps(item))

        if app.config.get("USE_DB"):
            get_db_session().add(event)


@app.route("/", methods=["POST"])
def insert_data_items():
    data_dict = json.loads(request.data)
    if data_dict["type"] == "CONNECTIONS":
        persist_connections(data_dict["deviceid"], data_dict["items"])
    if data_dict["type"] == "EVENTS":
        persist_events(data_dict["deviceid"], data_dict["items"])
    return "OK"

@app.errorhandler(500)
def mis_error(e):
    applogger.error("error %s" % str(e))
    return e
