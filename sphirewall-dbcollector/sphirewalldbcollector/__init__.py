import logging
import uuid
from flask import current_app

from flask.app import Flask
import os
from multiprocessing import Lock
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sphirewalldbcollector.model import Base

app = Flask(__name__)
app.secret_key = str(uuid.uuid4())

default_config = os.path.join(app.root_path, "/etc/sphirewall_dbcollector.conf")
if os.path.exists(default_config):
    app.config.from_pyfile(default_config)

engine = create_engine(
    app.config["SQLALCHEMY_DATABASE_URI"], convert_unicode=True, pool_size=10, pool_recycle=3600
)

applogger = app.logger
file_handler = logging.FileHandler(app.config["LOGFILE"])
file_handler.setLevel(logging.INFO)
applogger.setLevel(logging.INFO)
applogger.addHandler(file_handler)

lock = Lock()
def init_db():
    with lock:
        connection = engine.connect()
        if app.config.get("CREATE_DB", True):
            Base.metadata.create_all(bind=engine)
    session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=connection))
    return session


def close_db(db_session):
    db_session.flush()
    db_session.commit()
    db_session.close()
    db_session.bind.close()


with app.test_request_context():
    current_app.init_db = init_db
    current_app.close_db = close_db

    from sphirewalldbcollector import routes_base